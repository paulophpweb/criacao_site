(function($){
	$(document).ready(function(){
		$("#acf-field-icone_do_botao")
		.css({width:"200px"})
		.parent()
		.append("<input type='button' class='button dashicons-picker' value='Choose Icon'>");
		$('.dashicons-picker').dashiconsPicker();

		//Removendo campos desnecessários dentro do ACF
		$("#hide-on-screen, .field_option_image").hide();
		$("input[value='uploadedTo']").prop('checked', true);
		$("#hide-on-screen").prev().hide();
	});
})(jQuery);