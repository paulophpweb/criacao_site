<?php
require_once(_DIR_PLUGIN."sdlib/theme/SDAbstractImage.php");
class SDAbstractController extends SDAbstractImage{
	public $admin_post;
	public $post;
	public $view;
	public $partial;
	private $viewHTML = "";
	public function __construct($admin_post){
		$aNewVars = array();

		if(!empty($admin_post->vars) && count($admin_post->vars)){
			$page = ceil(count($admin_post->vars) / 2);
			$j = 0;
			for($i=0;$i<$page;$i++){
				$aNewVars[$admin_post->vars[$j+$i]] = $admin_post->vars[$j+$i+1];
				$j++;
			}
		}
		$thema_config = get_page_by_path('thema-config',OBJECT,'page');
		$admin_post->vars = $aNewVars;
		$this->view = new stdClass;
		$this->admin_post = $admin_post;
		$this->admin_post->status_de_desenvolvimento = get_field("status_de_desenvolvimento", $thema_config->ID);
		$this->admin_post->google_site_verification = get_field("google_site_verification", $thema_config->ID);
		

		unset($this->admin_post->post_parent);
		unset($this->admin_post->post_content_filtered);
		unset($this->admin_post->post_excerpt);
		unset($this->admin_post->comment_status);
		unset($this->admin_post->post_content);
		unset($this->admin_post->post_status);
		unset($this->admin_post->ping_status);
		unset($this->admin_post->post_password);
		unset($this->admin_post->guid);
		unset($this->admin_post->to_ping);
		unset($this->admin_post->pinged);
		unset($this->admin_post->post_modified);
		unset($this->admin_post->post_modified_gmt);
		unset($this->admin_post->post_date);
		unset($this->admin_post->post_date_gmt);
		unset($this->admin_post->post_author);

		$this->checkRender();
		add_action('wp_head', array($this, "print_head"));
		add_action('wp_footer', array($this, "print_footer"));
		global $compress_css;
		$compress_css = 1;
		add_action( 'wp_print_styles', array($this, "print_styles"));
	}
	public function print_styles(){

		$this->_addSEO();
		update_option( 'blog_public', '1');
		return;
		if($this->admin_post->status_de_desenvolvimento == "dev")return;
		update_option( 'blog_public', '1');

		// use global to call variable outside function
		global $wp_styles;
		// arrange the queue based on its dependency
		$wp_styles->all_deps($wp_styles->queue);	
		// The result
		$handles = $wp_styles->to_do;

		$path_img = $this->getURL("/");
		$css_code = '';
		$regex = array(
			"[\.\./]"=>'',
			"[images/]"=>$path_img,
			"`^([\t\s]+)`ism"=>'',
			"`^\/\*(.+?)\*\/`ism"=>"",
			"`([\n\A;]+)\/\*(.+?)\*\/`ism"=>"$1",
			"`([\n\A;\s]+)//(.+?)[\n\r]`ism"=>"$1\n",
			"`(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+`ism"=>"\n",
			"[\n]"=>''
		);
		echo "<style type='text/css'>\n";
		foreach ($handles as $handle){
			$src = strtok($wp_styles->registered[$handle]->src, '?');		
			if (strpos($src, 'http') !== false){
				$site_url = site_url();
				if (strpos($src, $site_url) !== false)
					$css_file_path = str_replace($site_url, '', $src);
				else
					$css_file_path = $src;
				$css_file_path = ltrim($css_file_path, '/');
			} 
			else{			
				$css_file_path = ltrim($src, '/');
			}
			
			if  (file_exists($css_file_path)) {
				
				ob_start();
				include($css_file_path);
				$tmp_css = ob_get_contents();

				
				ob_end_clean();
				$tmp_css = preg_replace(array_keys($regex),$regex,$tmp_css);
				echo "\n/* SD-".strtoupper($handle)." */\n";
				echo $tmp_css;
			}
		}
		echo "</style>\n";
		foreach ($handles as $handle){
			wp_deregister_style($handle);
		}
	}

	public function setAdmPost($params = array()){
		$this->admin_post = $params;
	}
	public function debug($exit = true){
		echo '<p style="margin:0; padding:0; color:#ff0000;">$this->view</p><pre style="padding:0; margin:0;">';
		print_r($this->view);
		echo '</pre><p style="margin:0; padding:0; color:#ff0000;">$this->admin_post</p>';
		echo "<pre style='padding:0; margin:0;'>";
		print_r($this->admin_post);
		echo '</pre><p style="margin:0; padding:0; color:#ff0000;">$this->post</p><pre style="padding:0; margin:0;">';
		print_r($this->post);
		echo '</pre>';
		echo '</pre><p style="margin:0; padding:0; color:#ff0000;">$_GET</p><pre style="padding:0; margin:0;">';
		print_r($_GET);
		echo '</pre>';
		echo '</pre><p style="margin:0; padding:0; color:#ff0000;">$_POST</p><pre style="padding:0; margin:0;">';
		print_r($_POST);
		echo '</pre>';
		if($exit)exit;
	}
	private function checkRender(){
		if (!file_exists($this->admin_post->file_template)){
			echo "FALHA NO CARREGAMENTO DO ARQUIVO [controller:{$this->admin_post->file_template}]";
			header('HTTP/1.0 404 Service Unavailable');
			exit;
		}
	}
	public function print_footer(){
		$post = get_page_by_path('thema-config',OBJECT,'page');
		$this->addANALYTICS($post);
		$this->_addJS();
	}
	public function print_head(){
		$post = get_page_by_path('thema-config',OBJECT,'page');
		$this->_addICON($post);
		$this->_addEXTRAS($post);

	}

	public function addJS($file_name, $order = null){
		if(!empty($file_name)){
			if(empty($order))$order = array($this->post_name());
			if (filter_var($file_name, FILTER_VALIDATE_URL) === false){
				$url = _URL_JS.$file_name.".js";
				wp_enqueue_script(strtolower(StringUtils::clear($file_name)), $url, $order, _NO_CACHE, true);
			}
			else{
				$url = $file_name;
				wp_enqueue_script(strtolower(StringUtils::clear($file_name)), $url, $order, _NO_CACHE, true);
			}
			
		}else echo "<!-- ARQUIVO [file:$file_name] não existe; -->\n";
	}

	public function addCSS($file_name, $order = null){
		if(!empty($file_name)){
			if(empty($order))$order = array($this->post_name());
			wp_enqueue_style(strtolower(StringUtils::clear($file_name)), _URL_CSS.$file_name.".css", $order, _NO_CACHE);
		}else echo "<!-- ARQUIVO [file:$file_name] não existe; -->\n";
	}

	private function _addJS(){
		wp_enqueue_script("scripts", _URL_JS."scripts.js", array("jquery-core"), _NO_CACHE, true);
		if($this->is_post()){
			wp_enqueue_script($this->post_name(), _URL_JS.$this->post_name()."/".strtolower($this->admin_post->action).".js", array("scripts"), _NO_CACHE, true);
		}else {
			wp_enqueue_script($this->post_name(), _URL_JS.strtolower($this->post_name()).".js", array("scripts"), _NO_CACHE, true);
		}
	}
	private function _addCSS(){
		wp_enqueue_style( "bootstrap", _URL_CSS."plugins/bootstrap-responsive.css", null, _NO_CACHE);

		$params = array(
			"ID"=>$this->admin_post->ID,
			"post_name"=>$this->admin_post->post_name,
			"post_type"=>$this->admin_post->post_type,
			"template_name"=>$this->admin_post->template_name,
			"action"=>$this->admin_post->action
		);
		$params = array_merge($params, $this->admin_post->vars);
		$params = http_build_query($params);
		
		if($this->is_post()){
			if(file_exists(_DIR_CSS.$this->post_name()."/default.css")){
				$name = 'default-archive';
				wp_enqueue_style($name, _URL_CSS.$this->post_name()."/default.css", array("style"), _NO_CACHE);
			}else{
				$name = 'style';
			}
			$name_style = $this->post_name();
			if($name_style == "login")$name_style = "thema-".$name_style;
			wp_enqueue_style($name_style, _URL_CSS.$this->post_name()."/".strtolower($this->admin_post->action).".css", array($name), _NO_CACHE);
		}else {
			$name_style = $this->post_name();
			if($name_style == "login")$name_style = "thema-".$name_style;
			wp_enqueue_style($name_style, _URL_CSS.strtolower($this->post_name()).".css", array("bootstrap"), _NO_CACHE);
		}
		wp_enqueue_style( "style", _URL_TEMPLATE."style.css?".$params, array(), _NO_CACHE);
	}
	private function _addSEO(){
		global $wpdb;
		$id = $wpdb->get_var("select ID from wp_posts where post_name='home' AND post_type='seo-estatico';");
		$type = "website";
		$page_link = get_bloginfo("url");
		$title = $site_name = get_bloginfo("name");
		

		if($this->admin_post->post_name != "Index"){
			$this->admin_post->seo = get_field("seo", $this->admin_post->ID);
			if($this->admin_post->action == "index"){//archive
				$id = $wpdb->get_var("select ID from wp_posts where post_name='blog' AND post_type='seo-estatico';");

			}else if($this->admin_post->action == "open"){//post_type/open
				$type = "article";
				$id = $this->post->ID;
			}
		}else $this->admin_post->seo = true;
		if(!empty($id)){
			$title = get_field("titulo", $id);
			$descricao = get_field("descricao", $id);
			$palavras_chave = get_field("palavras_chave", $id);
			$title_share = get_field("titulo_compartilhar", $id);
			$description_share = get_field("descricao_compartilhar", $id);
			$imagem_compartilhamento = get_field("imagem_compartilhamento", $id);
			if(!empty($imagem_compartilhamento)){
				$imagem_compartilhamento = $this->createIMG($id, $imagem_compartilhamento, 620, 325);
			}
		}
		
?>
<title><?php echo $title; ?></title>
<?php if(!empty($this->admin_post->google_site_verification)) : ?>
<meta name="google-site-verification" content="<?php echo $this->admin_post->google_site_verification; ?>"/>
<?php endif; ?>
<meta name="author" content="Sítio Digital" />
<meta http-equiv="content-language" content="pt-br" />
<meta name="copyright" content="© 2016 Sítio Digital" />
<meta name="robots" content="index, follow">
<?php if($this->admin_post->seo):
if(!$title){
	$args = array(
	  'name'        => 'home',
	  'post_type'   => 'seo-estatico'
	);
	$my_posts = get_posts($args);
	if(count($my_posts)){
		$my_posts = $my_posts[0];
	}
	$campos = get_fields($my_posts->ID);
	$title = $campos['titulo'];
	$descricao = $campos['descricao'];
	$palavras_chave = $campos['palavras_chave'];
	$title_share = $campos['titulo_compartilhar'];
	$imagem_compartilhamento = $campos['imagem_compartilhamento'];
	if(!empty($imagem_compartilhamento)){
		$imagem_compartilhamento = $this->createIMG($my_posts->ID, $imagem_compartilhamento, 620, 325);
	}
	$description_share = $campos['descricao_compartilhar'];
}
 ?>
<meta property="og:locale" content="pt_BR" />
<meta name="twitter:card" content="summary" />

<meta property="og:type" content="<?php echo $type; ?>"/>
<meta property="og:url" content="<?php echo $page_link; ?>"/>
<meta property="og:site_name" content="<?php echo $site_name; ?>"/>

<?php if(!empty($descricao)): ?>
<meta name="description" content="<?php echo $descricao; ?>"/>
<?php endif; ?>
<?php if(!empty($palavras_chave)): ?>
<meta name="keywords" content="<?php echo $palavras_chave; ?>"/>
<?php endif; ?>
<?php if(!empty($title_share)): ?>
<meta property="og:title" content="<?php echo $title_share; ?>"/>
<meta name="twitter:title" content="<?php echo $title_share; ?>" />
<?php endif; ?>
<?php if(!empty($imagem_compartilhamento)): ?>
<meta property="og:image" content="<?php echo $imagem_compartilhamento; ?>"/>
<?php endif; ?>
<?php if(!empty($description_share)): ?>
<meta property="og:description" content="<?php echo $description_share; ?>"/>
<meta name="twitter:description" content="<?php echo $description_share; ?>" />
<?php endif; ?>
<?php
endif;
?>
<?php 
	}
	private function addANALYTICS($post){
		global $wpdb;
		$google_analytics = $wpdb->get_var("select meta_value from wp_postmeta where post_id=".$post->ID." AND meta_key='google_analytics';");
		if(!empty($google_analytics)){
			echo "\n<script type='text/javascript'>\n";
			echo "//Google Analytics\n";
			echo stripcslashes($google_analytics)."\n";
			echo "//End Google Analytics\n";
			echo "</script>";
		}
	}
	
	private function _addEXTRAS($post){
		$google_site_verification = get_field("google_site_verification", $post->ID);
		if(!empty($google_site_verification))echo '<meta name="google-site-verification" content="'.$google_site_verification.'" />'."\n";
		echo '<base href="'._URL_THEME.'" />';
		//echo '<link rel="alternate" type="application/rss+xml" title="'.get_option("blogname").'" href="'._URL_THEME.'feed/" />';
		//echo '<link rel="alternate" type="application/atom+xml" title="'.get_option("blogname").'" href="'._URL_THEME.'feed/atom/" />';
		echo '<link rel="canonical" href="'._URL_THEME.'" />';
	}
	private function _addICON($post){
		$favicon = get_field("favicon", $post->ID);
		if(!empty($favicon)){
			foreach ($favicon as $key => $value) {
				echo "<".$value["tipo"]."href='".$value["favicon"]["url"]."'/>\n";
			}
		}
	}
	
	protected function is_ajax(){
		return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
	}
	protected function is_post(){
		return $this->admin_post->type == "post";
	}
	protected function is_page(){
		return $this->admin_post->type == "page";
	}
	protected function post_name(){
		return $this->admin_post->post_name;
	}
	protected function template_class(){
		return $this->admin_post->template_class;
	}
	protected function template_name(){
		return $this->admin_post->template_name;
	}
	private function _popularView(){
		if(empty($this->viewHTML)){
			ob_start();
			include($this->admin_post->file_template);
			$this->viewHTML = ob_get_contents();
			ob_end_clean();
		}
	}
	public function renderView(){
		$this->_popularView();
		echo $this->viewHTML;
	}
	public function renderHeader($name = null, $controller = null){


		$this->_popularView();
		$this->_addCSS();
		
		if($name == null){
			$file = _DIR_VIEWS."partial/header.php";
			$this->checkRender($file);
			switch ($this->admin_post->template_name) {
				case 'manutencao':
					header('HTTP/1.0 503 Service Unavailable');
					break;
				case 'error':
					header('HTTP/1.0 404 Service Unavailable');
					break;
				default:
					header("HTTP/1.1 200 OK");
					break;
			}

			
			require_once($file);
		}else{
			header("HTTP/1.1 200 OK");
			if($controller == null)$file = _DIR_VIEWS."partial/$name.php";
			else $file = _DIR_VIEWS.$controller."/partial/$name.php";
			$this->checkRender($file);
			require_once($file);
		}
	}
	public function renderFooter($name = null, $controller = null){
		if($name == null){
			$file = _DIR_VIEWS."partial/footer.php";
			$this->checkRender($file);
			require_once($file);
		}else{
			if($controller == null)$file = _DIR_VIEWS."partial/$name.php";
			else $file = _DIR_VIEWS.$controller."/partial/$name.php";
			$this->checkRender($file);
			require_once($file);
		}
	}
	/**
	 * $view_name_or_folder Nome do post dentro do criar admin;
	 * $partial_name Nome da parte dentro da pasta partial;
	 * $params Passa os parametros para o partial;
	 * ex: 	$view_name_or_folder = "newsletter";
	 * 		$partial_name = "meio";
	 * Isto significa que se $view_name_or_folder for do tipo page, o partial estará dentro do seguinte diretório:
	 * views/partial/meio.php
	 * Se view_name_or_folder for do tipo post, o partial estará dentro do seguinte diretório:
	 * views/newsletter/partial/meio.php
	 * Todo partial tem por padrão direito a um CSS e um JS dentro dos mesmos diretórios, mudando apenas a parta rais de views para js/css
	 */
	public function renderPartial($partial_name, $view_name_or_folder = null, $params = array()){
		if(empty($view_name_or_folder))$view_name_or_folder = strtolower($this->post_name());
		global $wpdb;
		$select = $wpdb->prepare("SELECT ID FROM wp_posts WHERE post_type='criar-admin' AND post_status='publish' AND post_name = %s ;", $view_name_or_folder);
		$ID = $wpdb->get_var($select);
		$type = get_field("tipo_de_post", $ID);
		$file = null;
		$file_css = null;
		$file_js = null;
		$part_name = null;
		$dependence = null;
		if($type == "post"){
			$part_name = "partial-".$view_name_or_folder."-".$partial_name;
			$file = _DIR_VIEWS.$view_name_or_folder."/partial/".$partial_name.".php";
			$file_css = $view_name_or_folder."/partial/".$partial_name.".css";
			$file_js = $view_name_or_folder."/partial/".$partial_name.".js";
		}else{
			$part_name = "page-partial-".$partial_name;
			$file = _DIR_VIEWS."partial/".$partial_name.".php";
			$file_css = "partial/".$partial_name.".css";
			$file_js = "partial/".$partial_name.".js";
		}
		if(count($params)){
			$this->partial = $params;
		}
		if($this->is_page()){
			$dependence = array($this->post_name());
		}
		else{
			if($view_name_or_folder == "index")$dependence = array($this->post_name());
			else $dependence = array($view_name_or_folder);
		}
		if(file_exists($file)){
			include($file);

			if(file_exists(_DIR_CSS.$file_css))wp_enqueue_style(StringUtils::clear($part_name), _URL_CSS.$file_css, $dependence, _NO_CACHE);
			if(file_exists(_DIR_JS.$file_js))wp_enqueue_script(StringUtils::clear($part_name), _URL_JS.$file_js, $dependence, _NO_CACHE, true);
		}
	}
	
	/**
	 * Cria o link com parametros
	 * Os parametros vao ser recebidos pelo get
	 * 
	 * Ex:
	 * $this->getLink(array('institucional','page' => 23,'tipo' => 'galeria'))
	 * 
	 * 
	 * @param unknown_type $params
	 */
	public function getLink($params = array()){
		$urlParams = '';
		if(empty($params) && !count($params))return "";
		$i = 0;
		foreach ($params as $key => $value){
			if(is_int($key)){
				$urlParams .= $value.($urlParams == '' && (count($params) - 1) == $i ? '' : '/');
			}else{
				$urlParams .= $key.'/'.$value.((count($params) - 1) == $i ? '' : '/');
			}
			$i++;
		}
		if($urlParams && $this->admin_post->post_name){
			return _URL_THEME.$urlParams;
		}else{
			return _URL_THEME;
		}
	}
	public function get_id_by_slug($slug_name){
		global $wpdb;
		$select = $wpdb->prepare("SELECT ID FROM wp_posts WHERE post_name = %s AND post_type <>'criar-admin' AND post_status='publish'", $slug_name);
		return $wpdb->get_var($select);
	}
	/**
	 * Recupera o link a partir do método $this->getLink e redireciona para ele.
	 * 
	 * Ex:
	 * $this->redirect(array('error'));
	 * 
	 * 
	 * @param unknown_type $params
	 */
	public function redirect($params = array()){
		$link = $this->getLink($params);
		header("Location: $link");
		exit;
	}
	
	public function getInstagram($dados = array())
	{
		// access_token = 407968476.5b9e1e6.ac684d350e5949ed8cff8a3053d216af
		// quantidade = count=3
		echo "https://api.instagram.com/v1/users/407968476/media/recent?access_token=".$dados['access_token']."";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, "https://api.instagram.com/v1/users/407968476/media/recent?access_token=".$dados['access_token']."");
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		foreach ($dados as $param => $value) {
		$parametros .= "&$param=$value";
		}
		$parametros = substr($parametros, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $parametros);
		//echo $form->ip;
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_TIMEOUT, 15);
		$retorno = curl_exec($curl);
		curl_close($curl);
		echo($retorno);
		//echo "foi";
		if ($retorno)
			return $retorno;
		else if ($retorno == "")
			return false;
	
	}

	public function no_logged(){
		if(function_exists("wp_get_current_user"))$user = wp_get_current_user();
		if(empty($user) || (!empty($user) && !in_array("administrator", $user->roles)))return true;
		return false;
	}
	
	public function no_logged_all(){
		if(function_exists("wp_get_current_user"))$user = wp_get_current_user();
		if(empty($user->data->ID))return true;
		return false;
	}
}