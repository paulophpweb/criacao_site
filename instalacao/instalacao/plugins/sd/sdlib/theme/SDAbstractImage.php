<?php

class SDAbstractImage{
	public function getURL($path_image){
		if(empty($path_image))return "";
		if(substr($path_image, 0,1)=="/")return _URL_IMG.substr($path_image."S", 1,-1);
		return _URL_IMG.$path_image;
	}
	/**
     * Redimensiona imagem\
     * @param Int $postID ID do post para que possa criar um diretório com este ID e quando remover o post remover as imagens relacionadas
     * @param String $image attachment do wordpress com a ID da imagem
     * @param Int OR Array $nova_largura valor em pixels da nova largura da imagem, o array deve ser neste exemplo array('160','920')
     * @param Int OR Array $nova_altura valor em pixels da nova altura da imagem, o array deve ser neste exemplo array('160','400')
     * @param String $tipo método para redimensionamento (padrão [vazio], preenchimento ou crop)
     * @param Int $qualidade Determina a qualidade da imagem
     * @param String $cor_preenchimento Caso a imagem seja cropada e não tenha proposção, ele cropa e coloca uma cor de preenchimento
     * @return Object com 
     **/
	public function createIMG($postID, $image, $nova_largura = 150, $nova_altura = 150, $tipo = 'crop', $qualidade=100, $cor_preenchimento = "#f1f1f1"){

		$rArray = array();
		if(is_array($nova_largura) && is_array($nova_altura)){
			foreach ($nova_largura as $key => $altura){
				$rArray[$nova_largura[$key].'x'.$nova_altura[$key]] = $this->createIMG($postID, $image,$nova_largura[$key],$nova_altura[$key],true);
			}
			return $rArray;
		}
		
		if(empty($image) || is_string($image)){
			$image = $this->createIMGTMP($postID, $image, $nova_largura, $nova_altura, "preenchimento", $qualidade, $cor_preenchimento);
		}else{

			$image["width"] = $nova_largura;
			$image["height"] = $nova_altura;
			unset($image["sizes"]);
			if($nova_largura==150 && $nova_altura==150){
				$image["url"] = $image["sizes"]["thumbnail"];
			}else{
				$file = get_attached_file($image["id"]);

				if(file_exists($file)){
					$new_dir_name = "personalize-image-post-id-".$postID;
					$dir = _DIR_UPLOAD.$new_dir_name;
					$this->createFolder($dir);
					$name_tmp = basename($file);
					list($name, $extensao) = explode(".", $name_tmp);
					$name = $name."-".$nova_largura."x".$nova_altura."-".$tipo.".".$extensao;
					$image["url"] = _URL_UPLOAD.$new_dir_name."/".$name;
					if(is_dir($dir)){
						
						$new_file_name = $dir.DIRECTORY_SEPARATOR.$name;
						
						if(!file_exists($new_file_name)){
							//Canvas::Instance($file)->hexa($cor_preenchimento)->redimensiona($nova_largura, $nova_altura, $tipo)->grava($new_file_name, $qualidade);
							$img = new canvas($file);

							$img->resize_crop_image($nova_largura, $nova_altura,$file,$new_file_name,$qualidade);
							
						}else return $image["url"];
					}else $this->erro();
				}else{
					//$image = $this->createIMGTMP($postID, basename($file), $nova_largura, $nova_altura, "preenchimento", $qualidade, $cor_preenchimento);
				}
			}
		}
		return $image["url"];
	}
	private function createIMGTMP($postID, $name, $nova_largura, $nova_altura, $tipo, $qualidade, $cor_preenchimento){
		$new_dir_name = "personalize-image-post-id-".$postID;
		$dir = _DIR_UPLOAD.$new_dir_name;
		$this->createFolder($dir);
		if(is_dir($dir)){
			list($name, $extensao) = explode(".", $name);
			$name = $name."-".$nova_largura."x".$nova_altura."-".$tipo.".".$extensao;
			$new_file_name = $dir.DIRECTORY_SEPARATOR.$name;

			$file = _DIR_PLUGIN."images".DIRECTORY_SEPARATOR."sitio-digital.png";
			if(file_exists($file)){
				if(!file_exists($new_file_name)){
					//Canvas::Instance()->hexa($cor_preenchimento)->novaImagem($nova_largura, $nova_altura)->marca($file, "meio", "centro")->grava($new_file_name, $qualidade);
				}
			}
			else $this->erro();
		}else $this->erro();
		return array(
			"id"=>$postID,
			"alt"=>"Imagem gerada pela API da Agência Sítio Digital",
			"title"=>"Imagem gerada pela API da Agência Sítio Digital",
			"caption"=>"Imagem gerada pela API da Agência Sítio Digital",
			"mime_type"=>"image/jpeg",
			"url"=>_URL_UPLOAD.$new_dir_name."/".$name,
			"width"=>$nova_largura,
			"height"=>$nova_altura,
		);
	}
	private function erro(){
		echo "Não foi possível criar o diretório $dir";
		exit;
	}
	private function createFolder($file){
		if(!is_dir($file)){
			mkdir($file, 0775);
			@chmod (dirname($file), 0775);
		}
	}
}