<?php
class SDInsert{
	private $post_ID;
	private $post_type;
	private $primary_key;
	private $primary_key_value;
	private $data_recorded = false;
	public function __construct($post_type, $primary_key = null, $primary_key_value = null){
		$this->post_type = $post_type;
		$this->primary_key = $primary_key;
		$this->primary_key_value = $primary_key_value;
		$this->is_insert = $this->_isInsert();
	}
	public function hasInsert(){
		return $this->is_insert;
	}
	private function _isInsert(){
		global $wpdb;
		if(!empty($this->primary_key)){
			$primary_key_is_table_post_or_meta = strpos($this->primary_key, "post_") === 0;
			if(!$primary_key_is_table_post_or_meta){
				$query = "SELECT DISTINCT(p.ID) as total FROM wp_posts as p, wp_postmeta as pm ";
				$query.= "WHERE p.ID=pm.post_id AND p.post_type = %s AND ";
				$query.= "pm.meta_key = %s AND pm.meta_value = %s ";
				$query.= "ORDER BY p.post_date DESC";
				$query = $wpdb->prepare($query, $this->post_type, $this->primary_key, $this->primary_key_value);
			}else{
				$query = "SELECT DISTINCT(ID) as total FROM wp_posts WHERE post_type = %s AND $this->primary_key = %s ;";
				$query = $wpdb->prepare($query, $this->post_type, $this->primary_key_value);
			}
			$this->post_ID = (int)$wpdb->get_var($query);
			return $this->post_ID > 0 ? false : true;
		}
		return true;
	}

	public function insert($col_key, $col_value){
		$primary_key_is_table_post_or_meta = strpos($col_key, "post_") === false;
		if($this->is_insert){
			if($primary_key_is_table_post_or_meta)$this->addMeta($col_key, $col_value);
			else $this->insertPost($col_key, $col_value);
		}else{		
			if($primary_key_is_table_post_or_meta)$this->addMeta($col_key, $col_value);
			else $this->updatePost($col_key, $col_value);
		}
	}
	public function result($msg_sucesso = "Dados inseridos com sucesso", $msg_erro = "Falha ao inserir os dados"){
		$result = new stdClass();
		$result->status = "erro";
		$result->mensagem = $msg_erro;

		if($this->data_recorded){
			$result->status = "sucesso";
			$result->mensagem = $msg_sucesso;
		}
		return $result;
	}
	private function addMeta($col_key, $col_value){
		add_post_meta($this->post_ID, $col_key, $col_value, true);
		$this->data_recorded = true;
	}

	private function insertPost($col_key, $col_value){
		$p = array(
		  $col_key   	 => $col_value,
		  'post_name'    => StringUtils::clear($col_value),
		  'post_status'  => 'publish',
		  "post_type"	 => $this->post_type
		);
		$this->post_ID = wp_insert_post($p);
		$this->data_recorded = true;
	}

	private function updatePost($col_key, $col_value){
		$p = array(
		  'ID'			=> $this->post_ID,
		  $col_key		=> $col_value,
		  'post_name'	=> StringUtils::clear($col_value),
		  "post_type"	=> $this->post_type
		);
		wp_update_post($p);
		$this->data_recorded = true;
	}
}