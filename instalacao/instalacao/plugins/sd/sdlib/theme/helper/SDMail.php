<?php
class SDMail{
	private $de;
	public $phpmailer;
	private $mensagem = "";
	public function __construct(){
		add_filter( 'wp_mail_from_name', array($this, "fromName"));
		add_filter( 'wp_mail_from', array($this, "fromMail"));
		add_action( 'phpmailer_init', array($this, 'phpmailer'));
	}
	public function phpmailer($mailer){
		foreach ($this->phpmailer as $key => $value) {
			$mailer->$key = $value;
		}
		$mailer->IsSMTP();
	}

    public function fromName(){
    	return get_bloginfo("name");
    }
    public function fromMail( $original_email_from ){
		return $this->de;
	}
	public function body($admin_post){
		global $wpdb;
		$_REQUEST["ano"] = date("Y");
		$this->mensagem = $wpdb->get_var("select meta_value as html from wp_posts as p, wp_postmeta as pm where p.ID = pm.post_id AND ID={$admin_post->ID} and meta_key='modelo_de_corpo_do_email'");
		foreach ($_REQUEST as $key => $value) {
			$this->mensagem = str_replace("{{".$key."}}", $value, $this->mensagem);
		}
		return $this;
	}
	
	public function send($para, $de, $titulo, $mensagem = null,$campos=null){
		if(!$campos)
			$post = get_page_by_path('thema-config',OBJECT,'page');
		if(empty($mensagem))$mensagem = $this->mensagem;
		$this->phpmailer = new stdClass;
		$this->phpmailer->Host = !$campos ? get_field("host", $post->ID) : $campos["host"];
		$this->phpmailer->Port = !$campos ? get_field("porta", $post->ID) : $campos["porta"];
		$this->phpmailer->Username = !$campos ? get_field("usuario", $post->ID) : $campos["usuario"];
		$this->phpmailer->Password = !$campos ? get_field("senha", $post->ID) : $campos["senha"];
		$this->phpmailer->FromName = get_bloginfo("name");
		$this->phpmailer->From = !$campos ? get_field("usuario", $post->ID) : $campos["usuario"];
		if(!$campos){
			$this->phpmailer->SMTPSecure = get_field("tipo_de_autenticacao", $post->ID) ? get_field("tipo_de_autenticacao", $post->ID) : '';
		}else{
			$this->phpmailer->SMTPSecure = $campos["tipo_de_autenticação"] ? $campos["tipo_de_autenticação"] : '';
		}
		$this->phpmailer->SMTPAuth = true;
		$this->phpmailer->SMTPDebug = false;
		
		$this->de = $de;
		$headers = "MIME-Version: 1.1\n";
		$headers .= "Content-type: text/html; charset=UTF-8\n";
		$headers .= "Reply-To: {$this->de}\n";
		
		$mail = wp_mail($para, $titulo, $mensagem,$headers);
		$result = new stdClass();
		if($mail){
			$result->status = "sucesso";
			$result->mensagem = "Mensagem enviada com sucesso";
		}else{
			$result->status = "erro";
			$result->mensagem =  "Falha ao enviar a mensagem";
		}
		//header("Content-Type: application/json", true);
		return $result;
	}
}