<?php
class SDArchive{
	public static $itens = array();
	public function __construct(){
		add_action('init', array($this, 'render'),100);
		add_action('admin_menu', array($this, 'render_menu'), 103);
	}

	public function render($e){
		global $current_user;
		foreach (SDArchive::$itens as $key => $value){
			$value->data["show_in_menu"] = ((
				($value->root && $current_user->ID == 1) ||
				!$value->root) ? $value->data["show_in_menu"] : false);
			
			register_post_type(StringUtils::clear($value->nome),$value->data);
		}
	}
	public function render_menu($e){
		$count = 6;
		foreach (SDArchive::$itens as $key => $value){
			$slug = StringUtils::clear($value->nome);
			if(!$value->data["capabilities"]["create_posts"])remove_submenu_page ('edit.php?post_type='.$slug, 'edit.php?post_type='.$slug);
		}
	}
	/**
	 * Metodo archive, serve para adicionar no admin uma nova área de tipo de post com type_post diferente.
	 * @param Array $itens = array(taxonomia:Array, icon_class:String, placeholder:String, show_in_menu:Boolean, nome:String, create_posts:Boolean, campos:Array);
	 * @param String $itens->taxonomia = Lista de taxonimia registrada para este post.
	 * @param String $itens->placeholder = Título do que vai no placeholder do campo post_title
	 * @param String $itens->show_in_menu = Indica se este post_type ira aparecer no menu lateral esquerdo do admin
	 * @param String $itens->nome = Nome do post_type
	 * @param String $itens->post_tag = Escolha se deseja ter supporte a nuvens de tags para este arquivo
	 * @param String $itens->root = Apenas o usuário root (usuário de ID=1) pode acessar este menu/conteúdo
	 * @param String $items->icon_class = Dashicon class representativa
	 * @param String $itens->create_posts = Indica se o usuário pode criar posts no admin para este post_type
	 * @param String $itens->campos = Campos padrão do post ex:Array('title', 'editor', 'comments', 'trackbacks', 'author', 'excerpt', 'custom-fields', 'thumbnail', "permalink")
	 * @return AddArchive
	 */
	public static function insert($item){
		$campos = Array('title');
		
		$item = (object)array_merge(array(
			"placeholder"=>"Digite o título aqui",
			"show_in_menu"=>true,
			"create_posts"=>true,
			"root"=>false,
			"post_tag"=>false,
			"taxonomia"=>array(),
			"campos"=>$campos),
		$item);

		$slug = StringUtils::clear($item->nome);
		$labels = array(
			'name' => _x($item->nome, 'post type general name'),
			'singular_name' => _x($item->nome, 'post type singular name'),
			'add_new' => _x('Adicionar Novo', $item->nome),
			'add_new_item' => __('Adicionar Novo '.$item->nome),
			'edit_item' => __('Editar '.$item->nome),
			'new_item' => __('Novo '.$item->nome),
			'all_items' => __('Listar '.$item->nome),
			'view_item' => __('Visualizar '.$item->nome),
			'search_items' => __('Procurar '.$item->nome),
			'not_found' =>  __('Não existe '.$item->nome),
			'not_found_in_trash' => __('Não existe '.$item->nome.' na lixeira'), 
			'parent_item_colon' => '',
			
			'menu_name' => __($item->nome)
		);
		$capabilities = array(
			'create_posts' => $item->create_posts

		);
		$rewrite = array(
			'slug' => $slug,
			'with_front' => false,
			'pages'      => true,
			'feeds'      => true,
			'ep_mask'    => EP_PERMALINK
		);
		
		$taxonomies = $item->post_tag ? array("post_tag") : array();
		if(!empty($item->taxonomia)){
			$taxonomies = array_merge($taxonomies, $item->taxonomia);
		}
		$group = new stdClass;
		$group->data = array(
			'description' 			=> $item->nome,
			'show_ui' 				=> true, 
			'exclude_from_search'   => false,
			'supports' 				=> $item->campos,
			'public' 				=> true,
			'capability_type' 		=> 'post',
			'hierarchical' 			=> false,
			'post_tag'              => true,
			'labels' 				=> $labels,
			'publicly_queryable' 	=> true,
			'capabilities' 			=> $capabilities,
			'map_meta_cap' 			=> true,
			'show_in_menu' 			=> $item->show_in_menu,
			'show_in_nav_menus'     => true,
			'show_in_admin_bar'     => true,
			'query_var' 			=> true,
			'can_export'            => true,
			'publicly_queryable'    => true,
			'taxonomies' 			=> $taxonomies,
			'rewrite'				=> $rewrite,
			'has_archive' 			=> true,
			"menu_icon"				=> $item->icon_class
		);
		$group->root = $item->root;
		$group->placeholder = $item->placeholder;
		$group->nome = $item->nome;
		self::$itens[] = $group;
		return $this;
	}
}
new SDArchive();
?>