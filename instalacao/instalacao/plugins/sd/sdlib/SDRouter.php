<?php 
class SDRouter{
	private $permissions = array("SDManutencao.css", "style.css");
	private $sitemaps = array("sitemap", "sitemaps", "sitemap_index", "index_sitemap", "sitemaps_index", "index_sitemaps");
	private $robots = array("rotobs");
	public function __construct(){
		$fn = basename($_SERVER['PHP_SELF']);

		
		//checa se os arquivos de css são dinamicos
		if(in_array($fn, $this->permissions)){
			header("HTTP/1.1 200 OK");
			header('Content-type: text/css');
			header('Cache-control: must-revalidate');

			if($fn == "SDManutencao.css"){
				require_once(_DIR_PLUGIN."sdlib/SDManutencao.php");
				$m = new SDManutencao();
				$m->readCSSManutencao();
			}
	    }
	    else if(function_exists("add_filter")){
			add_filter('template_redirect', array($this, 'template_render'));
			add_filter( 'mod_rewrite_rules', array($this, "rewrite_rules_sd"));
		}
	}

	public function template_render(){
		global $wp_query;

		$post = get_page_by_path('thema-config',OBJECT,'page');
		$status_de_desenvolvimento = get_field("status_de_desenvolvimento", $post->ID);
		
		$name = $wp_query->query_vars["pagename"];
		if(empty($name))$name = $wp_query->query_vars["name"];

	    $is_sitemaps = true;
	    $pos = strrpos($name, "-");
	    if(!empty($pos)){
	        $name = substr($name ,0, -4);
	        $n_name = explode("-", $name);
	        if(count($n_name)>1)$name = array_pop($n_name);
	        $n_name = implode("-", $n_name);
	    }else $is_sitemaps = false;

	    //Render sitemaps
	    if(in_array($name, $this->sitemaps) && $is_sitemaps){
	    	
	        require_once(_DIR_PLUGIN."sdlib/SDSitemaps.php");
	        $s = new SDSitemaps();
	        if(in_array($n_name, $this->sitemaps))$s->boot_main();
	        else $s->boot_internal($n_name);
	        exit;
	    }
	    
	    //Render robots
	    if(in_array($name, $this->robots) || $wp_query->query_vars["robots"] == 1){
	        require_once(_DIR_PLUGIN."sdlib/SDRobots.php");
	        $r = new SDRobots();
	        $r->boot($status_de_desenvolvimento == "dev");
	        exit;
	    }
	    $this->searchRouter();
	}
	private function no_logged(){
		global $current_user;
      	get_currentuserinfo();

      	// if($current_user == null || $current_user->user_email == null)return true;
      	// else{
      	// 	return false;
      	// }
		if(function_exists("wp_get_current_user"))$user = wp_get_current_user();
		if(empty($user)){//|| (!empty($user) && !in_array("administrator", $user->roles))
			return true;
		}
		return false;
	}
	private function searchRouter(){
		//REMOVENDO SCRIPTS INDESEJAVEIS EM MEU THEMA WP
		remove_trash();

		global $wp_query;
		global $wpdb;
		$vars = split("/", $_SERVER["REQUEST_URI"]);
		$vars = array_filter($vars, 'strlen');
		

		$r = new StdClass;
		$indefinido = array_shift($vars);
		if($indefinido == "login"){
			$this->renderDefault("Login", $vars);
			exit;
		}
		$pags_and_posts = $wpdb->get_results("SELECT * FROM wp_posts WHERE post_type='criar-admin' AND post_status='publish'");

		foreach ($pags_and_posts as $key => $value) {
			if($indefinido == $value->post_name){
				$type = get_field("tipo_de_post", $value->ID);
				$value->type = $type;
				$value->ajax = (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
				$value->template_name = ucfirst(strtolower(StringUtils::clear($value->post_name, "-")));
				$value->template_class = ucfirst(strtolower(StringUtils::clear($value->post_name, "_")));

				if($type=="post")$this->initPost($value, $vars);
				else{
					$value->template_name = ucfirst(strtolower(StringUtils::clear($value->post_name, "-")));
					$value->template_class = ucfirst(strtolower(StringUtils::clear($value->post_name, "_")));
					$value->action = "open";
					$this->initPage($value, $vars);
				}
				exit;
			}
		}

		$location_root = array("index", "home", "default", "padrao");
		if(empty($indefinido))$indefinido = "index";
		$this->renderDefault(in_array($indefinido, $location_root) ? "Index" : "Erro", $vars);
		exit;
	}
	
	private function initPage($post, $vars){
		$post->file_template = _DIR_VIEWS.strtolower($post->template_name).".php";
		$this->render($post,$vars);
	}
	
	private function initPost($post, $vars){
		$create_file_archive = get_field("create_file_archive", $post->ID);
		
		if(empty($create_file_archive) || count($create_file_archive) == 0)$this->renderDefault();
		else{
			$post->define_primary_post = false;
			if(count($vars) == 0){
				if(in_array("index", $create_file_archive)){
					$post->file_template = _DIR_VIEWS.strtolower($post->template_name)."/index.php";
					$post->action = "index";
				}else{
					$post->define_primary_post = true;
					$post->file_template = _DIR_VIEWS.strtolower($post->template_name)."/open.php";
					$post->action = "open";
				}
			}else{
				$post->file_template = _DIR_VIEWS.strtolower($post->template_name)."/open.php";
				$post->action = "open";
			}
			$exigir_senha = get_field("exigir_senha_post", $post->ID);
			if($exigir_senha){
				if($this->no_logged()){
					if(in_array($post->action == "open" ? "page" : "post", $exigir_senha)){
						$this->renderDefault("Login", $vars);
						exit;
					}

				}
			}

			$this->render($post,$vars);
		}
	}

	private function render($post, $vars){
		$file = _DIR_CONTROLLERS. "SD$post->template_class.php";
		if (file_exists($file) == false){
			echo "FALHA NO CARREGAMENTO DO ARQUIVO [router:$file]";
			header('HTTP/1.0 503 Service Unavailable');
		}else{
			global $wpdb;
			require_once($file);
			$name = "SD".$post->template_class;
			if($post->type == "post"){
				if(count($vars)==0){
					$search_post = $post->define_primary_post;
					$page_name_tmp = "open";
				}
				else{
					$search_post = true;
					$page_name_tmp = array_shift($vars);
				}
			}else{
				$search_post = true;
				$page_name_tmp = $post->post_name;
			}
			$post_sem_resultado = false;
			if($post->action == "open"){
				$data = null;
				if($search_post){
					if($post->define_primary_post){
						$select = $wpdb->prepare("select * from wp_posts where post_type = %s AND post_status = 'publish';", strtolower($post->template_class));
					}
					else{
						$select = $wpdb->prepare("select * from wp_posts where post_name= %s AND post_type <>'criar-admin' AND post_status = 'publish';", $page_name_tmp);
					}
					$data = $wpdb->get_row($select);
					$post_com_resultado = !empty($data);
				}
				
				if($post->type == "page" && count($vars)>0){
					$page_name_tmp = $vars[0];

					$page_name = "action_".strtolower(StringUtils::clear($page_name_tmp, "_"));
					if(!method_exists($name,$page_name))$page_name = "action_open";
					else array_shift($vars);
				}else{
					$page_name = "action_".strtolower(StringUtils::clear($page_name_tmp, "_"));

					if(!method_exists($name,$page_name)){
						if(!$post_com_resultado){
							$this->renderDefault();
							exit;
						}else{
							$page_name = "action_open";

						}
					}else{
						$new_template = _DIR_VIEWS.strtolower($post->template_name)."/index.php";

						if(!$post_com_resultado && file_exists($new_template)){
							$post->file_template = $new_template;
						}
					}
				}

				header("HTTP/1.1 200 OK");
				if($page_name == "action_index")$post->action = "index";
				$post->vars = $vars;
				$r = new $name($post);
				$r->post = $data;
				$r->$page_name();
			}else{
				
				header("HTTP/1.1 200 OK");
				$post->vars = $vars;
				$r = new $name($post);
				$r->post = new stdClass;
				$r->action_index();
			}
		}
	}
	private function renderDefault($template = "Erro", $vars = null){
		$post = new StdClass;
		$post->post_name = $post->template_name = $post->template_class = $template;
		$post->file_template = _DIR_VIEWS.strtolower($post->template_name).".php";
		$post->action = "open";
		$post->type = "page";
		$file = _DIR_CONTROLLERS. "SD$post->template_class.php";
		
		if (file_exists($file) == false){
			echo "FALHA NO CARREGAMENTO DO ARQUIVO [file:$file]";
			header('HTTP/1.0 503 Service Unavailable');
		}else{
			require_once($file);
			$name = "SD".$post->template_class;
			
			if($template != "Erro")header("HTTP/1.1 200 OK");
			
			if(count($vars)>0){
				$page_name_tmp = $vars[0];

				$page_name = "action_".strtolower(StringUtils::clear($page_name_tmp, "_"));
				if(!method_exists($name,$page_name))$page_name = "action_open";
				else array_shift($vars);
				$post->vars = $vars;

				$r = new $name($post);
				$r->$page_name();
			}else{
				$r = new $name($post);
				$r->action_open();
			}
		}
		exit;
	}
	public function rewrite_rules_sd($rules){
	    $rules .= "\n<FilesMatch '^style.css$|^SDManutencao.css$'>\n\tSetHandler php5-script\n</FilesMatch>";
	    return $rules;
	}
}
new SDRouter();