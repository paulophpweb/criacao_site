<?php
class SDSitemaps{
	public function __construct(){
		header("HTTP/1.1 200 OK");
		header("Content-type: text/xml");
		//header('Content-Type: text/plain');
	}
	
	public function boot_internal($var){
		echo '<?xml version="1.0" encoding="UTF-8"?>'
			."\n"
			.'<urlset '
			.'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '
			.'xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" '
			.'xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 '
			.'http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" '
			.'xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'
			."\n";
			if($var == "page")$this->sitemaps_page($var);
			else $this->sitemaps_single($var);
			echo "</urlset>";
	}
	
	public function boot_main(){
		echo '<?xml version="1.0" encoding="UTF-8"?>'
			."\n"
			.'<sitemapindex '
			.'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '
			.'xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd" '
			.'xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'
			."\n";

		$data = get_posts(array(
			"post_type"=>"criar-admin",
			"post_status"=>"publish",
			"posts_per_page"=>-1,
			"meta_query"=>array(
				'relation' => 'AND',
				array(
					'key' => 'tipo_de_post',
					'value' =>  "post",
					'compare' => '='
				),
				array(
					'key' => 'seo',
					'value' =>  "1",
					'compare' => '='
				)
			)
		));
		// Adicionando listagem de páginas
		$value = new stdClass;
		$value->post_name = "page";
		$date = new DateTime();
		$value->post_modified = $date->format('Y-m-d\TH:i:sP');
		
		$this->print_post($value);

		foreach ($data as $key => $value) {
			$this->print_post($value);
		}
		
		echo '</sitemapindex>';
	}
	private function sitemaps_page($var){
		$data = get_posts(array(
			"post_type"=>"criar-admin",
			"post_status"=>"publish",
			"posts_per_page"=>-1,
			"meta_query"=>array(
				'relation' => 'AND',
				array(
					'key' => 'tipo_de_post',
					'value' =>  "page",
					'compare' => '='
				),
				array(
					'key' => 'seo',
					'value' =>  "1",
					'compare' => '='
				)
			)
		));
		$value = new stdClass;
		
		//Neste caso estamos adicionando a pagina principal
		$value->prioridade = 1;
		$value->frequencia = "weekly";
		$value->post_type = "";
		$value->post_name = "";
		$this->print_single($value);

		//Adicionando demais páginas
		foreach ($data as $key => $value) {
			$value->prioridade = get_field("prioridade_de_indexacao", $value->ID);
			$value->frequencia = get_field("frequencia_de_postagem", $value->ID);
			$value->post_type = "";
			$this->print_single($value);
		}
		
	}
	private function sitemaps_single($var){
		$admin = get_posts(array(
			"post_type"=>"criar-admin",
			"name"=>$var
		));
		$admin = $admin[0];
		
		$prioridade = get_field("prioridade_de_indexacao", $admin->ID);
		$frequencia = get_field("frequencia_de_postagem", $admin->ID);
		$data = get_posts(array(
			"post_type"=>$var,
			"post_status"=>"publish",
			"posts_per_page"=>-1
		));
		foreach ($data as $key => $value) {
			$value->prioridade = $prioridade;
			$value->frequencia = $frequencia;
			$value->post_type .= "/";
			$this->print_single($value);
		}
	}
	private function print_single($p){
		$url = _URL_THEME.$p->post_type.$p->post_name;
		echo "\t<url>\n";
		echo "\t\t<loc>$url</loc>\n";
		echo "\t\t<changefreq>$p->frequencia</changefreq>\n";
		echo "\t\t<priority>$p->prioridade</priority>\n";
		echo "\t</url>\n";
	}

	private function print_post($p){
		$url =  _URL_THEME.$p->post_name."-sitemaps.xml";
		$date =  $p->post_modified;
		$date = new DateTime($date);
		$date = $date->format('Y-m-d\TH:i:sP');
		echo "<sitemap>\n";
		echo "\t<loc>$url</loc>\n";
		echo "\t<lastmod>$date</lastmod>\n";
		echo "</sitemap>\n";
		
	}
}