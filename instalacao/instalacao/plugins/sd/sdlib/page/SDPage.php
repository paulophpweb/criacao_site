<?php
class SDPage{
	public static $itens = array();
	public function __construct(){
		add_action('admin_menu', array($this, 'render_menu'), 102);
		add_action('init', array($this, 'render'),101);
		add_action('admin_footer', array($this, 'marcar_menu_admin_esquerdo'));
	}
	
	public function marcar_menu_admin_esquerdo() {
		global $wpdb;
		foreach (SDPage::$itens as $key => $value){
			$slug = StringUtils::clear($value->nome);
			$select = "select ID from wp_posts as p where post_type='".$value->post_type."' AND post_name='".$slug."' limit 1";
			$id = $wpdb->get_var($select);
			if($value->post_type == $_GET["post_type"] || $id == $_GET["post"]){
				?>
				<script>
					jQuery("#menu-posts-<?php echo $value->menu_group; ?>").removeClass("wp-not-current-submenu").addClass("wp-has-current-submenu");
					jQuery("#menu-posts-<?php echo $value->menu_group; ?> .wp-submenu a[href='post.php?post=<?php echo $_GET["post"]; ?>&action=edit'").css({"font-weight": 600, color:"#000000"});
				</script>
				<?php
			}
		}
	}
	public function render($e){
		global $wpdb;
		remove_action( 'homepage', 'storefront_homepage_content',10);
		
		foreach (SDPage::$itens as $key => $item){
			$slug = StringUtils::clear($item->nome);
			if(empty($item->id)){
				$select = "select ID from wp_posts as p where post_type='".$item->post_type."' AND post_name='".$slug."' limit 1";
				$id = $wpdb->get_var($select);
			}else{
				$select = "select ID from wp_posts as p where ID={$item->id}";
				$id = $wpdb->get_var($select);
			}
			if(empty($id)){
				$p = array(
				  'ID'			 => $item->id,
				  'post_title'   => $item->titulo,
				  'post_name'    => $slug,
				  'post_status'  => 'publish',
				  "post_type"	 => $item->post_type
				);
				$id = wp_insert_post( $p );
				$op = get_option("id-".$slug);
				if(empty($op))add_option("id-".$slug, $id);
				else update_option("id-".$slug, $id);
			}
			$item->id = $id;
		}
	}

	
	public function render_menu($e){
		$count = 6;
		
		global $current_user;
		foreach (SDPage::$itens as $key => $value){
			if(($value->root && $current_user->ID == 1) || !$value->root){
				if($value->show_in_menu){
					if($value->menu_group === false){
						if($value->id == $_GET["post"])$this->readStyle("#toplevel_page_post-post-{$_GET["post"]}-action-edit");
						add_menu_page($value->nome, $value->nome, 'edit_posts', "/post.php?post={$value->id}&action=edit",null, $value->icon_class, $count++);
					}
					else{
						if($value->id == $_GET["post"])$this->readStyle("#menu-posts-".$value->menu_group);
						add_submenu_page('edit.php?post_type='.$value->menu_group, '', $value->nome, 'edit_posts', "/post.php?post={$value->id}&action=edit", '');
					}
				}
			}
		}
	}

	/**
	 * Metodo archive, serve para adicionar no admin uma nova área de tipo de post com type_post diferente.
	 * @param Array $itens = array(icon_class:String, titulo:String, placeholder:String, show_in_menu:Boolean, nome:String, create_posts:Boolean, campos:Array, post_type:String, menu_group:String);
	 * @param String $itens->placeholder = Título do que vai no placeholder do campo post_title
	 * @param String $itens->titulo = Título da página criada
	 * @param String $itens->root = Apenas o usuário root (usuário de ID=1) pode acessar este menu/conteúdo
	 * @param String $itens->post_type = Indica onde será criado o post
	 * @param String $itens->menu_group = Indica dentro de qual post_type o menu será colocado no admin. OBS: Se o menu_group == false, ele será um menu principal
	 * @param String $itens->show_in_menu = Indica se este post_type ira aparecer no menu lateral esquerdo do admin
	 * @param String $itens->nome = Nome do post_name
	 * @param String $itens->post_tag = Escolha se deseja ter supporte a nuvens de tags para este arquivo
	 * @param String $itens->id = ID da pagina que deseja criar.
	 * @param String $items->icon_class = Dashicon class representativa
	 * @param String $itens->campos = Campos padrão do post ex:Array('title', 'editor', 'comments', 'trackbacks', 'author', 'excerpt', 'custom-fields', 'thumbnail', "permalink")
	 * @return AddPage
	 */
	public static function insert($item){
		$campos = Array("");
		$item = (object)array_merge(array(
			"placeholder"=>"Digite o título aqui",
			"show_in_menu"=>true,
			"post_type"=>"page",
			"menu_group"=>"page",
			"titulo"=>"",
			"id"=>null,
			"root"=>false,
			"post_tag"=>false,
			"campos"=>$campos),
		$item);
		if(!empty($item->id)){
			if(get_post_status($item->id) != "publish"){
				global $wpdb;
				$p = array(
				  'ID'			 => $item->id,
				  'post_title'   => $itens->nome,
				  'post_name'   => StringUtils::clear($itens->nome),
				  'post_status'  => 'publish',
				  "post_type"	 => "page"
				);
				$wpdb->insert("wp_posts", $p);
			}
		}
		self::$itens[] = $item;
		return $this;
	}
	public function readStyle($post_type){
		?>
			<style>
			<?php echo $post_type; ?>{
			    background-color:#a7cd45!important;
			}
			<?php echo $post_type; ?> .wp-menu-name{
			    color: #ffffff!important;
			}
			<?php echo $post_type; ?> > a ::after{
			    -moz-border-bottom-colors: none!important;
			    -moz-border-left-colors: none!important;
			    -moz-border-right-colors: none!important;
			    -moz-border-top-colors: none!important;
			    border-color: transparent #f1f1f1 transparent transparent!important;
			    border-image: none!important;
			    border-style: solid!important;
			    border-width: 8px!important;
			    content: " "!important;
			    height: 0!important;
			    margin-top: -8px!important;
			    pointer-events: none!important;
			    position: absolute!important;
			    right: 0!important;
			    top: 50%!important;
			    width: 0!important;
			}
			</style>
		<?php
	}
}
new SDPage();