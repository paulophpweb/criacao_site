<?php
class DateUtils{
	
    public $mes_sigla_en = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
	public $mes_sigla_pt = array("Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez");

	public $mes_normal_en = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
	public $mes_normal_pt = array("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");

	public $dia_sigla_en = array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
	public $dia_sigla_pt = array("Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb");

	public $dia_normal_en = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
	public $dia_normal_pt = array("Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado");
	public $mes = array(
		array("sigla"=>"Jan", "normal"=>"Janeiro"),
		array("sigla"=>"Fev", "normal"=>"Fevereiro"),
		array("sigla"=>"Mar", "normal"=>"Março"),
		array("sigla"=>"Abr", "normal"=>"Abril"),
		array("sigla"=>"Mai", "normal"=>"Maio"),
		array("sigla"=>"Jun", "normal"=>"Junho"),
		array("sigla"=>"Jul", "normal"=>"Julho"),
		array("sigla"=>"Ago", "normal"=>"Agosto"),
		array("sigla"=>"Set", "normal"=>"Setembro"),
		array("sigla"=>"Out", "normal"=>"Outubro"),
		array("sigla"=>"Nov", "normal"=>"Novembro"),
		array("sigla"=>"Dez", "normal"=>"Dezembro")
	);
	public $day = array(
		array("sigla"=>"Dom", "normal"=>"Domingo", "normal_longo"=>"Domingo"),
		array("sigla"=>"Seg", "normal"=>"Segunda", "normal_longo"=>"Segunda - Feira"),
		array("sigla"=>"Ter", "normal"=>"Terça", "normal_longo"=>"Terça - Feira"),
		array("sigla"=>"Qua", "normal"=>"Quarta", "normal_longo"=>"Quarta - Feira"),
		array("sigla"=>"Qui", "normal"=>"Quinta", "normal_longo"=>"Quinta - Feira"),
		array("sigla"=>"Sex", "normal"=>"Sexta", "normal_longo"=>"Sexta - Feira"),
		array("sigla"=>"Sáb", "normal"=>"Sábado", "normal_longo"=>"Sábado")
	);
	
	//Documentation http://php.net/manual/pt_BR/function.date.php
	public function __construct(){//2015-01-12 19:11:29
	    return $this;
	}

	public function format($vars){
	    $dateUtils = new DateUtils();
		$date = date_create($vars["post_date"]);
		$r =  date_format($date, $vars["format"]);//"Y/m/d H:i:s");
		
		$r = str_replace($dateUtils->dia_normal_en, $dateUtils->dia_normal_pt, $r);
		$r = str_replace($dateUtils->dia_sigla_en, $dateUtils->dia_sigla_pt, $r);
		
		$r = str_replace($dateUtils->mes_normal_en, $dateUtils->mes_normal_pt, $r);
		$r = str_replace($dateUtils->mes_sigla_en, $dateUtils->mes_sigla_pt, $r);

		return $r;
	}
}
?>