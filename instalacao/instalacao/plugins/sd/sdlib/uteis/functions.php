<?php

function sd_autoload($class_name) {
    if(file_exists(_DIR_PLUGIN."sdlib".DIRECTORY_SEPARATOR."theme".DIRECTORY_SEPARATOR."helper".DIRECTORY_SEPARATOR.$class_name.'.php'))
    include(_DIR_PLUGIN."sdlib".DIRECTORY_SEPARATOR."theme".DIRECTORY_SEPARATOR."helper".DIRECTORY_SEPARATOR.$class_name.'.php');
}
spl_autoload_register("sd_autoload");

//REMOVENDO SCRIPTS INDESEJAVEIS EM MEU THEMA WP
function remove_trash(){
    show_admin_bar(false);
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    wp_deregister_script( 'wp-embed.min.js' );
    wp_deregister_script( 'wp-embed' );
    wp_deregister_script( 'embed' );
}
remove_trash();

//MUDANDO O COPY NO RODAPÉ DO ADMIN
function remove_footer_admin () {
  echo '&copy; 2016 / '.(date("Y")).'- Sítio Digital';
}
add_filter('admin_footer_text', 'remove_footer_admin');



//ADICIONANDO STYLE NO ADMIN
function safely_add_stylesheet_to_admin() {
     wp_enqueue_style( 'custom-login', _URL_PLUGIN . 'css/admin.css', null, _NO_CACHE);
     wp_enqueue_style( 'dashicons-picker', _URL_PLUGIN . 'css/dashicons-picker.css', null, _NO_CACHE);
     wp_enqueue_script( 'dashicons-picker', _URL_PLUGIN. 'js/dashicons-picker.js', null, _NO_CACHE);
     wp_enqueue_script( 'admin', _URL_PLUGIN. 'js/admin.js', null, _NO_CACHE);
     
}
add_action( 'admin_enqueue_scripts', 'safely_add_stylesheet_to_admin' );

//URL LOGO LOGIN
function my_login_logo_url() {
    return "https://www.agenciasitiodigital.com.br";
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

//TITLE LOGO LOGIN
function my_login_logo_url_title() {
    return 'SD Soluções Interativas';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );

//REMOVER ITEMS DA BARRA DE ADMIN
function wps_admin_bar() {
    global $wp_admin_bar;
    $wp_admin_bar->add_menu( array(
        'id'    => 'wp-logo',
        'title' => '<span class="ab-icon"></span>',
        'href'  => "https://www.agenciasitiodigital.com.br",
        'meta'  => array(
            'target'=>"_blank",
            'title' => __('SD Soluções Interativas'),
        )
    ));
   
    $wp_admin_bar->remove_menu('about');
    $wp_admin_bar->remove_menu('comments');
    $wp_admin_bar->remove_menu('wporg');
    $wp_admin_bar->remove_menu('documentation');
    $wp_admin_bar->remove_menu('support-forums');
    $wp_admin_bar->remove_menu('feedback');
    $wp_admin_bar->remove_menu('view-site');
    $wp_admin_bar->remove_menu('new-content');
    $wp_admin_bar->remove_menu('options-general');
    $wp_admin_bar->remove_menu('configurar');
    $wp_admin_bar->remove_menu('users');

    $id = $_GET["post"];
    if(!empty($id)){
        global $wpdb;
        $p = $wpdb->get_row("SELECT post_name, post_type FROM wp_posts WHERE ID=".$id);
        $slugs_remove = array("thema-config", "thema-scripts", "criar-admin", "canais");
        $types_remove = array("criar-admin");
        if(in_array($p->post_name, $slugs_remove) || in_array($p->post_type, $types_remove))$wp_admin_bar->remove_menu('view');
    }else{
        $wp_admin_bar->add_menu( array(
            'id'    => 'view',
            'meta'  => array(
                'target'=>"_blank"
            )
        ));
    }
    $wp_admin_bar->add_menu( array(
        'id'    => 'site-name',
        'meta'  => array(
            'target'=>"_blank"
        )
    ));
    
}
add_action( 'wp_before_admin_bar_render', 'wps_admin_bar' );


//REMOVER ITEMS DA DASHBOARD
function remove_links_menu() {
    remove_menu_page('index.php'); // Dashboard
    
    remove_menu_page('upload.php'); // Media
    remove_menu_page('link-manager.php'); // Links
    remove_menu_page('edit-comments.php'); // Comments
    remove_menu_page('tools.php'); // Tools
    remove_menu_page('edit.php'); // Posts
    remove_menu_page('edit.php?post_type=page'); // Pages
    

    global $submenu;
    unset($submenu['options-general.php'][15]); // Removes 'Writing'.
    unset($submenu['options-general.php'][25]); // Removes 'Discussion'.
    unset($submenu['options-general.php'][30]); // Removes 'Mídia'.

    unset($submenu['plugins.php'][15]);// Remove Editor de plugib

    $current_user = wp_get_current_user();
    if($current_user->ID != 1){
        if($current_user->ID != 1 && $current_user->user_login != "megasoft"){
            remove_menu_page('themes.php'); // Appearance
            remove_menu_page('plugins.php'); // Plugins
            remove_menu_page('edit.php?post_type=acf');
            remove_menu_page('post.php?post=98&action=edit'); // Retirar o menu configurar
            remove_menu_page('users.php'); // Retirar o menu usuário
            remove_menu_page('options-general.php'); // Configuracoes
        }else if($current_user->ID == 1 && $current_user->user_login != "sitiodigital"){
            remove_menu_page('edit.php?post_type=criar-admin'); // Retirar o criar admin
            remove_menu_page('plugins.php'); // Retirar o criar admin
            remove_menu_page('edit.php?post_type=acf'); // Retirar o criar admin
            remove_menu_page('options-general.php'); // Configuracoes
            remove_menu_page('themes.php'); // Appearance
            remove_menu_page('edit.php?post_type=acf');
        }else if($current_user->user_login = "megasoft"){
            remove_menu_page('plugins.php'); // Retirar o criar admin
            remove_menu_page('options-general.php'); // Configuracoes
            remove_menu_page('themes.php'); // Appearance
            remove_menu_page('edit.php?post_type=acf');
        }
    }
}

add_action( 'admin_menu', 'remove_links_menu', 999);

//REMOVER EDITOR DO MENU DE NAVEGAÇÃO
function remove_editor_menu() {
  remove_action('admin_menu', '_add_themes_utility_last', 101);
}
 
add_action('_admin_menu', 'remove_editor_menu', 1);


//MOSTRAR A DASHBOARD NUMA SÓ COLUNA
function single_screen_columns( $columns ) {
    $columns['dashboard'] = 1;
    return $columns;
}
function single_screen_dashboard(){return 1;}
add_filter( 'screen_layout_columns', 'single_screen_columns' );
add_filter( 'get_user_option_screen_layout_dashboard', 'single_screen_dashboard' );

//ESCONDER OPÇÃO DE COR NOS PERFIS
function admin_color_scheme() {
   global $_wp_admin_css_colors;
   $_wp_admin_css_colors = 0;
}
function change_admin_color($result) {
    return 'light';
}
add_action('admin_head', 'admin_color_scheme');
add_filter('get_user_option_admin_color', 'change_admin_color');

//Remover itens do dashboard
function remove_dashboard_meta() {
        remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_primary', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
}
add_action( 'admin_init', 'remove_dashboard_meta' );


//Não dar dica se o usuário está correto.
 function msg_error_login($a){
    return "<strong>ERRO</strong>: Usuário e/ou senha Inválido.";
 }
 add_filter('login_errors','msg_error_login');





function layers_child_dashboard_widgets() {
  wp_add_dashboard_widget('my_dashboard_widget', 'SD Soluções Interativas', 'layers_child_theme_info');
}
 
add_action('wp_dashboard_setup', 'layers_child_dashboard_widgets' );
 
function layers_child_theme_info() {
  echo '<ul>
    <li><strong>Desenvolvido por :</strong> Sítio Digital</li>
    <li><strong>Website :</strong> <a href="https://www.sitiodigital.com.br" target="_blank">https://www.sitiodigital.com.br</a></li>
    <li><strong>Email :</strong>  <a href="mailto:contato@sitiodigital.com.br">contato@sitiodigital.com.br</a></li>
    <li><strong>Telefone :</strong>  <a href="tel:06230972656">(62) 3097.2656</a></li>
  </ul>';
}

// remove os feeds
remove_action ('wp_head', 'rsd_link');
remove_action ('wp_head', 'wlwmanifest');
remove_action( 'wp_head', 'wlwmanifest_link');
remove_action( 'wp_head', 'wp_shortlink_wp_head');
remove_action( 'wp_head', 'rest_output_link_wp_head');
remove_action('wp_head', 'wp_generator');

//ADICIONANDO PATH DE URL DO THEMA
function theme_url($concat = ""){
    return get_template_directory_uri()."/".$concat;
}

function is_login(){
    $id = get_option("id-thema-config");
    $value = get_post_meta($id, "router_admin", true);
    if(empty($value))return false;
    if(strpos($_SERVER["REQUEST_URI"], $value))return true;
    return false;
}

function smartCopy($source, $dest, $options=array('folderPermission'=>0755,'filePermission'=>0755)){ 
    $result=false;
    if (is_file($source)){
        if ($dest[strlen($dest)-1]=='/'){
            if (!file_exists($dest)) { 
                cmfcDirectory::makeAll($dest,$options['folderPermission'],true); 
            } 
            $__dest=$dest."/".basename($source); 
        } else { 
            $__dest=$dest; 
        } 
        $result=copy($source, $__dest); 
        chmod($__dest,$options['filePermission']); 
        
    } elseif(is_dir($source)) { 
        if ($dest[strlen($dest)-1]=='/') { 
            if ($source[strlen($source)-1]=='/') { 
                //Copy only contents 
            } else { 
                $dest=$dest.basename($source); 
                @mkdir($dest); 
                chmod($dest,$options['filePermission']); 
            } 
        } else { 
            if ($source[strlen($source)-1]=='/') { 
                @mkdir($dest,$options['folderPermission']); 
                chmod($dest,$options['filePermission']); 
            } else { 
                @mkdir($dest,$options['folderPermission']); 
                chmod($dest,$options['filePermission']); 
            } 
        } 

        $dirHandle=opendir($source); 
        while($file=readdir($dirHandle)){
            if($file!="." && $file!=".."){
                if(!is_dir($source."/".$file)){
                    $__dest=$dest."/".$file; 
                }else{
                    $__dest=$dest."/".$file; 
                } 
                $result=smartCopy($source."/".$file, $__dest, $options); 
            } 
        } 
        closedir($dirHandle); 
    } else { 
        $result=false; 
    } 
    return $result; 
}
function SD_get_field($name, $post_id){
    global $wpdb;
    $select = "select pm.meta_value as $name from wp_posts as p, wp_postmeta as pm where p.ID=pm.post_id and ID = $post_id and meta_key='$name';";
    return $wpdb->get_row($select);
}
function deletedir($dir)
    {
        if (is_dir($dir))
        { 
            $files = scandir($dir); 
            foreach ($files as $file)
            { 
                if ($file != "." && $file != "..")
                { 
                    if (filetype($dir."/".$file) == "dir")
                    {
                        $this->deletedir($dir."/".$file);
                    }
                    else
                    {
                        unlink($dir."/".$file); 
                    }
                } 
            } 
            reset($objects); 
            if(rmdir($dir))
            {
                return 'deleted successfully!';
            }
            else
            {
                return 'delete failed!';
            }
        } 
        else
        {
            return 'doesn\'t exist or inaccessible!';
        }
    }



// Bootstrap pagination function

function wp_bs_pagination($pages = '', $range = 4)

{  

     $showitems = ($range * 2) + 1;  

 

     global $paged;

     if(empty($paged)) $paged = 1;

 

     if($pages == '')

     {

         global $wp_query; 

         $pages = $wp_query->max_num_pages;

         if(!$pages)

         {

             $pages = 1;

         }

     }   

 

     if(1 != $pages)

     {
        echo '<div class="text-center">'; 
        echo '<nav><ul class="pagination">';

         if($paged > 1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged - 1)."' aria-label='Anterior'>&lsaquo;<span class='hidden-xs'> Anterior</span></a></li>";

 

         for ($i=1; $i <= $pages; $i++)

         {

             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))

             {

                 echo ($paged == $i)? "<li class=\"active\"><span>".$i." <span class=\"sr-only\">(autual)</span></span>

    </li>":"<li><a href='".get_pagenum_link($i)."'>".$i."</a></li>";

             }

         }

 

         if ($paged < $pages && $showitems < $pages) echo "<li><a href=\"".get_pagenum_link($paged + 1)."\"  aria-label='Próxima'><span class='hidden-xs'>Próxima </span>&rsaquo;</a></li>";  


         echo "</ul></nav>";
         echo "</div>";
     }

}