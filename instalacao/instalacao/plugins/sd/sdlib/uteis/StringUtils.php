<?php
class StringUtils{
	public static function clear($str, $separador = "-"){
		$str = trim($str);
		$n = utf8_decode("ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿ!@#$%ˆ&*(),./;:=");
		$s = "SOZsozYYuAAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiionoooooouuuuyy----------------";
		$str = strtr(utf8_decode($str),$n,$s);
		$str = strtr($str,"ABCDEFGHIJKLMNOPQRSTUVWXYZ","abcdefghijklmnopqrstuvwxyz"); 
		$str = preg_replace('#([^.a-z0-9]+)#i', '-', $str); 
		$str = preg_replace('#-{2,}#','-',$str); 
		$str = preg_replace('#-$#','',$str); 
		$str = preg_replace('#^-#','',$str);
		$str = str_replace("-",$separador,$str);
		return $str; 
	}
	
    public static function limite($texto, $limite){
    	  $contador = strlen($texto);
          if ( $contador >= $limite ) {      
              $texto = mb_substr($texto, 0, $limite,"UTF-8"). '...';
              return $texto;
          }
          else{
            return $texto;
          }
	}
}
?>