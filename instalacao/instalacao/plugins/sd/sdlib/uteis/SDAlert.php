<?php
class SDAlert{
	public static $list = array();
	public function __construct(){
		if(is_admin()){
			add_action('admin_notices', array($this, "admin_notices"), 190);
		}
	}
	public function admin_notices(){
		foreach (SDAlert::$list as $key => $value) {
			$this->render($value);
		}
	}
	private function render($obj){
		if($obj->error){
			echo '<div id="message" class="error">';
		}else{
			echo '<div id="message" class="updated fade">';
		}
		echo "<p>{$obj->msg}</p></div>";
	}

	public static function insert($msg, $error = true){
		$item = new stdClass;
		$item->msg = $msg;
		$item->error = $error;
		self::$list[] = $item;
		return $this;
	}
}
new SDAlert();