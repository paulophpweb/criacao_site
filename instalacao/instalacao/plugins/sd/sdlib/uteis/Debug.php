<?php 
class Debug{
	public static function toString($str, $exit = true){
		echo "<pre>";
		print_r($str);
		echo "</pre>";
		if($exit)exit;
	}
}