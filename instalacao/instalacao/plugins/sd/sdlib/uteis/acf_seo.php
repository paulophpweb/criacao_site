<?php

class acf_seo{
	private $location = array();
	public function addPage($id){
		$this->location[] = array(array (
			'param' => 'page',
			'operator' => '==',
			'value' => $id,
			'order_no' => 0,
			'group_no' => 1,
		));
	}
	public function addPostType($name){
		$this->location[] = array(array (
			'param' => 'post_type',
			'operator' => '==',
			'value' => $name,
			'order_no' => 0,
			'group_no' => 0,
		));
	}
	public function render(){
		
		register_field_group(array(
			'id' => 'acf_seo',
			'title' => 'Seo',
			'fields' => array (
				array (
					'key' => 'field_5814ba3a5d884',
					'label' => 'SEO',
					'name' => '',
					'type' => 'tab',
				),
				array (
					'key' => 'field_5813a59495a09',
					'label' => 'Título site',
					'name' => 'titulo',
					'type' => 'text',
					'instructions' => 'Digite o título que deseja para está página com no máximo 65 caracteres.',
					'required' => false,
					'default_value' => '',
					'placeholder' => 'Título',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => 65,
				),
				array (
					'key' => 'field_5813a5c395a0a',
					'label' => 'Descrição site',
					'name' => 'descricao',
					'type' => 'text',
					'instructions' => 'Faça um resumo do conteúdo desta página com no máximo 165 caracteres.',
					'required' => false,
					'default_value' => '',
					'placeholder' => 'Descrição',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => 165,
				),
				array (
					'key' => 'field_5813a5f395a0b',
					'label' => 'Palavras Chave Site',
					'name' => 'palavras_chave',
					'type' => 'text',
					'instructions' => 'Digite as principais palavras chave que esta página possui,	separando-as por virgula (,).',
					'required' => false,
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_5814ba595d885',
					'label' => 'Share',
					'name' => '',
					'type' => 'tab',
				),
				array (
					'key' => 'field_5814b8599d125',
					'label' => 'Título compartilhar',
					'name' => 'titulo_compartilhar',
					'type' => 'text',
					'instructions' => 'Digite um título que será utilizado ao compartilhar nas redes sociais',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_5814b86e9d126',
					'label' => 'Descrição compartilhar',
					'name' => 'descricao_compartilhar',
					'type' => 'text',
					'instructions' => 'Digite a descrição do conteúdo que será utilizado ao compartilhar nas redes sociais',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_5813a69195a0c',
					'label' => 'Imagem compartilhar',
					'name' => 'imagem_compartilhamento',
					'type' => 'image',
					'instructions' => 'Selecione uma imagem que possa representar esta página nos canais digitais. Lembrando que está imagem deve ter o tamanho de 620x325 pixels e 72 DPI de resolução',
					'required' => false,
					'save_format' => 'object',
					'preview_size' => 'thumbnail',
					'library' => 'uploadedTo',
				),
			),
			'location' => $this->location,
			'options' => array (
			'position' => 'normal',
				'layout' => 'no_box',
				'hide_on_screen' => array (
					0 => 'permalink',
					1 => 'the_content',
					2 => 'excerpt',
					3 => 'custom_fields',
					4 => 'discussion',
					5 => 'comments',
					6 => 'revisions',
					7 => 'slug',
					8 => 'author',
					9 => 'format',
					10 => 'featured_image',
					11 => 'categories',
					12 => 'tags',
					13 => 'send-trackbacks',
				),
			),
			'menu_order' => 50,
		));
	}
}