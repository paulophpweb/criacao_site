<?php
class SDRobots{
	public function boot($is_development = false){
		header("HTTP/1.1 200 OK");
		header('Content-Type: text/plain');
		echo "User-agent: *\n";
		if($is_development){
			echo "Disallow: /";
		}else{
			echo "Disallow: /feed/\n";
			echo "Disallow: /trackback/\n";
			echo "Disallow: /wp-admin/\n";
			echo "Disallow: /sd-admin/\n";
			echo "Disallow: /wp-content/\n";
			echo "Disallow: /wp-includes/\n";
			echo "Disallow: /xmlrpc.php\n";
			echo "Disallow: /wp-\n";
			echo "Allow: /wp-content/uploads/\n";
			echo "Sitemap: "._URL_THEME."sitemap.xml";
		}
	}
}