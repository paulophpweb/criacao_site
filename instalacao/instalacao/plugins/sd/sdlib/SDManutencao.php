<?php
class SDManutencao{
	public function read503(){
		//Define que o wordpress pode atualizar os plugins e thema sozinho. Isto não pode ficar ativado quando o site estiver online.
		//Não se esqueça de dar permissão no projeto chmod -R 777 {path url} e antes de subir o projeto, voltar para 644
		define('FS_METHOD', 'direct');
		$file503 = get_template_directory() . '/views/manutencao.php';
		if (file_exists($file503) == false){
			$file503 = dirname(__FILE__) . '/<b>modelo</b>/503.php';
			header('HTTP/1.0 503 Service Unavailable');
		}else{
			include($file503);
		}
		exit;
	}
	public function readCSSManutencao(){
		header('Content-type: text/css');
		header('Cache-control: must-revalidate');
		$post = get_page_by_path('thema-config',OBJECT,'page');
		global $wpdb;

		$bg_desktop_manutencao = $wpdb->get_var("select pm.meta_value from wp_posts as p, wp_postmeta as pm where p.ID=pm.post_id and post_name='thema-config' and meta_key='bg_desktop_manutencao';");
		$bg_mobile_manutencao = $wpdb->get_var("select pm.meta_value from wp_posts as p, wp_postmeta as pm where p.ID=pm.post_id and post_name='thema-config' and meta_key='bg_mobile_manutencao';");
		if(empty($bg_desktop_manutencao)){
			$bg_desktop_manutencao = array(
				"url"=>plugins_url("sd/images/bg-desktop-construcao.jpg")
			);
		}else{
			$bg_desktop_manutencao = array(
				"url"=>$wpdb->get_var("select guid from wp_posts where ID=$bg_desktop_manutencao")
			);
		}
		if(empty($bg_mobile_manutencao)){
			$bg_mobile_manutencao = array(
				"url"=>plugins_url("sd/images/bg-mobile-construcao.jpg")
			);
		}else{
			$bg_mobile_manutencao = array(
				"url"=>$wpdb->get_var("select guid from wp_posts where ID=$bg_mobile_manutencao")
			);
			
		}
		?>
body{
	background-image: url(<?php echo $bg_desktop_manutencao["url"]; ?>);
}
@media (max-width: 699px){
	body{
		background: url(<?php echo $bg_mobile_manutencao["url"]; ?>) center center fixed;
	}
}
<?php
	}
}
new SDManutencao();