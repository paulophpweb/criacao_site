<?php
class SDThemaConfig{
	private $post_name;
	private $post_ID;
	private $post_oud;

	public function __construct(){
		add_action('acf/save_post', array($this, 'save_post'));
	}
	public function save_post($post_id){
		$id = get_option("id-thema-config");
		if($_POST["post_type"] == "criar-admin"){
			flush_rewrite_rules();
			$this->post_name = $_POST["post_name"] = StringUtils::clear($_POST["post_title"]);
			$this->post_id = $post_id;
			$this->post_oud_name = get_post($post_id);
			$this->post_oud_name = $this->post_oud_name->post_name;
			//Garantindo que o slug não ira ter acentos e caracteres indesejaveis 
			wp_update_post(array('ID' => $this->post_id,'post_name' => $this->post_name));
			$tipo_de_post = get_field("tipo_de_post", $this->post_id);

			//Decidindo que tipo de arquivo será criado;
			if($tipo_de_post == "post")$this->createFilePost();
			else $this->createFilePage();
		}else if($post_id == $id){
			$this->updateRoltersSQL($id);
			
	    }
	}
	
	private function createFilePage(){
		$criar_arquivos_da_pagina = get_field("criar_arquivos_da_pagina", $this->post_id);
		if($criar_arquivos_da_pagina){

			$c = $this->getData($this->post_name, _DIR_PLUGIN."code-sample/js.js");
			$this->createItemFilePage(_DIR_THEME_SD."js/",$this->post_name,".js", $c->data);
			
			$this->createItemFilePage(_DIR_THEME_SD."css/",$this->post_name,".css");
			
			$c = $this->getData($this->post_name, _DIR_PLUGIN."theme-sample/index.php");
			$this->createItemFilePage(_DIR_VIEWS,$this->post_name,".php", $c->data);
			
			$c = $this->getData($this->post_name, _DIR_PLUGIN."code-sample/page.php");
			$this->createItemFilePage(_DIR_CONTROLLERS,$c->name, ".php", $c->data, $c->name_oud);
		}
	}

	private function createItemFilePage($path, $name, $extensao, $data = "", $name_oud = null){
		$file = $path.(empty($name_oud) ? $this->post_oud_name : $name_oud).$extensao;
		$this->createFolder($file);

		if(!file_exists($file)){
			$this->createFile($path.$name.$extensao, $data);
		}else{
			$is_css = strrpos($name, ".css") === false;
			$is_js = strrpos($name, ".js") === false;
			if($extensao != ".js" && $extensao != ".css"){
				$myfile = fopen($file, "r") or die("Unable to open file!");
				$data = fread($myfile, filesize($file));
				fclose($myfile);
				$myfile = @fopen($file, "w+");
				if(!empty($myfile)){
					$data = str_replace($name_oud,$name,$data);
					fwrite($myfile, $data);
					fclose($myfile);
				}
			}
			rename($file, $path.$name.$extensao);
		}
	}

	private function createFilePost(){
		$create_file_archive = get_field("create_file_archive", $this->post_id);
		if(!empty($create_file_archive) && count($create_file_archive)>0){
			
			foreach ($create_file_archive as $key => $value) {
				$c = $this->getData($this->post_name, _DIR_PLUGIN."code-sample/js.js");
				$this->createItemFile(_DIR_THEME_SD."js/",$this->post_name,"/".$value.".js", $c->data);
				$this->createFolder(_DIR_THEME_SD."js/".$this->post_name."/partial/".$value.".js");

				//echo _DIR_THEME_SD."css/",$this->post_name,"/".$value.".css; ";
				$this->createItemFile(_DIR_THEME_SD."css/",$this->post_name,"/".$value.".css");
				$this->createFolder(_DIR_THEME_SD."css/".$this->post_name."/partial/".$value.".css");
				
				$c = $this->getData($this->post_name, _DIR_PLUGIN."theme-sample/index.php");
				$this->createItemFile(_DIR_VIEWS,$this->post_name,"/".$value.".php", $c->data);
				$this->createFolder(_DIR_VIEWS.$this->post_name."/partial/".$value.".php");
			}
			
			if(count($create_file_archive)==2){
				$this->createItemFile(_DIR_THEME_SD."css/",$this->post_name,"/default.css", "r");
			}
			$c = $this->getData($this->post_name, _DIR_PLUGIN."code-sample/archive.php");
			$this->createItemFilePage(_DIR_CONTROLLERS,$c->name, ".php", $c->data, $c->name_oud);
		}
	}
	
	private function createItemFile($path, $folder, $name, $data = ""){
		$file = $path.$folder.$name;
		$foud = $path.$this->post_oud_name.$name;
		if(is_dir(dirname($foud))){
			if($is_css && $is_js){
				$myfile = fopen($foud, "r") or die("Unable to open foud!");
				$data = fread($myfile, filesize($foud));
				fclose($myfile);
				$myfile = fopen($foud, "w+") or die("Unable to open foud!");
				$data = str_replace($name_oud,$name,$data);
				fwrite($myfile, $data);
				fclose($myfile);
			}
			@rename(dirname($foud), dirname($file));
		}else{
			$this->createFolder($file);
		}
		$this->createFile($file, $data);
	}
	private function createFile($file, $data, $type = "w+"){
		if(!file_exists($file)){
			$th = fopen($file, $type) or die("Can't create file");
			fwrite($th, $data);
			fclose($th);
			chmod($file, 0775); 
		}
	}
	private function createFolder($file){
		if(!is_dir(dirname($file))){
			@mkdir(dirname($file), 0775);
			@chmod (dirname($file), 0775);
		}
	}
	private function getData($name, $path_copy){
		$name = "SD".ucfirst(strtolower(StringUtils::clear($name, "_")));
		$oudName = "SD".ucfirst(strtolower(StringUtils::clear($this->post_oud_name, "_")));
		
		$myfile = fopen($path_copy, "r") or die("Unable to open file!");
		$data = fread($myfile, filesize($path_copy));
		fclose($myfile);
		$data = str_replace(array("{{name}}", $oudName),$name,$data);
		$r = new StdClass;
		$r->data = $data;
		$r->name = $name;
		$r->name_oud = $oudName;
		return $r;
	}

	private function updateRoltersSQL($id){
		global $wpdb;
		$base_url = get_field("base_url", $id);
		$sr = get_field("subistituicao_de_rota", $id);
		$siteurl = get_option("siteurl");
		if(empty($base_url)){
			update_field("base_url", $siteurl, $id);
			$base_url = $siteurl;
		}
		update_option("siteurl", $base_url);
      	update_option("home", $base_url);
		if(!empty($sr)){
			$options = $wpdb->prepare("UPDATE wp_options SET option_value = replace(option_value, %s , %s ) WHERE option_name = 'home' OR option_name = 'siteurl';",$sr, $base_url);
			$posts_1 = $wpdb->prepare("UPDATE wp_posts SET guid = replace(guid, %s , %s );",$sr, $base_url);
			$posts_2 = $wpdb->prepare("UPDATE wp_posts SET post_content = replace(post_content, %s , %s );",$sr, $base_url);
			$post_meta = $wpdb->prepare("UPDATE wp_postmeta SET meta_value = replace(meta_value, %s , %s ) WHERE meta_key <> %s;",$sr, $base_url, 'subistituicao_de_rota');
			$wpdb->query($options);
			$wpdb->query($posts_1);
			$wpdb->query($posts_2);
			$wpdb->query($post_meta);
      	}
	}
}
new SDThemaConfig();