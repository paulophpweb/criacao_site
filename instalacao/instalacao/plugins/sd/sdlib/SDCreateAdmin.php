<?php
class SDCreateAdmin{
	private $acf_seo;
	public function __construct(){
		if (class_exists('acf_seo')) {
			$this->acf_seo = new acf_seo();
			$this->acf_seo->addPostType("seo-estatico");
			add_action("init", array($this, "inicialize"),99);
			add_filter( 'post_row_actions', array($this, "remove_row_actions"), 10, 1 );
			add_action('admin_head', array($this, "maybe_modify_admin_css"));
		}else SDAlert::insert("a inclusão do acf_seo não funcionou. Verifique o que pode ter acontecido.");
	}
	
	public function maybe_modify_admin_css(){

	    if( get_post_type() === 'seo-estatico' ) {
	        ?>
	        <style>
	            div#titlediv {
	                display: none;
	            }
	        </style>
	        <?php
	    }
	}
	
	public function remove_row_actions($actions){
	    if( get_post_type() === 'seo-estatico' )
	        unset( $actions['edit'] );
	        unset( $actions['view'] );
	        unset( $actions['trash'] );
	        unset( $actions['inline hide-if-no-js'] );
	    return $actions;
	}

	public function inicialize(){
		global $wpdb;
		
		$id = get_option("id-thema-config");
		if(!empty($id)){
			$taxs = get_field( 'create_tax', $id);
			if(!empty($taxs)){
				$tax_in_posts = array();
				foreach ($taxs as $key => $value) {
					if(!empty($value["receber_taxonimias"])){
						$receptores = array();

						foreach ($value["receber_taxonimias"] as $k => $v) {
							$p = $wpdb->get_row("SELECT post_name FROM wp_posts WHERE ID=".$v);
							if(empty($p)){
								$p = new stdClass;
							}
							$p->post_type = get_field("tipo_de_post", $v);
							if(empty($tax_in_posts[$p->post_name]))$tax_in_posts[$p->post_name] = array();
							$tax_in_posts[$p->post_name][] = StringUtils::clear($value["nome_tax"]);
							
							
							if($p->post_type != "page")$receptores[] = $p->post_name;
							else{
								$p = $wpdb->get_row("SELECT * FROM wp_posts WHERE post_name='".$p->post_name."' AND post_type='".$p->post_type."'");
								$link = get_admin_url()."post.php?post=$p->ID&action=edit";
								$link = '<a href="'.$link.'" target="_blank"><u>page</u></a>';
								
								SDAlert::insert("Este plugin não suporta taxonomia <strong>{$value["nome_tax"]}</strong> para o <strong>post_type</strong> do tipo <font color='red'><b>$link</b></font>.</br>Favor entre em <strong>Criar Thema / Thema Config</strong> e em <strong>Criar Relacionamento/Taxonomia</strong> e remova as página <strong>".$p->post_title."</strong> do filtro.");
							}
						}


						SDTaxonomy::insert(array(
							"show_in_menu"=>$value["add_submenu"],
							"add_tags"=>$value["add_tags"],
							"nome"=>$value["nome_tax"],
							"post_type"=>$receptores,
							"type_input"=>$value["type_input"],
							"show_admin_column"=>$value["admin_column"],
						));
					}
				}
				
			}
			$select = "SELECT p.*, pm.meta_value as 'tipo_de_post' ";
			$select.= "FROM wp_posts as p,wp_postmeta as pm WHERE ";
			$select.= "p.ID=pm.post_id AND post_type='criar-admin' AND post_status='publish' AND meta_key='tipo_de_post'";
			$pags_and_posts = $wpdb->get_results($select);
			
			foreach ($pags_and_posts as $key => $value) {
				if(!empty($value->tipo_de_post)){
					$func = "create_".$value->tipo_de_post;
					$value->taxonomia = $tax_in_posts[$value->post_name];
					$this->$func($value);
				}
			}
			
		}else SDAlert::insert("O option [id-thema-config] não foi definido. Portanto não podemos criar os arquivos.");
		
		$this->acf_seo->render();
	}

	private function create_page($p){
		$nome  = $p->post_title;
		$placeholder = get_field( 'titulo_do_placeholder', $p->ID);
		$icon_class = get_field( 'icone_do_botao', $p->ID);
		$menu_group = get_field( 'menu_group', $p->ID);
		$seo = get_field( 'seo', $p->ID);
		if($menu_group == "")$menu_group = false;
		else{
			$post_typegrouped = get_field( 'tipo_de_post', $menu_group->ID );
			if($post_typegrouped == "post"){
				$menu_group = StringUtils::clear($menu_group->post_title);
			}else{
				$menu_group = false;
				SDAlert::insert("O sistema não possui suporte para agrupar página com página. Local do erro:[title='".$nome."', ID=".$p->ID."]");
			}
		}

		$is_root = get_field('acesso', $p->ID);
		$show_in_menu = get_field('aparecer_no_menu_esquerdo', $p->ID);

		$campos = get_field('campos_pagina', $p->ID);
		$nuvem_de_tags = get_field('nuvem_de_tags', $p->ID);
		if($campos == "" || ($campos[0] == "null" && count($campos)==1))$campos = array("");
		else if($campos[0] == "null")array_shift($campos);

		$params = array(
            "nome"=>$nome,
            "titulo"=>$nome,
            "placeholder"=>$placeholder,
            "show_in_menu"=>$show_in_menu,
            "post_type"=>"page",
            "root"=>$is_root,
            "menu_group"=>$menu_group,
            "icon_class"=>$icon_class,
            "campos"=>$campos,
            "post_tag"=>$nuvem_de_tags,
        );
		SDPage::insert($params);
		if($seo){
			$slug_tmp = StringUtils::clear($nome);
			global $wpdb;
			$id = $wpdb->get_var("select ID from wp_posts where post_name='".$slug_tmp."' AND post_type='page';");
			$this->acf_seo->addPage($id);
		}
	}
	private function create_post($p){
		$titulo = $nome  = $p->post_title;
		$placeholder = get_field( 'titulo_do_placeholder', $p->ID);
		$post_type =  StringUtils::clear($nome);
		$icon_class = get_field( 'icone_do_botao', $p->ID);
		$is_root = get_field( 'acesso', $p->ID);
		$show_in_menu = get_field( 'aparecer_no_menu_esquerdo', $p->ID);
		$campos = get_field( 'campos_listagem', $p->ID);
		$usuario_conteudo = get_field( 'usuario_conteudo', $p->ID);
		$nuvem_de_tags = get_field( 'nuvem_de_tags', $p->ID);
		$seo = get_field( 'seo', $p->ID);

		if($campos == "" || ($campos[0] == "null" && count($campos)==1))$campos = array("");
		else if($campos[0] == "null")array_shift($campos);
		SDArchive::insert(
            array(
				"nome"=>$nome,
				"placeholder"=>$placeholder,
				"post_type"=>$post_type,
				"show_in_menu"=>$show_in_menu,
				"create_posts"=>$usuario_conteudo,
				"root"=>$is_root,
				"icon_class"=>$icon_class,
				"campos"=>$campos,
				"post_tag"=>$nuvem_de_tags,
				"taxonomia"=>$p->taxonomia
            )
        );
        if($seo){
	        $slug_tmp = StringUtils::clear($nome);
	        $this->acf_seo->addPostType($slug_tmp);
	        global $wpdb;
			$id = $wpdb->get_var("select ID from wp_posts where post_name='".$slug_tmp."' AND post_type='seo-estatico';");
			if(empty($id)){
				$p = array(
				  'post_title'   	=> $nome,
				  'post_name'   	=> $slug_tmp,
				  'post_status'  	=> 'publish',
				  "post_type"	 	=> 'seo-estatico',
				  "ping_status"		=>'closed',
				  "comment_status"	=>'closed',
				);
				$wpdb->insert("wp_posts", $p);
			}
	    }
	}
}
new SDCreateAdmin();