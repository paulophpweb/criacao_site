<?php
class SDMetaBox{
	public static $supports = Array('title', 'editor', 'comments', 'trackbacks', 'author', 'excerpt', 'custom-fields', 'thumbnail', "permalink");
	public function __construct(){
		if(is_admin()){
			add_action( 'admin_menu', array($this, 'render_meta_box'),100);
			add_filter( 'enter_title_here', array($this, 'enter_title_here'),100);
		}
	}
	
	public function enter_title_here( $input ) {
	    global $post_type;
	    foreach (SDArchive::$itens as $key => $value){
	    	if(StringUtils::clear($value->nome) == $post_type)return __($value->placeholder, 'your_textdomain' );
	    }
	    if($post_type=="page"){
	    	$title = get_the_title($_GET["post"]);
	    	foreach (SDPage::$itens as $key => $value){
		    	if($value->nome == $title){
		    		return __($value->placeholder, 'your_textdomain');
		    	}
		    }
	    }
	    return $input;
	}
	public function hide_permalink() {
	    return '';
	}

	public function render_meta_box($e){
		if(!empty($_GET) && !empty($_GET["post"]) && get_post_type($_GET["post"]) == "page"){
			$title = get_the_title($_GET["post"]);
			$supports= array("custom-fields", "title", "editor", "revisions","comments","author", "permalink");
			$metabox = array('commentstatusdiv','slugdiv','pageparentdiv');
			foreach (SDPage::$itens as $key => $value){
				if($value->nome == $title){
					$s = array_diff($supports, $value->campos);
					if(!in_array("permalink", $s)){
						if(in_array("title", $s)){
							SDAlert::insert("Para mostrar o exemplo de [permalink] link permanente, é necessário adicionar o campo [title]");
						}
					}
					$m = array_diff($metabox, $value->campos);
					if(!in_array("commentstatusdiv", $m)){
						if(in_array("comments", $s)){
							SDAlert::insert("Para mostrar o [commentstatusdiv] Discusão, é necessário adicionar o campo [comments]");
						}
					}

					if(in_array("title", $m)){
					?>
					<style>
						div#wpbody-content div.wrap h1 a.page-title-action, div#post-body-content{
							display: none!important;
						}
						div#wpbody-content div.wrap h1{
							color: #F5F5F5;
						}
						div#wpbody-content div.wrap h1::before{
							content: "<?php echo $value->nome ?>";
							color: #000000;
						}
					</style>
					
					<?php
					}
					foreach ($s as $key => $v) {
						if($v=="permalink")add_filter( 'get_sample_permalink_html', array($this, 'hide_permalink'),106);
						else remove_post_type_support('page', $v);
					}
					foreach ($m as $key => $v) {
						remove_meta_box($v , 'page' , 'normal');
					}
					
					if($value->post_tag)register_taxonomy_for_object_type('post_tag', 'page');
					break;
				}
			}
		}
		
	}
}
new SDMetaBox();