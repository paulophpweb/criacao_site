<?php

function wpse_139269_term_radio_checklist_start_el_version( $args, $post_id, $tax_radio) {

	if(empty($tax_radio))return $args;
    if(!empty( $args['taxonomy']) && in_array($args['taxonomy'], $tax_radio)){
        if(empty($args['walker'] ) || is_a( $args['walker'], 'Walker')){
            if(!class_exists( 'WPSE_139269_Walker_Category_Radio_Checklist_Start_El_Version')){
                class WPSE_139269_Walker_Category_Radio_Checklist_Start_El_Version extends Walker_Category_Checklist{
                    public function start_el( &$output, $category, $depth = 0, $args = array(), $id = 0){

                        if(empty($args['taxonomy']))$taxonomy = 'category';
                        else $taxonomy = $args['taxonomy'];
                        if($taxonomy == 'category')$name = 'post_category';
                        else $name = 'tax_input[' . $taxonomy . ']';
                        $args['popular_cats'] = empty( $args['popular_cats'] ) ? array() : $args['popular_cats'];
                        $class = in_array( $category->term_id, $args['popular_cats'] ) ? ' class="popular-category"' : '';

                        $args['selected_cats'] = empty( $args['selected_cats'] ) ? array() : $args['selected_cats'];

                        if(!empty( $args['list_only'])){
                            $aria_cheched = 'false';
                            $inner_class = 'category';
                            if(in_array($category->term_id, $args['selected_cats'])){
                                $inner_class .= ' selected';
                                $aria_cheched = 'true';
                            }
                            $output .= "\n" . '<li' . $class . '>' .
                                '<div class="' . $inner_class . '" data-term-id=' . $category->term_id.
                                ' tabindex="0" role="checkbox" aria-checked="' . $aria_cheched . '">' .
                                esc_html( apply_filters( 'the_category', $category->name ) ) . '</div>';
                        }else{
                            $output .= "\n<li id='{$taxonomy}-{$category->term_id}'$class>" .
                            '<label class="selectit"><input value="' . $category->term_id . '" type="radio" name="'.$name.'[]" id="in-'.$taxonomy.'-' . $category->term_id . '"' .
                            checked( in_array( $category->term_id, $args['selected_cats'] ), true, false ) .
                            disabled( empty( $args['disabled'] ), false, false ) . ' /> ' .
                            esc_html( apply_filters( 'the_category', $category->name ) ) . '</label>';
                        }
                    }
                }
            }
            $args['walker'] = new WPSE_139269_Walker_Category_Radio_Checklist_Start_El_Version;
        }
    }
    return $args;
}

class SDTaxonomy{
	public static $itens = array();
	private $itens_radio = array();
	public function __construct(){
		add_action('init', array($this, 'render'),125);
	}

	public function render($e){
		global $post_type;
		
    	foreach (SDTaxonomy::$itens as $key => $value)$this->render_tax($value);
	    add_filter( 'wp_terms_checklist_args', array($this,"term_radio") , 10, 2 );
	}
	
	public function term_radio($args, $post_id){
		return wpse_139269_term_radio_checklist_start_el_version($args, $post_id,$this->itens_radio);
	}

	private function render_tax($value){
		register_taxonomy(
	       	StringUtils::clear($value->nome),
	        $value->post_type,
	        array(
	            'hierarchical'          => !$value->add_tags,
	            'show_ui'               => true,
	            "show_in_menu"			=> $value->show_in_menu,
	            'show_admin_column'     => $value->show_admin_column,
	            'query_var'             => true,
	            'label'                 => __($value->nome),
	            'rewrite'               => array( 'slug' => StringUtils::clear($value->nome))
	        )
	    );
	    if($value->type_input == "radio")$this->itens_radio[] = StringUtils::clear($value->nome);
	}
	
	/**
	 * Metodo archive, serve para adicionar no admin uma nova área de tipo de post com type_post diferente.
	 * @param Array $itens = array(type_input:Boolean, show_in_menu:Boolean, add_tags:Boolean, nome:String, post_type:Array,show_admin_column:Boolean);
	 * @param String $itens->show_in_menu = Deseja que o botão de listar/adicionar taxonimia apareça no menu?
	 * @param String $itens->type_input = Indica se a taxonomia ira ser radio ou checkbox
	 * @param String $itens->add_tags = Indica se o usuário pode adicionar novas tags nesta taxonomia/metatag
	 * @param String $itens->nome = Nome da taxonomia
	 * @param String $itens->post_type = Lista de Post Type que receberão as taxonomias
	 * @param String $itens->show_admin_column = Se vai aparecer como coluna na listagem dos post no painel
	 * @return AddPage
	 */
	public static function insert($item){
		global $post_type;
		$item = (object)$item;
		self::$itens[] = $item;
		return $this;
	}
}
new SDTaxonomy();