<?php 

add_action("init", "init");
function init(){
    $taxonomias = get_field("create_tax", 4);
    if(count($taxonomias)){
        global $wpdb;
        $aSlug = array();
        foreach ($taxonomias as $taxonomia){
            foreach ($taxonomia['receber_taxonimias'] as $posts){
                $slug = $wpdb->get_var("
                    SELECT 
                        wp_posts.post_name 
                    FROM 
                        wp_posts 
                        LEFT JOIN wp_postmeta ON wp_posts.ID = wp_postmeta.post_id AND wp_postmeta.meta_key = 'tipo_de_post'
                    WHERE wp_posts.ID = ".$posts."");
                $aSlug[] = $slug;
            }
            $nome_tax = StringUtils::clear($taxonomia['nome_tax']);
            register_taxonomy($nome_tax,
                $aSlug,
                array(
                    "label" => $taxonomia['nome_tax'],
                    "singular_label" => $taxonomia['nome_tax'],
                    "rewrite" => true,
                    "hierarchical" => true,
                    "show_admin_column" => $taxonomia['admin_column'],
                    "show_ui" => $taxonomia['type_input'],
                    "show_tagcloud" => $taxonomia['add_tags'],
                    "rewrite" => array("slug" => $nome_tax, "hierarchical" => true)
                )
            );
             
        }
    } 
}


