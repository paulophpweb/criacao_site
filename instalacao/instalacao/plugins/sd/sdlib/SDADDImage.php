<?php
class SDADDImage{
	public function __construct(){
		add_action( 'admin_init', array($this, 'codex_init'));
	}
	public function codex_init(){
		define('WP_MEMORY_LIMIT', '128M');
		add_filter('intermediate_image_sizes_advanced', array($this, 'intermediate_image_sizes_advanced'));
		add_filter( 'sanitize_file_name', array($this, 'sanitize_file_name'), 10 );
		add_filter('wp_generate_attachment_metadata',array($this, 'wp_generate_attachment_metadata'));
		add_action('wp_trash_post', array($this, 'trash_post'));
	}
	//REMOVENDO ACENTOS PARA ARQUIVOS DE UPLOAD
	public function sanitize_file_name( $filename ) {
	    $info = pathinfo( $filename );
	    $ext  = empty( $info['extension'] ) ? '' : '.' . $info['extension'];
	    $name = basename( $filename, $ext );
	    return StringUtils::clear($name).$ext;
	}
	//REMOVENDO TAMANHOS DE IMAGENS INDESEJAVEIS
	public function intermediate_image_sizes_advanced( $sizes) {
	    unset( $sizes['medium']);
	    $sizes['large'] = array(
	    	"width"=>1920,
	    	"height"=>1920,
	    	"crop"=>false
	    );
	    unset( $sizes['medium_large']);
	    return $sizes;
	}
	public function wp_generate_attachment_metadata($image_data) {
	  if (!isset($image_data['sizes']['large'])) return $image_data;
	  $upload_dir = wp_upload_dir();
	  $uploaded_image_location = $upload_dir['basedir'] . '/' .$image_data['file'];
	  $current_subdir = substr($image_data['file'],0,strrpos($image_data['file'],"/"));
	  $large_image_location = $upload_dir['basedir'] . '/'.$current_subdir.'/'.$image_data['sizes']['large']['file'];
	  unlink($uploaded_image_location);
	  rename($large_image_location,$uploaded_image_location);
	  $image_data['width'] = $image_data['sizes']['large']['width'];
	  $image_data['height'] = $image_data['sizes']['large']['height'];
	  unset($image_data['sizes']['large']);
	  return $image_data;
	}
	public function trash_post($postID){
		if(!empty($_GET["post"]) || $_GET["action"] == "trash"){
			$new_dir_name = "personalize-image-post-id-".$postID;
			$dir = _DIR_UPLOAD.$new_dir_name;
			if(is_dir($dir)){
				deletedir($dir);
			}
		}
	}
}
new SDADDImage();