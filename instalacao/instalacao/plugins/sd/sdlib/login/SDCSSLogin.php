<?php
class SDCSSLogin{
	public function __construct(){
		add_action( 'login_enqueue_scripts', array($this, "add_css_login"));
		add_action('acf/save_post', array($this, 'save_post'));
	}

	public function add_css_login(){
		$noCache = "noCache=".rand(10,1000);
	    wp_enqueue_style( 'custom-login', _URL_PLUGIN . 'css/login.css?'.$noCache);
	}
	
	public function logged(){
		// $recaptcha = new \ReCaptcha\ReCaptcha($wp->post->secret);
	 //    $resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
	 //    if(!$resp->isSuccess()){				
  //   		$result->status = "error";
		// 	$result->mensagem =  "Clique no captcha para provar que você não é um robô!";
  //   	}
	}
	public function save_post($post_id){
		$id = get_option("id-thema-config");
	    if($post_id == $id){
	        $this->render();
	    }
	}
	
	public function render(){
		$id = get_option("id-thema-config");
		$logo = get_field("logo_login", $id);
		$is_color = get_field("cor_da_pagina_login", $id);
		if($is_color == "cor"){
			$cor = get_field("cor_de_fundo_do_login", $id);
			if(empty($cor))$cor = "#f1f1f1";
			$css = "background-color: $cor!important;";
		}else{
			if($is_color == "img"){
				$img = get_field("imagem_de_fundo_do_login", $id);
				if(!empty($img) && !empty($img["url"])){
					$img = $img["url"];
					$css = "background: url($img) center bottom!important;";
				}else{
					$css = "background: url(../images/bg-login.jpg) center bottom!important;";
				}
			}else{
				$css = "background: url(../images/bg-login.jpg) center bottom!important;";
			}
		}
		if(!empty($logo) && !empty($logo["url"])){
			$logo = $logo["url"];
		}else $logo = "../images/logo-sitio.png";
		ob_start();
		?>
h1 a{
 	-webkit-background-size:auto!important;
 	background-size:auto!important;
 	width:275px!important;
 	height:84px!important;
 	background-image:url(<?php echo $logo; ?>) !important;
 }
#login{
 	width:351px !important;
 }
 .g-recaptcha{
 	margin-bottom: 15px;
 }
 body{
 	<?php echo $css; ?>
 	background-attachment: fixed;
 }
 .login #backtoblog a, .login #nav a, .login h1 a{
 	color: #f9f9f9!important;
 }
		<?php
		$css = ob_get_clean();
		
		$fp = fopen(_DIR_PLUGIN . 'css/login.css', "w");
		$escreve = fwrite($fp, $css);
		fclose($fp);
	}
}
new SDCSSLogin();
?>