<?php 
class SDCaptchaLogin{
	public function __construct(){
		$id = get_option("id-thema-config");
    	$secret_key = get_post_meta($id, "captcha_secret_key", true);
    	$site_key = get_post_meta($id, "captcha_site_key", true);
    	$developer = get_post_meta($id, "status_de_desenvolvimento", true);
    	
    	if(!empty($secret_key) && !empty($site_key) && $developer != "dev"){
			add_action( 'login_form', array($this, 'add_captcha'));
			add_action( 'login_enqueue_scripts', array($this, "add_js"));
			add_action('wp_login', array($this, "is_logged"));
			add_action('wp_logout',array($this,'no_logged'));
		}
	}

	public function is_logged(){
		// $id = get_option("id-thema-config");
  //   	$secret_key = get_post_meta($id, "captcha_secret_key", true);
  //   	if(!$this->is_localhost()){
	 //    	$recaptcha = new \ReCaptcha\ReCaptcha($secret_key);
		// 	$resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
		// 	if(!$resp->isSuccess())wp_logout();
  //   	}
	}
	
	private function is_localhost() {
	    $whitelist = array( '127.0.0.1', '::1' );
	    if( in_array( $_SERVER['REMOTE_ADDR'], $whitelist) )return true;
	    return false;
	}
	
	public function no_logged(){
		wp_clearcookie();
		$id = get_option("id-thema-config");
	    $value = get_post_meta($id, "router_admin", true);
		$url = home_url('/'.$value);
		wp_redirect($url);
  		exit();
	}
	
	public function add_js(){
		wp_enqueue_script('recaptcha', 'https://www.google.com/recaptcha/api.js?hl=pt&');
	}
	
	public function add_captcha(){
		$id = get_option("id-thema-config");
    	$site_key = get_post_meta($id, "captcha_site_key", true);
		 echo '<div class="g-recaptcha" data-sitekey="'.$site_key.'"></div>';
	}
}
new SDCaptchaLogin();