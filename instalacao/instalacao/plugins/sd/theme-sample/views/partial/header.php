<!DOCTYPE html>
<html <?php language_attributes();?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<!-- Chrome, Firefox OS and Opera -->
	<meta name="theme-color" content="#a6ce39">
	<!-- Windows Phone -->
	<meta name="msapplication-navbutton-color" content="#a6ce39">
	<!-- iOS Safari -->
	<meta name="apple-mobile-web-app-status-bar-style" content="#a6ce39">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>