<?php
/**
* Plugin Name: SD Soluções Interativas
* Plugin URI: https://www.agenciasitiodigital.com.br
* Description: Facilitador de site
* Version: 1.0
* Author: SD Soluções Interativas
* Author URI: https://www.agenciasitiodigital.com.br
* License: A "Slug" license name e.g. GPL12
*/

date_default_timezone_set('America/Sao_Paulo');
//Remover intervalo entre palavras
//echo preg_replace("/:before(.+?)[\s]*\/?[\s]*dashicons-/si", "','", $str);
function dir_filter($path){
    return str_replace("/", DIRECTORY_SEPARATOR, $path);
}
$url= plugins_url("sd/");
$dir = plugin_dir_path(__FILE__);
$theme_root = get_theme_root().DIRECTORY_SEPARATOR;
$wp_upload = wp_upload_dir();
$upload_dir = $wp_upload["basedir"].DIRECTORY_SEPARATOR;
$upload_url = $wp_upload["baseurl"]."/";

$home = home_url().DIRECTORY_SEPARATOR;
$template_url = $home."wp-content/themes/sd-theme/";


define("_DIR_PLUGIN", $dir);
define("_DIR_THEME", $theme_root);
define("_DIR_UPLOAD", $upload_dir);
define("_DIR_THEME_SD", dir_filter($theme_root."sd-theme/"));
define("_DIR_VIEWS", dir_filter($theme_root."sd-theme/views/"));
define("_DIR_CONTROLLERS", dir_filter($theme_root."sd-theme/controllers/"));
define("_DIR_CSS", dir_filter($theme_root."sd-theme/css/"));
define("_DIR_JS", dir_filter($theme_root."sd-theme/js/"));
define("_DIR_IMG", dir_filter($theme_root."sd-theme/images/"));

define("_URL_PLUGIN", $url);
define("_URL_UPLOAD", $upload_url);
define("_URL_CSS", $template_url."css/");
define("_URL_JS", $template_url."js/");
define("_URL_IMG", $template_url."images/");
define("_URL_TEMPLATE", $template_url);
define("_URL_THEME", $home);

require_once(dir_filter(_DIR_PLUGIN."sdlib/uteis/SDAlert.php"));
$id = get_option("id-thema-config");
if(function_exists("get_field")){
    if(empty($id))$noCache = rand(10,1000);
    else{
        global $wpdb;
        $status = $wpdb->get_var("SELECT pm.meta_value FROM wp_postmeta as pm WHERE pm.post_id =$id AND meta_key='status_de_desenvolvimento';");

        if($status == "dev")$noCache = rand(10,1000);
        else{
            $noCache = $wpdb->get_var("SELECT pm.meta_value FROM wp_postmeta as pm WHERE pm.post_id =$id AND meta_key='cache_version';");
        }
    }
    
    define("_NO_CACHE", $noCache);
    require_once(dir_filter(_DIR_PLUGIN."sdlib/autoload.php"));
    require_once(dir_filter(_DIR_PLUGIN."sdlib/canvas.php"));
    require_once(dir_filter(_DIR_PLUGIN."sdlib/uteis/functions.php"));
    require_once(dir_filter(ABSPATH.'wp-admin/includes/plugin.php'));
    require_once(dir_filter(_DIR_PLUGIN."sdlib/uteis/Debug.php"));
    require_once(dir_filter(_DIR_PLUGIN."sdlib/login/SDCSSLogin.php"));
    require_once(dir_filter(_DIR_PLUGIN."sdlib/SDThemaConfig.php"));
    require_once(dir_filter(_DIR_PLUGIN."sdlib/SDRouter.php"));
    require_once(dir_filter(_DIR_PLUGIN."sdlib/uteis/acf_seo.php"));
    require_once(dir_filter(_DIR_PLUGIN."sdlib/uteis/campos_personalizados_prefeitura.php"));
    //SDCreateTheme
    $permission_create_theme = false;
    if(!is_dir(_DIR_THEME."sd-theme")){
        $theme = _DIR_THEME;
        $src = _DIR_PLUGIN."theme-sample";
        $dest = $theme."sd-theme";
        smartCopy($src, $dest);
        if(is_dir(_DIR_THEME."sd-theme")){
            $permission_create_theme = true;
            SDAlert::insert("Thema sd-theme adicionado com sucesso.");
        }else{
            $permission_create_theme = false;
            SDAlert::insert("Você não tem permissão 0775 para escrever dentro da pasta [$theme].");
        }
    }else $permission_create_theme = true;

    if ($permission_create_theme && is_plugin_active('advanced-custom-fields/acf.php')) {
            if(is_plugin_active('acf-repeater/acf-repeater.php')){
                require_once(dir_filter(_DIR_PLUGIN."sdlib/meta/SDMetaBox.php"));
                require_once(dir_filter(_DIR_PLUGIN."sdlib/archive/SDArchive.php"));
                require_once(dir_filter(_DIR_PLUGIN."sdlib/page/SDPage.php"));
                require_once(dir_filter(_DIR_PLUGIN."sdlib/meta/SDTaxonomy.php"));
                require_once(dir_filter(_DIR_PLUGIN."sdlib/SDADDImage.php"));

                if(is_login()){
                    require_once(dir_filter(_DIR_PLUGIN."sdlib/login/SDCaptchaLogin.php"));
                }
                else if(is_admin()){
                    SDPage::insert(
                        array(
                            "nome"=>"Thema config",
                            "titulo"=>"sd-theme",
                            "placeholder"=>"Nome do Thema",
                            "post_type"=>"page",
                            "campos"=>array(),
                            "root"=>true,
                            "menu_group"=>"criar-admin",
                            "icon_class"=>"dashicons-format-links"
                        )
                    );
                    /*SDPage::insert(
                        array(
                            "nome"=>"Canais",
                            "titulo"=>"Canais",
                            "post_type"=>"page",
                            "root"=>false,
                            "menu_group"=>false,
                            "icon_class"=>"dashicons-share"
                        )
                    );*/
                    SDArchive::insert(
                        array(
                            "nome"=>"Criar Admin",
                            "show_in_menu"=>true,
                            "titulo"=>"Nome do(a) post/página",
                            "create_posts"=>true,
                            "root"=>true,
                            "icon_class"=>"dashicons-admin-home"
                        )
                    );
                    SDArchive::insert(
                        array(
                            "nome"=>"SEO estático",
                            "show_in_menu"=>true,
                            "titulo"=>"Nome do(a) post/página",
                            "create_posts"=>false,
                            "root"=>true,
                            "icon_class"=>"dashicons-admin-home"
                        )
                    );
                    SDPage::insert(
                        array(
                            "nome"=>"Home",
                            "titulo"=>"Home",
                            "post_type"=>"seo-estatico",
                            "campos"=>array(),
                            "root"=>false,
                            "icon_class"=>"dashicons-share"
                        )
                    );
                    
                    require_once(dir_filter(_DIR_PLUGIN."sdlib/SDTaxonomyView.php"));
                    
            }else{
                //Adicionando os metodos de recuperação de dados de login.
                //require_once(dir_filter(ABSPATH . "wp-includes/pluggable.php" ));
            }
            
            require_once(dir_filter(_DIR_PLUGIN."sdlib/SDCreateAdmin.php"));
            
            //neste caso estamos dentro do thema;
            require_once(dir_filter(_DIR_PLUGIN."sdlib/login/SDRenameLogin.php"));

            $idCanais = get_option("id-canais");
            if(!empty($idCanais)){
                require_once(dir_filter(_DIR_PLUGIN."sdlib/uteis/acf.php"));
                //Relacionando o ACF com o SD-LIB
                global $GLOBALS;
                $itens_name = array("acf_canais", "acf_sd-theme", 'acf_criar-admin');

                $itens_id = array(get_option("id-canais"), get_option("id-thema-config"),'criar-admin');
                
                foreach ($GLOBALS['acf_register_field_group'] as $key => $value){
                    if(in_array($value["id"], $itens_name)){
                        $indice = array_search($value["id"], $itens_name);
                        if(empty($GLOBALS['acf_register_field_group'][$key]["location"]["value"])){
                            if(empty($GLOBALS['acf_register_field_group'][$key]["location"][0]["value"])){
                                $GLOBALS['acf_register_field_group'][$key]["location"][0][0]["value"] = $itens_id[$indice];
                            }else $GLOBALS['acf_register_field_group'][$key]["location"][0]["value"] = $itens_id[$indice];
                        }else $GLOBALS['acf_register_field_group'][$key]["location"]["value"] = $itens_id[$indice];
                    }
                }
            }
        }else{
            SDAlert::insert("Para que o Plugin SDLIB funcione, ative/instale o plugin ACF-REPEAT no painel de plugins.");
        }
    }else{
        SDAlert::insert("Para que o Plugin SDLIB funcione, ative/instale o plugin ACF no painel de plugins.");
    }
}else SDAlert::insert("Para que o Plugin SDLIB funcione, ative/instale o plugin ACF no painel de plugins.");

