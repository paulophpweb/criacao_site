<?php
get_header(); ?>

	<div id="primary" class="content-area" style="width: 960px; margin: 0 auto; padding:250px 10px 100px;">
		<div id="content" class="site-content">
			<header class="page-header">
				<h1 class="page-title"><?php _e( 'Não foi encontrado', 'emporiocamila' ); ?></h1>
			</header>

			<div class="page-wrapper">
				<div class="page-content">
					<h2><?php _e( 'Isso é um pouco constrangedor, não é?', 'emporiocamila' ); ?></h2>
					<p><?php _e( 'Parece que nada foi encontrado neste local. Talvez tente uma pesquisa?', 'emporiocamila' ); ?></p>
					<p><?php //var_dump(debug_backtrace()); ?></p>
					<?php get_search_form(); ?>
				</div>
			</div>

		</div>
	</div>

<?php get_footer(); ?>