<div id="primary" class="content-area" style="width: 960px; margin: 0 auto; padding:250px 10px 100px;">
	<div id="content" class="site-content">
		<header class="page-header" style="height:60px;">
			<h1 class="page-title"><?php _e( 'A página não foi encontrada', 'emporiocamila' ); ?></h1>
		</header>

		<div class="page-wrapper">
			<div class="page-content">
				<h2><?php _e( 'Tente voltar ao início para procurar pela sua página', 'emporiocamila' ); ?></h2>
				<p><?php _e( 'Parece que nada foi encontrado neste local. Talvez tente uma pesquisa?', 'emporiocamila' ); ?></p>
				<p><?php //var_dump(debug_backtrace()); ?></p>
				<?php get_search_form(); ?>
			</div> 
		</div>

	</div>
</div>
