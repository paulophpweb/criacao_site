<?php 
$taxonomies=get_terms('categorias-noticias',array(
    'hide_empty' => false
));
$aVars = $this->admin_post->vars;
$page = 0;
$limite = 3;
$cond = array('post_type' => 'noticias');
if(isset($aVars["categoria"])){
  $cond['tax_query'] =  array(
      array(
          'taxonomy' => 'categorias-noticias',
          'field' => "slug",
          'terms' => $aVars["categoria"],
      )
  );
}
$total_post = count(get_posts($cond));
$totalPagina = ceil($total_post/$limite);
if(!empty($aVars["page"])){
  $page = ($aVars["page"]*$limite)-$limite;
}
$cond = array('post_type' => 'noticias','numberposts' => $limite,'orderby' => 'post_date','order' => "ASC",'offset' => $page);
$categoria = "Todas";
if(isset($aVars["categoria"])){
      $cond['tax_query'] =  array(
          array(
              'taxonomy' => 'categorias-noticias',
              'field' => "slug",
              'terms' => $aVars["categoria"],
          )
      );
      $categoria = get_term_by("slug",$aVars["categoria"],"categorias-noticias")->name;
  }
?>
<section class="noticias">
    <div class="container noPaddingXs">
        <article class="col-lg-8 col-md-8 col-sm-7 col-xs-12 container noPaddingXs">
            <div class="box-padding-1020 bg-cinza-claro pl-35">
                <p class="cinza m-0">Você está aqui: Página Inicial / Notícias</p>
            </div>
            <div class="box-padding-35 bg-branco">
                <div class="box">
                    <h2>Categoria: <?php echo $categoria; ?></h2>
                </div>
                <!--<div class="box">
                    <select name="" class="branco estilizado">
                        <option>Filtrar por pedido</option>
                        <option>Filtrar por pedido</option>
                        <option>Filtrar por pedido</option>
                    </select>
                </div>-->
                <?php 
                  $noticias = get_posts($cond);
                  if(count($noticias)):
                  foreach ($noticias as $key => $noticia):
                  $campos = get_fields($noticia->ID);
                ?>
                    <a href="/noticia/<?php echo $noticia->post_name;?>" class="box-galeria box">
                        <figure style="background-image: url(<?php echo $this->createIMG($noticia->ID, $campos["capa"], 607,405, true); ?>);"></figure>
                        <span class="box">
                            <span class="box"><span class="date h5 cinza-claro"><?php echo DateUtils::format(array("post_date" => $noticia->post_date,"format" => "d.m.Y"));?></span></span>
                            <span class="box"><span class="h3"><?php echo $noticia->post_title; ?></span></span>
                            <span class="box"><span class="h5 cinza-claro"><?php echo $campos["breve_descricao"]; ?></span></span>
                        </span>
                        <span class="divide"></span>
                    </a>
                <?php 
                endforeach;
                else:
                ?>

              <h3>Nenhuma notícia encontrada para esta categoria!</h3>

              <?php endif; ?>
                <?php 
                  if (function_exists("wp_bs_pagination"))
                      {
                           wp_bs_pagination($totalPagina,2);
                  }
                ?>
            </div>
            <div class="box mb-20">
                <a href="javascript:;" class="ir-topo h5 cinza-claro scrollToDiv">Ir para o topo <span></span></a>
            </div>
        </article>
        <aside class="col-lg-4 col-md-4 col-sm-5 col-xs-12 container noPaddingXs">
            <div class="box-padding-1020 bg-cinza-claro pl-35">
                <p class="cinza m-0">Categoria de notícias</p>
            </div>
            <div class="box-padding pl-35 bg-branco links-aside mb-20">
                <div class="box">
                 <a href="/noticias" class="p-10-0 <?php echo $aVars["categoria"] ? "" : "selecionado"; ?>">Todas</a>
               </div>
               <?php
                if(count($taxonomies)):
                foreach ($taxonomies as $taxonomy):
               ?>
               <div class="box">
                 <a href="/noticias/index/categoria/<?php echo $taxonomy->slug;?>" class="p-10-0 <?php echo $aVars["categoria"] == $taxonomy->slug ? "selecionado" : ""; ?>"><?php echo StringUtils::limite($taxonomy->name,30);?></a>
               </div>
               <?php endforeach;?>
               <?php endif;?>                             
            </div>
            <ul class="banner-link box">
                <?php 
                  $banners = get_posts(array('post_type' => 'banners','numberposts' => -1,"meta_query" => array(
                    'relation' => 'AND', // Optional, defaults to "AND"
                    array(
                            'key'     => 'destaque',
                            'value'   => 'nao',
                            'compare' => 'LIKE'
                    )
                    )));
                  if(count($banners)):
                  foreach ($banners as $key => $banner):
                  $campos = get_fields($banner->ID);
                ?>
                    <li class="bg-azul mb-20">
                        <a target="<?php echo $campos["trajeto"];?>" href="<?php echo $campos["url"] ? $campos["url"] : "javascript:;";?>" class="bg-azul" style="background-image: url('<?php echo $this->createIMG($banner->ID, $campos['imagem'], 362,180, true);?>');"></a>
                    </li>
                <?php 
                endforeach;
                endif;
                ?>
            </ul>            
        </aside>
    </div>
</section>