<?php 
$noticia = get_post();
$campos = get_fields($noticia->ID);
$taxonomies=get_terms('categorias-noticias',array(
    'hide_empty' => false
));
?>
<section class="noticias-interna">
    <div class="container">
        <article class="col-lg-8 col-md-8 col-sm-7 col-xs-12 noPaddingXs impressao100">
            <div class="box-padding-1020 bg-cinza-claro pl-35 removerImpressao">
                <p class="cinza m-0">Você está aqui: Página Inicial / Notícia / <?php echo $noticia->post_title; ?></p>
            </div>
            <div class="box-padding pl-35 bg-branco posr">
                <a href="javascript:;" onclick="window.print();" class="impressora removerImpressao"></a>
                <ul id="banner" style="height: auto;">
                    <li>
                        <a href="<?php echo $campos["capa"]["url"]; ?>" class="fancybox" rel="group">
                            <figure><img src="<?php echo $this->createIMG($noticia->ID, $campos["capa"], 607,405, true); ?>" alt=""></figure>
                        </a>
                    </li>
                    <?php 
                        if(count($campos["galeria"])): 
                        foreach ($campos["galeria"] as $key => $galeria):
                    ?>
                    <li>
                        <a href="<?php echo $galeria["url"]; ?>" class="fancybox" rel="group">
                            <figure><img src="<?php echo $this->createIMG($noticia->ID, $galeria, 607,405, true); ?>" alt=""></figure>
                        </a>
                    </li>
                    <?php 
                        endforeach;
                        endif; 
                    ?>
                </ul>
                <!--<figure><img src="<?php echo $this->createIMG($noticia->ID, $campos["capa"], 607,405, true); ?>" alt=""></figure>-->
                <div class="arrow removerImpressao">
                    <a href="javascript:;" id="next"></a>
                    <a href="javascript:;" id="prev"></a>
                </div>
                <h2 class="regular m-0 ajusteImpressao"><?php echo $noticia->post_title; ?></h2>
                <div class="botoes-interagir removerImpressao">
                    <!-- Icon-only Branded Twitter button -->
                    <a class="share-btn share-btn-branded share-btn-twitter"
                       href="https://twitter.com/share?url=<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>"
                       title="Share on Twitter">
                        <span class="share-btn-icon"></span>
                        <span class="share-btn-text-sr">Twitter</span>
                    </a>

                    <!-- Icon-only Branded Facebook button -->
                    <a class="share-btn share-btn-branded share-btn-facebook"
                       href="https://www.facebook.com/sharer/sharer.php?u=<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>"
                       title="Facebook">
                        <span class="share-btn-icon"></span>
                        <span class="share-btn-text-sr">Facebook</span>
                    </a>

                    <!-- Icon-only Branded Google+ button -->
                    <a class="share-btn share-btn-branded share-btn-googleplus"
                       href="https://plus.google.com/share?url=<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>"
                       title="Google+">
                        <span class="share-btn-icon"></span>
                        <span class="share-btn-text-sr">Google+</span>
                    </a>

                    <!-- Icon-only Branded LinkedIn button -->
                    <a class="share-btn share-btn-branded share-btn-linkedin"
                       href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>"
                       title="LinkedIn">
                        <span class="share-btn-icon"></span>
                        <span class="share-btn-text-sr">LinkedIn</span>
                    </a>

                    <!-- Icon-only Branded Pinterest button -->
                    <a class="share-btn share-btn-branded share-btn-pinterest"
                       href="https://www.pinterest.com/pin/create/button/?url=<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>"
                       title="Pinterest">
                        <span class="share-btn-icon"></span>
                        <span class="share-btn-text-sr">Pinterest</span>
                    </a>
                </div>
                <div class="box">
                    <?php echo $campos["texto"]; ?>
                </div>
            </div>
            <div class="box mb-20 removerImpressao">
                <a href="javascript:;" class="ir-topo h5 cinza-claro scrollToDiv">Ir para o topo <span></span></a>
            </div>
        </article>
        <aside class="col-lg-4 col-md-4 col-sm-5 col-xs-12 noPaddingXs removerImpressao">
            <div class="box-padding-1020 bg-cinza-claro pl-35">
                <p class="cinza m-0">Categoria de notícias</p>
            </div>
            <div class="box-padding pl-35 bg-branco links-aside mb-20">
                <?php
                if(count($taxonomies)):
                foreach ($taxonomies as $taxonomy):
               ?>
               <div class="box">
                 <a href="/noticias/index/categoria/<?php echo $taxonomy->slug;?>" class="p-10-0"><?php echo StringUtils::limite($taxonomy->name,30);?></a>
               </div>
               <?php endforeach;?>
               <?php endif;?> 
            </div>
            <ul class="banner-link box">
                <?php 
                  $banners = get_posts(array('post_type' => 'banners','numberposts' => -1,"meta_query" => array(
                    'relation' => 'AND', // Optional, defaults to "AND"
                    array(
                            'key'     => 'destaque',
                            'value'   => 'nao',
                            'compare' => 'LIKE'
                    )
                    )));
                  if(count($banners)):
                  foreach ($banners as $key => $banner):
                  $campos = get_fields($banner->ID);
                ?>
                    <li class="bg-azul mb-20">
                        <a target="<?php echo $campos["trajeto"];?>" href="<?php echo $campos["url"] ? $campos["url"] : "javascript:;";?>" class="bg-azul" style="background-image: url('<?php echo $this->createIMG($banner->ID, $campos['imagem'], 362,180, true);?>');"></a>
                    </li>
                <?php 
                endforeach;
                endif;
                ?>
            </ul>
        </aside>
    </div>    
</section>