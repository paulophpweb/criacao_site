<?php 
$configurar = get_page_by_path("configuracoes");
$camposConfigurar = get_fields($configurar->ID);
$aVars = $this->admin_post->vars;
$page = 0;
$limite = 4;
$cond = array('post_type' => 'agenda');
$total_post = count(get_posts($cond));
$totalPagina = ceil($total_post/$limite);
if(!empty($aVars["page"])){
  $page = ($aVars["page"]*$limite)-$limite;
}
$cond = array('post_type' => 'agenda','numberposts' => $limite,'orderby' => 'data','order' => "DESC",'offset' => $page);
?>
<section class="noticias">
    <div class="container noPaddingXs">
        <article class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container noPaddingXs">
            <div class="box-padding-1020 bg-cinza-claro pl-35">
                <p class="cinza m-0">Você está aqui: Página Inicial / Agenda</p>
            </div>
            <div class="box-padding-35 bg-branco">
               <div class="box-title">
               		<figure><img src="<?php echo $this->getURL("jornal.png"); ?>" alt=""></figure>
	                <h4 class="azul v-a" style="text-transform: uppercase;">AGENDA DA <?php echo $camposConfigurar["tipo"]; ?></h4>
	            </div>
	            <ul class="box-padding bg-branco noticias">
	                <?php 
	                  $agendas = get_posts($cond);
	                  if(count($agendas)):
	                  foreach ($agendas as $key => $agenda):
	                  $campos = get_fields($agenda->ID);
	                ?>
	                    <li class="box-bb-cinza pb-10 pt-20">
	                        <p class="data-verde"><?php echo DateUtils::format(array("post_date" => $campos["data"],"format" => "d.m"));?></p>
	                        <a class="cinza calc-65" href="/agenda/<?php echo $agenda->post_name;?>"><?php echo $agenda->post_title;?></a>
	                    </li>
	                <?php 
	                endforeach;
	                endif;
	                ?>
	            </ul>
	            <?php 
                  if (function_exists("wp_bs_pagination"))
                      {
                           wp_bs_pagination($totalPagina,2);
                  }
                ?>
            </div>
            <div class="box mb-20">
                <a href="javascript:;" class="ir-topo h5 cinza-claro scrollToDiv">Ir para o topo <span></span></a>
            </div>
        </article>
    </div>
</section>