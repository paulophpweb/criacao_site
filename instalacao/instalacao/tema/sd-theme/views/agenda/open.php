<?php 

$configurar = get_page_by_path("configuracoes");
$camposConfigurar = get_fields($configurar->ID);

$agenda = get_post();
$campos = get_fields($agenda->ID);

?>
<section class="noticias">
    <div class="container noPaddingXs">
        <article class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container noPaddingXs">
            <div class="box-padding-1020 bg-cinza-claro pl-35">
                <p class="cinza m-0">Você está aqui: Página Inicial / Agenda / <?php echo $agenda->post_title; ?></p>
            </div>
            <div class="box-padding-35 bg-branco">
               <div class="box-title">
               		<figure><img src="<?php echo $this->getURL("jornal.png"); ?>" alt=""></figure>
	                <h4 class="azul v-a" style="text-transform: uppercase;">AGENDA DA <?php echo $camposConfigurar["tipo"]; ?></h4>
	            </div>
	            <ul class="box-padding bg-branco noticias">
                    <li class="box-bb-cinza pb-10 pt-20">
                        <p class="data-verde"><?php echo DateUtils::format(array("post_date" => $campos["data"],"format" => "d.m"));?></p>
                        <a class="cinza calc-65" href="/agenda/<?php echo $agenda->post_name;?>"><?php echo $agenda->post_title;?></a>
                        <p><?php echo $campos["texto"]; ?></p>
                    </li>
	            </ul>
            </div>
            <div class="box mb-20">
                <a href="javascript:;" class="ir-topo h5 cinza-claro scrollToDiv">Ir para o topo <span></span></a>
            </div>
        </article>
    </div>
</section>