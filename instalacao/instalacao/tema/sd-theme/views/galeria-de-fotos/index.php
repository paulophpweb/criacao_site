<?php 
$taxonomies=get_terms('categorias-fotos',array(
    'hide_empty' => false
));
$aVars = $this->admin_post->vars;
$cond = array('post_type' => 'galeria-de-fotos','numberposts' => -1,'orderby' => 'post_date','order' => "DESC");
$categoria = "Todas";
if(isset($aVars["categoria"])){
      $cond['tax_query'] =  array(
          array(
              'taxonomy' => 'categorias-fotos',
              'field' => "slug",
              'terms' => $aVars["categoria"],
          )
      );
      $categoria = get_term_by("slug",$aVars["categoria"],"categorias-fotos")->name;
  }
?>
<section class="galeria">
    <div class="container noPaddingXs">
        <article class="col-lg-8 col-md-8 col-sm-7 col-xs-12 container noPaddingXs">
            <div class="box-padding-1020 bg-cinza-claro pl-35">
                <p class="cinza m-0">Você está aqui: Página Inicial / Galeria de Fotos</p>
            </div>
            <div class="box-padding-35 bg-branco">
                <div class="box">
                    <h2>Categoria: <?php echo $categoria; ?></h2>
                </div>
                <!--<div class="box">
                    <select name="" class="branco estilizado">
                        <option>Filtrar por pedido</option>
                        <option>Filtrar por pedido</option>
                        <option>Filtrar por pedido</option>
                    </select>
                </div>-->
                <?php 
                  $fotos = get_posts($cond);
                  if(count($fotos)):
                  foreach ($fotos as $key => $foto):
                  $campos = get_fields($foto->ID);
                ?>
                    <a data-id="<?php echo $foto->ID; ?>" href="/galeria-de-fotos/<?php echo $foto->post_name;?>" class="box-galeria box galeriaFotos">
                        <figure style="background-image: url(<?php echo $this->createIMG($foto->ID, $campos["capa"], 607,405, true); ?>);"></figure>
                        <span class="box">
                            <span class="box"><span class="date h5 cinza-claro"><?php echo DateUtils::format(array("post_date" => $foto->post_date,"format" => "d.m.Y"));?></span></span>
                            <span class="box"><span class="h3"><?php echo $foto->post_title; ?></span></span>
                            <span class="box"><span class="h5 cinza-claro"><?php echo $campos["breve_descricao"]; ?> - <?php echo count($campos['galeria']); ?> fotos <?php echo $campos["fotografo"] ? "por ".$campos["fotografo"] : ""; ?></span></span>
                        </span>
                        <span class="divide"></span>
                    </a>
                <?php 
                endforeach;
                else:
                ?>

              <h3>Nenhuma galeria de fotos encontrada para esta categoria!</h3>

              <?php endif; ?>
            </div>
            <div class="box mb-20">
                <a href="javascript:;" class="ir-topo h5 cinza-claro scrollToDiv">Ir para o topo <span></span></a>
            </div>
        </article>
        <aside class="col-lg-4 col-md-4 col-sm-5 col-xs-12 container noPaddingXs">
            <div class="box-padding-1020 bg-cinza-claro pl-35">
                <p class="cinza m-0">Categoria de fotos</p>
            </div>
            <div class="box-padding pl-35 bg-branco links-aside mb-20">
               <div class="box">
                 <a href="/galeria-de-fotos" class="p-10-0 <?php echo $aVars["categoria"] ? "" : "selecionado"; ?>">Todas</a>
               </div>
               <?php
                if(count($taxonomies)):
                foreach ($taxonomies as $taxonomy):
               ?>
               <div class="box">
                 <a href="/galeria-de-fotos/index/categoria/<?php echo $taxonomy->slug;?>" class="p-10-0 <?php echo $aVars["categoria"] == $taxonomy->slug ? "selecionado" : ""; ?>"><?php echo StringUtils::limite($taxonomy->name,30);?></a>
               </div>
               <?php endforeach;?>
               <?php endif;?>                              
            </div>
            <ul class="banner-link box">
                <?php 
                  $banners = get_posts(array('post_type' => 'banners','numberposts' => -1,"meta_query" => array(
                    'relation' => 'AND',
                    array(
                            'key'     => 'destaque',
                            'value'   => 'nao',
                            'compare' => 'LIKE'
                    )
                    )));
                  if(count($banners)):
                  foreach ($banners as $key => $banner):
                  $campos = get_fields($banner->ID);
                ?>
                    <li class="bg-azul mb-20">
                        <a target="<?php echo $campos["trajeto"];?>" href="<?php echo $campos["url"] ? $campos["url"] : "javascript:;";?>" class="bg-azul" style="background-image: url('<?php echo $this->createIMG($banner->ID, $campos['imagem'], 362,180, true);?>');"></a>
                    </li>
                <?php 
                endforeach;
                endif;
                ?>
            </ul>           
        </aside>
    </div>
</section>