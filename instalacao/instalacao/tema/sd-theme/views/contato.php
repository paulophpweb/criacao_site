<?php 
  $contato = get_page_by_path("contato");
  $campos = get_fields($contato->ID);

  $configurar = get_page_by_path("configuracoes");
  $camposConfigurar = get_fields($configurar->ID);

?>
<input type="hidden" name="latitudeMapa" id="latitudeMapa" value="<?php echo $campos["localizacao"]["lat"]?>"/>
<input type="hidden" name="longitudeMapa" id="longitudeMapa" value="<?php echo $campos["localizacao"]["lng"]?>"/>
<input type="hidden" name="enderecoMapa" id="enderecoMapa" value="<?php echo $campos["localizacao"]["address"]?>"/>
<section class="contato">
    <div class="container noPaddingXs">
        <article class="col-lg-8 col-md-8 col-sm-8 col-xs-12 noPaddingXs">
            <div class="box-padding-1020 bg-cinza-claro pl-35">
                <p class="cinza m-0">Você está aqui: Página Inicial / Contato</p>
            </div>
            <div class="box-padding-35 bg-branco posr">
                <div class="box mb-20">
                    <figure><img src="<?php echo $this->getURL("icone-contato.png"); ?>" alt=""></figure>
                    <h4 class="azul v-a">Localização da <?php echo $camposConfigurar["tipo"]; ?></h4>
                </div>
                <div class="box mapa-cheio mb-20" id="map"></div>
                <div class="box">
                    <p class="cinza-bem-claro fz-14 m-0">Endereço da <?php echo $camposConfigurar["tipo"]; ?></p>
                    <div class="clearfix"></div>
                    <p class="fz-16"><?php echo $campos["localizacao"]["address"]?></p>
                </div>
                <div class="box">
                    <div class="divide-contato bg-cinza-claro"></div>
                </div>
                <div class="box lista-contatos mb-10 hidden-sm hidden-xs">
                    <h5 class="cinza-bem-claro fz-14 light">Contatos</h5>
                    <div class="box bg-cinza-claro mb-10 linha-um">
                        <div class="box-40 coluna-um"><p class="h4 cinza">Unidade</p></div>
                        <div class="box-20"><p class="h4 cinza">Telefone</p></div>
                        <div class="box-20"><p class="h4 cinza">Email</p></div>
                    </div>
                    <?php if(count($campos["contatos"])):
                        foreach ($campos["contatos"] as $key => $value):
                    ?>
                    <div class="box linha-dois">
                        <div class="box-40 coluna-um"><p><?php echo $value["unidade"]; ?></p></div>
                        <div class="box-20"><p><?php echo $value["telefone"]; ?></p></div>
                        <div class="box-20"><p><?php echo $value["email"]; ?></p></div>
                    </div>
                    <?php 
                        endforeach;
                        endif;
                    ?>
                </div>
                <div class="box lista-contatos mb-10 hidden-lg hidden-md">
                    <h5 class="cinza-bem-claro fz-14 light">Contatos</h5>
                    
                    <?php if(count($campos["contatos"])):
                        foreach ($campos["contatos"] as $key => $value):
                    ?>
                    <div class="box bg-cinza-claro mb-10 linha-um hidden-xs">
                        <div class="box-40 coluna-um"><p class="h4 cinza">Unidade</p></div>
                        <div class="box-20"><p class="h4 cinza">Telefone</p></div>
                        <div class="box-20"><p class="h4 cinza">Email</p></div>
                    </div>
                    <div class="box linha-dois">
                        <div class="box-40 coluna-um"><p><?php echo $value["unidade"]; ?></p></div>
                        <div class="box-20"><p><?php echo $value["telefone"]; ?></p></div>
                        <div class="box-20"><p><?php echo $value["email"]; ?></p></div>
                    </div>
                    <?php 
                        endforeach;
                        endif;
                    ?>
                </div>
                <div class="box mb-10">
                    <div class="divide-contato bg-cinza-claro"></div>
                </div>       
                <div class="box mb-20">
                    <p class="cinza-bem-claro fz-14 m-0">Formulário de contato</p>
                </div> 
                <div class="box mb-20">
                    <form action="<?php echo $this->getLink(array('contato', 'enviar')); ?>" id="formulario" method="post" class="mb-20">
                        <label for="nome" class="fz-18 light cinza">Seu nome</label>
                        <input name="nome" required="required" data-msg="Nome completo Obrigatório" type="text" placeholder="Insira o seu nome completo">
                        <label for="email" class="fz-18 light cinza">Seu e-mail</label>
                        <input name="email" required="required" data-msg="Email Obrigatório" type="email" placeholder="Insira o seu endereço de e-mail">
                        <label for="assunto" class="fz-18 light cinza">Assunto</label>
                        <input name="assunto" type="text" placeholder="Insira o assunto da mensagem">
                        <label for="mensagem" class="fz-18 light cinza">Mensagem</label>
                        <textarea required="required" data-msg="Mensagem Obrigatório" name="mensagem" placeholder="Insira a mensagem que você quer enviar."></textarea>
                        <button type="submit">Enviar</button>
                    </form>
                </div> 
            </div>
            <div class="box"><a href="javascript:;" class="ir-topo h5 cinza-claro scrollToDiv">Ir para o topo <span></span></a></div>                      
        </article>
        <aside class="col-lg-4 col-md-4 col-sm-4 col-xs-12 noPaddingXs">
            <div class="box-padding-1020 bg-cinza-claro pl-35">
                <p class="cinza m-0">Acesse nossas redes sociais:</p>
            </div>
            <div class="box-padding bg-branco mb-20">
                <ul class="siga-redes">
                    <?php if($campos["facebook"]): ?>
                        <li class="bg-cinza-escuro">
                            <a href="<?php echo $campos["facebook"]; ?>" target="_blank">
                                <img src="<?php echo $this->getURL("face.png"); ?>" alt="">
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if($campos["twitter"]): ?>
                        <li class="bg-cinza-escuro">
                            <a href="<?php echo $campos["twitter"]; ?>" target="_blank">
                                <img src="<?php echo $this->getURL("twt.png"); ?>" alt="">
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if($campos["youtube"]): ?>
                        <li class="bg-cinza-escuro">
                            <a href="<?php echo $campos["youtube"]; ?>" target="_blank">
                                <img src="<?php echo $this->getURL("ytb.png"); ?>" alt="">
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if($campos["linkedin"]): ?>
                        <li class="bg-cinza-escuro">
                            <a href="<?php echo $campos["linkedin"]; ?>" target="_blank">
                                <img src="<?php echo $this->getURL("linkeddin.png"); ?>" alt="">
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if($campos["instagram"]): ?>
                        <li class="bg-cinza-escuro">
                            <a href="<?php echo $campos["instagram"]; ?>" target="_blank">
                                <img src="<?php echo $this->getURL("insta.png"); ?>" alt="">
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if($campos["google_plus"]): ?>
                        <li class="bg-cinza-escuro">
                            <a href="<?php echo $campos["google_plus"]; ?>" target="_blank">
                                <img src="<?php echo $this->getURL("gplus.png"); ?>" alt="">
                            </a>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
            <ul class="banner-link box">
                <?php 
                  $banners = get_posts(array('post_type' => 'banners','numberposts' => -1,"meta_query" => array(
                    'relation' => 'AND', // Optional, defaults to "AND"
                    array(
                            'key'     => 'destaque',
                            'value'   => 'sim',
                            'compare' => 'LIKE'
                    ),
                     array(
                            'key'     => 'local_de_exibicao',
                            'value'   => 'interna_right',
                            'compare' => 'LIKE'
                    )
                    )));
                  if(count($banners)):
                  foreach ($banners as $key => $banner):
                  $campos = get_fields($banner->ID);
                ?>
                    <li class="bg-azul mb-20">
                        <a target="<?php echo $campos["trajeto"];?>" href="<?php echo $campos["url"] ? $campos["url"] : "javascript:;";?>" class="bg-azul" style="background-image: url('<?php echo $this->createIMG($banner->ID, $campos['imagem'], 362,180, true);?>');"></a>
                    </li>
                <?php 
                endforeach;
                endif;
                ?>
            </ul>           
        </aside>
    </div>
</section>