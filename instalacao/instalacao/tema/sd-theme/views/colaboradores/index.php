<?php 
$configurar = get_page_by_path("configuracoes");
$camposConfigurar = get_fields($configurar->ID);
?>
<section class="noticias">
    <div class="container noPaddingXs">
        <article class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container noPaddingXs">
            <div class="box-padding-1020 bg-cinza-claro pl-35">
                <p class="cinza m-0">Você está aqui: Página Inicial / <?php echo $camposConfigurar["tipo"]; ?></p>
            </div>
            <div class="box-padding-35 bg-branco">
               <div class="table-container">
					<table class="table table-filter">
						<tbody>
							<?php 
                  $cond = array('post_type' => 'colaborador','numberposts' => -1,"order" => "ASC","orderby" => "title");
                  if($camposConfigurar["tipo"] == "Câmara"){
                    $cond["meta_query"] = array(
                    'relation' => 'AND', // Optional, defaults to "AND"
                    array(
                            'key'     => 'tipo',
                            'value'   => 'parlamentar',
                            'compare' => 'LIKE'
                    ));
                  }else{
                    $cond["meta_query"] = array(
                    'relation' => 'AND', // Optional, defaults to "AND"
                    array(
                            'key'     => 'tipo',
                            'value'   => 'equipe',
                            'compare' => 'LIKE'
                    ));
                  }
                  $pessoas = get_posts($cond);
                  if(count($pessoas)):
                  foreach ($pessoas as $key => $pessoa):
                  $campos = get_fields($pessoa->ID);
                ?>
                <?php if($camposConfigurar["tipo"] == "Câmara"): ?>
								<tr>
									<td>
										<div class="media">
											<a href="/colaborador/<?php echo $pessoa->post_name;?>" class="pull-left">
												<img src="<?php echo $this->createIMG($pessoa->ID, $campos['imagem'], 128,128, true);?>" class="media-photo">
											</a>
											<div class="media-body">
												<span class="media-meta pull-right"><?php echo $campos["telefone"]; ?></span>
												<h4 class="title">
													<?php echo $pessoa->post_title; ?>
												</h4>
												<p class="summary">
													<strong>Partido: </strong> <?php echo $campos["partido"]; ?><br>
                          <strong>Gabinete: </strong> <?php echo $campos["gabinete"]; ?>
												</p>
											</div>
										</div>
									</td>
								</tr>
								<?php else: ?>
								<tr>
									<td>
										<div class="media">
											<a href="/colaborador/<?php echo $pessoa->post_name;?>" class="pull-left">
												<img src="<?php echo $this->createIMG($pessoa->ID, $campos['imagem'], 128,128, true);?>" class="media-photo">
											</a>
											<div class="media-body">
												<span class="media-meta pull-right"><?php echo $campos["telefone"]; ?></span>
												<h4 class="title">
													<?php echo $pessoa->post_title; ?>
												</h4>
												<p class="summary">
													<strong>Cargo: </strong> <?php echo $campos["cargo"]; ?>
												</p>
											</div>
										</div>
									</td>
								</tr>
								<?php endif; ?>
							  <?php 
                endforeach;
                endif;
                ?>
						</tbody>
					</table>
				</div>
            </div>
            <div class="box mb-20">
                <a href="javascript:;" class="ir-topo h5 cinza-claro scrollToDiv">Ir para o topo <span></span></a>
            </div>
        </article>
    </div>
</section>