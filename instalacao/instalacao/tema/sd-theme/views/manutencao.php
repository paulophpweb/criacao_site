<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?php bloginfo('name'); ?> | <?php bloginfo('description'); ?></title>
        <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=us-ascii; ?>" />
        <?php
            wp_enqueue_style('sd', plugins_url("sd")."/css/admin.css");
            wp_enqueue_style( 'sd-theme', theme_url("css/SDManutencao.css"));
            //wp_enqueue_style( 'style-name', get_stylesheet_uri() );

            wp_deregister_script('jquery');
            wp_register_script('jquery', ("http://code.jquery.com/jquery-1.3.2.min.js"), false, '1.3.2');
            wp_enqueue_script("countdown", plugins_url("sd/js/jquery.countdown.js"), array("jquery"), true, true);
            wp_enqueue_script("manutencao", theme_url("js/SDManutencao.js"), array("countdown"), true, true);
        ?>
        <?php wp_head(); ?>
    </head>
<body>
<?php
    $post = get_page_by_path('canais',OBJECT,'page');
    $redes_sociais = get_field("redes_sociais", $post->ID);
    $telefones = get_field("telefones", $post->ID);
    $emails = get_field("emails", $post->ID);

    $post = get_page_by_path('thema-config',OBJECT,'page');
    $horario_voltar_site = get_field("horario_voltar_site", $post->ID);
    $hvs = new DateTime($horario_voltar_site."160000");

    $logo_cliente_manutencao = get_field("logo_cliente_manutencao", $post->ID);
    if(empty($logo_cliente_manutencao)){
        $logo_cliente_manutencao = array(
            "url"=>plugins_url("sd/images/logo-sitio.png"),
            "alt"=>"Sítio Digital",
            "title"=>"Sítio Digital"
        );
    }
    else if(empty($logo_cliente_manutencao["alt"]))$logo_cliente_manutencao["alt"] = $logo_cliente_manutencao["title"];

    $frase_manutencao = get_field("frase_manutencao", $post->ID);
    
?>
    <div class="container">
        <img src="<?php echo $logo_cliente_manutencao["url"]; ?>" alt="<?php echo $logo_cliente_manutencao["alt"]; ?>" title="<?php echo $logo_cliente_manutencao["title"]; ?>"/>
        <p><?php echo $frase_manutencao; ?></p>
        <div id="countdown_wrapper" class="prepend-6 span-18" data-d="<?php echo $hvs->format("j"); ?>" data-m="<?php echo $hvs->format("m"); ?>" data-y="<?php echo $hvs->format("Y"); ?>">
            
        </div>
        <nav class="redes">
            <a class="btn-sd" title="Desenvolvido por Sítio Digital" href="http://www.sitiodigital.com.br" target="_blank">
                <i class="sd"></i>
            </a>
            <?php
                if(count($emails)>0):
                $email = $emails[0]["email"];
            ?>
            <a class="btn-footer" href="mailto:<?php echo  $email; ?>" title="<?php echo  $email; ?>">
                <i class="email"></i>
            </a>
            <?php
                endif;
                foreach ($redes_sociais as $key => $value):
            ?>
	        <a class="btn-footer" href="<?php echo $value["link"]; ?>" target="_blank" title="<?php echo $value["link"]; ?>">
                <i class="<?php echo $value["nome"]; ?>"></i>
            </a>
            <?php
                endforeach;
                if(count($telefones)>0):
                    $tel = $telefones[0]["ddd"].$telefones[0]["numero"];
                    $tel_formated_ddd = preg_replace('~.*(\d{3})[^\d]{0,7}(\d{4})[^\d]{0,7}(\d{4}).*~', '($1) $2.$3', $tel);
                    $tel_formated = preg_replace('~.*(\d{3})[^\d]{0,7}(\d{4})[^\d]{0,7}(\d{4}).*~', '$2.$3', $tel);
            ?>
            <a class="btn-footer ic-ligar" href="tel:+55<?php echo $tel; ?>" title="<?php echo $tel_formated_ddd; ?>">
                <i class="telefone_2"></i>
            </a>

            <a class="btn-footer numero-telefone" href="tel:+55<?php echo $tel; ?>" title="<?php echo $tel_formated_ddd; ?>">
                <?php echo $tel_formated; ?>
            </a>
            <?php endif; ?>
        </nav>
    </div>
<?php wp_footer(); ?>
</body>
</html>