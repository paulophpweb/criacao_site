<?php ob_start(); ?>
<!DOCTYPE html>
<html moznomarginboxes mozdisallowselectionprint <?php language_attributes();?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<!-- Chrome, Firefox OS and Opera -->
	<meta name="theme-color" content="#a6ce39">
	<!-- Windows Phone -->
	<meta name="msapplication-navbutton-color" content="#a6ce39">
	<!-- iOS Safari -->
	<meta name="apple-mobile-web-app-status-bar-style" content="#a6ce39">

    <link rel='stylesheet'  href='<?php echo $this->getLink(array("index","custom_css")); ?>' type='text/css' media='all' />
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDavVznsyWhGliiM-5Bp94mw1_1m8tfmJo"></script>
	
	<?php wp_head(); ?>

    <script>
        var baseUrl = "<?php echo "http://$_SERVER[HTTP_HOST]"; ?>";
    </script>
    <?php ob_end_flush(); flush();?>
</head>
<?php 
$configurar = get_page_by_path("configuracoes");
$campos = get_fields($configurar->ID);

$contato = get_page_by_path("contato");
$camposContatao = get_fields($contato->ID);
?>

<body <?php body_class(); ?> style="background: #f2f2f2;">

	<header>
        <div class="bg-topo"><div class="gradiente"></div></div>
        <div class="container">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 noPaddingXs">
                <div class="logo-header bg-branco">
                    <a href="/"></a>
                </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
               <div class="box topo">
                    <div class="box up hidden-xs">
                       <div class="acessibilidade">
                            <a href="/acessibilidade" class="azul-claro">Acessibilidade: <i class="fa fa-wheelchair" aria-hidden="true"></i>
</a>
                       </div>
                       <ul class="redes-sociais">
                            <?php if($camposContatao["facebook"]): ?>
                            <li>
                                <a href="<?php echo $camposContatao["facebook"]; ?>" target="_blank">
                                    <i class="fa fa-facebook-square" aria-hidden="true"></i>
                                </a>
                            </li>
                            <?php endif; ?>
                            <?php if($camposContatao["twitter"]): ?>
                            <li>
                                <a href="<?php echo $camposContatao["twitter"]; ?>" target="_blank">
                                    <i class="fa fa-twitter-square" aria-hidden="true"></i>
                                </a>
                            </li>
                            <?php endif; ?>
                            <?php if($camposContatao["youtube"]): ?>
                            <li>
                                <a href="<?php echo $camposContatao["youtube"]; ?>" target="_blank">
                                    <i class="fa fa-youtube-square" aria-hidden="true"></i>
                                </a>
                            </li>
                            <?php endif; ?>
                            <?php if($camposContatao["linkedin"]): ?>
                            <li>
                                <a href="<?php echo $camposContatao["linkedin"]; ?>" target="_blank">
                                    <i class="fa fa-linkedin-square" aria-hidden="true"></i>
                                </a>
                            </li>
                            <?php endif; ?>
                            <?php if($camposContatao["instagram"]): ?>
                            <li>
                                <a href="<?php echo $camposContatao["instagram"]; ?>" target="_blank">
                                    <i class="fa fa-instagram" aria-hidden="true"></i>
                                </a>
                            </li>
                            <?php endif; ?>
                            <?php if($camposContatao["google_plus"]): ?>
                            <li>
                                <a href="<?php echo $camposContatao["google_plus"]; ?>" target="_blank">
                                    <i class="fa fa-google-plus-square" aria-hidden="true"></i>
                                </a>
                            </li>
                            <?php endif; ?>
                        </ul>
                       <div class="siga"><h5 class="azul-claro">Siga na rede:</h5></div>
                    </div>
                    <div class="box center">
                        <h3 class="branco" style="text-transform: uppercase;"><?php echo $campos["tipo"]; ?> MUNICIPAL DE</h3>
                        <form action="/resultado" id="buscar" class="hidden-xs" method="post">
                            <button type="submit">
                                <i class="fa fa-search" aria-hidden="true"></i>
                            </button>
                            <input name="query" type="text" placeholder="Pesquisar no site">
                        </form>
                    </div>
                    <div class="box titulo-header">
                        <h1 class="branco" style="text-transform: uppercase;"><?php echo $campos["cidade"]; ?></h1>
                    </div>
                </div>
                <div class="botao-menu-responsivo hidden-lg hidden-md hidden-sm">
                    <a href="">Menu<span></span></a>
                </div>
            </div> 
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPaddingXs">
                <div class="box posr">
                    <div class="menu-container hidden-xs">
                        <ul class="menu clearfix">
                            <li><a href="/" class="botao bg-verde">Início</a></li>
                            <li><a href="javascript:;" class="botao bg-verde multilevel">Institucional <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span></a>
                                <ul class="sub-menu clearfix">
                                     <?php
                                      $institucionais = get_posts(array('post_type' => 'institucional', 'posts_per_page'=>-1));
                                      if(count($institucionais)):
                                      foreach ($institucionais as $key => $institucional):
                                      $campos = get_fields($institucional->ID);
                                    ?>
                                    <li><a target="<?php echo $campos["trajeto"];?>" href="<?php echo $campos["url"] ? $campos["url"] : "institucional/".$institucional->post_name ?>" class="botao bg-verde branco"><?php echo $institucional->post_title; ?></a></li>
                                    <?php 
                                    endforeach;
                                    endif;
                                    ?>
                                </ul>
                            </li>
                            <li><a href="javascript:;" class="botao bg-verde multilevel">Serviços <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span></a>
                                <ul class="sub-menu clearfix">
                                     <?php 
                                      $servicos = get_posts(array('post_type' => 'servicos', 'posts_per_page'=>-1));
                                      if(count($servicos)):
                                      foreach ($servicos as $key => $servico):
                                      $campos = get_fields($servico->ID);
                                    ?>
                                        <li><a target="<?php echo $campos["trajeto"];?>" href="<?php echo $campos["url"] ? $campos["url"] : "servicos/".$servico->post_name ?>" class="botao bg-verde branco"><?php echo $servico->post_title; ?></a><div class="clearfix"></div></li>
                                    <?php 
                                    endforeach;
                                    endif;
                                    ?>
                                </ul>
                            </li>
                            <li><a href="/noticias/index" class="botao bg-verde">Notícias</a></li>
                            <li><a href="/galeria-de-fotos" class="botao bg-verde">Fotos</a></li>
                            <li><a href="/contato" class="botao bg-verde">Contato</a></li>
                        </ul>
                    </div>
                </div>
            </div>           
        </div>
        <div class="menu-responsivo hidden-lg hidden-md hidden-sm">
            <div class="bg-responsivo">
                <div class="menu-container">
                    <ul class="menu clearfix">
                        <li><a href="/" class="botao bg-verde">Início</a></li>
                        <li><a href="javascript:;" class="botao bg-verde multilevel">Institucional</a>
                            <ul class="sub-menu clearfix">
                                <?php
                                  $institucionais = get_posts(array('post_type' => 'institucional', 'posts_per_page'=>-1));
                                  if(count($institucionais)):
                                  foreach ($institucionais as $key => $institucional):
                                  $campos = get_fields($institucional->ID);
                                ?>
                                    <li><a target="<?php echo $campos["trajeto"];?>" href="<?php echo $campos["url"] ? $campos["url"] : "institucional/".$institucional->post_name ?>" class="botao bg-verde branco"><?php echo $institucional->post_title; ?></a></li>
                                <?php 
                                endforeach;
                                endif;
                                ?>
                            </ul>
                        </li>
                        <li><a href="javascript:;" class="botao bg-verde multilevel">Serviços</a>
                            <ul class="sub-menu clearfix">
                                <?php 
                                  $servicos = get_posts(array('post_type' => 'servicos', 'posts_per_page'=>-1));
                                  if(count($servicos)):
                                  foreach ($servicos as $key => $servico):
                                  $campos = get_fields($servico->ID);
                                ?>
                                <li><a target="<?php echo $campos["trajeto"];?>" href="<?php echo $campos["url"] ? $campos["url"] : "servicos/".$servico->post_name ?>" class="botao bg-verde branco"><?php echo $servico->post_title; ?></a><div class="clearfix"></div></li>
                                <?php 
                                endforeach;
                                endif;
                                ?>
                            </ul>
                        </li>
                        <li><a href="/noticias/index" class="botao bg-verde">Notícias</a></li>
                        <li><a href="/galeria-de-fotos" class="botao bg-verde">Fotos</a></li>
                        <li><a href="/contato" class="botao bg-verde">Contato</a></li>
                    </ul>
                </div>
                <div class="box up">
                   <div class="acessibilidade">
                        <a href="javascript:;" class="branco">Acessibilidade:</a>
                   </div>
                   <ul class="redes-sociais">
                        <?php for($a=0; $a<3; $a++): ?>
                            <li><a href=""></a></li>
                        <?php endfor; ?>
                    </ul>
                   <div class="siga"><h5 class="branco">Siga na rede:</h5></div>
                </div>
                <form action="" id="buscar">
                    <button></button>
                    <input type="text" placeholder="Pesquisar no site">
                </form>
            </div>
        </div>
	</header>
	
	
