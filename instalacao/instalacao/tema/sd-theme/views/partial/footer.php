<?php 
$configurar = get_page_by_path("configuracoes");
$camposConfigurar = get_fields($configurar->ID);
?>	
	<footer class="">
		<div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPaddingXs">
                <div class="box bg-cinza-claro rodape">
                    <div class="col-lg-4 col-md-3 col-sm-3 col-xs-12 noPadding">
                        <div class="box bg-cinza logo-footer">
                            <figure>
                                <a href="/"></a>
                            </figure>
                            <div class="box cont">
                                <h5 class="cinza" style="text-transform: uppercase;"><?php echo $camposConfigurar["tipo"]; ?> DE</h5>
                                <h3 class="cinza" style="text-transform: uppercase;"><?php echo $camposConfigurar["cidade"]; ?></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-9 col-sm-9 col-xs-12">
                       <div class="box-padding menu-footer">
                            <nav class="nav-footer">
                                <h3>Institucional</h3>
                                 <?php 
                                  $institucionais = get_posts(array('post_type' => 'institucional'));
                                  if(count($institucionais)):
                                  foreach ($institucionais as $key => $institucional):
                                  $campos = get_fields($institucional->ID);
                                ?>
                                    <a target="<?php echo $campos["trajeto"];?>" href="<?php echo $campos["url"] ? $campos["url"] : "institucional/".$institucional->post_name ?>" class="cinza"><?php echo $institucional->post_title; ?></a>
                                <?php 
                                endforeach;
                                endif;
                                ?>
                            </nav>

                            <nav class="nav-footer">
                                <h3>Serviços</h3>
                                <?php 
                                  $servicos = get_posts(array('post_type' => 'servicos'));
                                  if(count($servicos)):
                                  foreach ($servicos as $key => $servico):
                                  $campos = get_fields($servico->ID);
                                ?>
                                    <a target="<?php echo $campos["trajeto"];?>" href="<?php echo $campos["url"] ? $campos["url"] : "servicos/".$servico->post_name ?>" class="cinza"><?php echo $servico->post_title; ?></a><br>
                                <?php 
                                endforeach;
                                endif;
                                ?>
                            </nav>

                            <nav class="nav-footer">
                                <h3>Fotos</h3>
                                <?php 
                                  $cond = array('post_type' => 'galeria-de-fotos','numberposts' => 1,'orderby' => 'rand');
                                  $fotos = get_posts($cond);
                                  if(count($fotos)):
                                  foreach ($fotos as $key => $foto):
                                  $campos = get_fields($foto->ID);
                                ?>
                                    <a href="/galeria-de-fotos/<?php echo $foto->post_name;?>" class="cinza"><?php echo $foto->post_title; ?></a>
                                <?php endforeach; endif; ?>
                            </nav>
                            
                             <nav class="nav-footer">
                                <h3>Contato</h3>
                                <a href="/contato" class="cinza">Informações para atendimento</a>
                            </nav>
                            
                        </div>
                        
                    </div>
                    <div class="box-padding rodape-bottom">
                       <div class="copyright">
                           <h5 class="cinza-claro">Copyright © <?php echo date("Y") ?> - <?php echo $camposConfigurar["tipo"]; ?> de <?php echo $camposConfigurar["cidade"]; ?></h5>
                       </div>
                        <div class="agencia">
                            <a href="http://www.megasoftgyn.com.br/" target="_blank" class="cinza-claro h5" title="Megasoft">Powered by</a>
                        </div>
                        
                    </div>
                </div>
		    </div>
		</div>
	</footer>
	<?php wp_footer(); ?>
	</body>
</html>


