<?php 
$acessibilidade = get_page_by_path("acessibilidade");
$campos = get_fields($acessibilidade->ID);
?>
<section class="noticias-interna">
    <div class="container">
        <article class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPaddingXs impressao100">
            <div class="box-padding-1020 bg-cinza-claro pl-35 removerImpressao">
                <p class="cinza m-0">Você está aqui: Página Inicial / Acessibilidade</p>
            </div>
            <div class="box-padding pl-35 bg-branco posr">
                <a href="javascript:;" onclick="window.print();" class="impressora removerImpressao"></a>
                <div class="botoes-interagir removerImpressao">
                    <!-- Icon-only Branded Twitter button -->
                    <a class="share-btn share-btn-branded share-btn-twitter"
                       href="https://twitter.com/share?url=<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>"
                       title="Share on Twitter">
                        <span class="share-btn-icon"></span>
                        <span class="share-btn-text-sr">Twitter</span>
                    </a>

                    <!-- Icon-only Branded Facebook button -->
                    <a class="share-btn share-btn-branded share-btn-facebook"
                       href="https://www.facebook.com/sharer/sharer.php?u=<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>"
                       title="Facebook">
                        <span class="share-btn-icon"></span>
                        <span class="share-btn-text-sr">Facebook</span>
                    </a>

                    <!-- Icon-only Branded Google+ button -->
                    <a class="share-btn share-btn-branded share-btn-googleplus"
                       href="https://plus.google.com/share?url=<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>"
                       title="Google+">
                        <span class="share-btn-icon"></span>
                        <span class="share-btn-text-sr">Google+</span>
                    </a>

                    <!-- Icon-only Branded LinkedIn button -->
                    <a class="share-btn share-btn-branded share-btn-linkedin"
                       href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>"
                       title="LinkedIn">
                        <span class="share-btn-icon"></span>
                        <span class="share-btn-text-sr">LinkedIn</span>
                    </a>

                    <!-- Icon-only Branded Pinterest button -->
                    <a class="share-btn share-btn-branded share-btn-pinterest"
                       href="https://www.pinterest.com/pin/create/button/?url=<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>"
                       title="Pinterest">
                        <span class="share-btn-icon"></span>
                        <span class="share-btn-text-sr">Pinterest</span>
                    </a>
                </div>
                <div class="box">
                    <?php echo $campos["texto"]; ?>
                </div>
            </div>
            <div class="box mb-20 removerImpressao">
                <a href="javascript:;" class="ir-topo h5 cinza-claro scrollToDiv">Ir para o topo <span></span></a>
            </div>
        </article>
    </div>    
</section>