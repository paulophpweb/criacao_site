<?php 
$configurar = get_page_by_path("configuracoes");
$camposConfigurar = get_fields($configurar->ID);
?>
<section>
    <div class="container">
        <div class="col-lg-8 col-md-7 col-sm-6 col-xs-12 noPaddingXs">
            <div class="banner box">
                <div class="list_carousel">
                    <ul id="banner">
                        <?php 
                          $banners = get_posts(array('post_type' => 'destaque'));
                          if(count($banners)):
                          foreach ($banners as $key => $banner):
                          $campos = get_fields($banner->ID);
                        ?>
                            <li data-titulo="<?php echo StringUtils::limite($banner->post_title,100); ?>" data-descricao="<?php echo StringUtils::limite($campos["texto"],200); ?>"  class="itemCarrousel<?php echo $key; ?>">
                                <a style="background-image: url('<?php echo $this->createIMG($banner->ID, $campos['imagem'], 747,300, true);?>');" href="<?php echo $campos["url"] ? $campos["url"] : "javascript:;";?>" target="<?php echo $campos["trajeto"];?>"></a>
                            </li>
                        <?php 
                        endforeach;
                        endif;
                        ?>
                    </ul>
                </div>
                <div class="arrow">
                    <a href="javascript:;" id="next"></a>
                    <a href="javascript:;" id="prev"></a>
                </div>
            </div>
            <div class="box-padding bg-branco mb-20">
                <a href="javascript:;" class="h3 cinza" id="tituloBanner"></a><br>
                <p class="cinza" id="descricaoBanner"></p>
            </div>
            <div class="box-title">
               <figure><img src="<?php echo $this->getURL("icone-noticia.png"); ?>" alt=""></figure>
                <h4 class="azul v-a">ÚLTIMAS NOTÍCIAS</h4>
            </div>
            <ul class="box-padding bg-branco noticias">
                <?php 
                  $noticias = get_posts(array('post_type' => 'noticias','numberposts' => 3,'orderby' => 'post_date','order' => "DESC","meta_query" => array(
                    'relation' => 'AND', // Optional, defaults to "AND"
                    array(
                            'key'     => 'destaque',
                            'value'   => 'sim',
                            'compare' => 'LIKE'
                    ))));
                  if(count($noticias)):
                  foreach ($noticias as $key => $noticia):
                  $campos = get_fields($noticia->ID);
                ?>
                    <li class="box-bb-cinza pb-10 pt-20">
                        <p class="data-verde"><?php echo DateUtils::format(array("post_date" => $noticia->post_date,"format" => "d.m"));?></p>
                        <a class="cinza calc-65" href="/noticia/<?php echo $noticia->post_name;?>"><?php echo StringUtils::limite($noticia->post_title,80);?></a>
                    </li>
                <?php 
                endforeach;
                endif;
                ?>
            </ul>
            <div class="box-padding bg-cinza-claro mb-20">
                <a href="/noticias" class="botao branco bg-cinza-medio">Ver todas as notícias</a>
            </div>
            <ul class="banner-link box">
                <?php 
                  $banners = get_posts(array('post_type' => 'banners','numberposts' => 4,"meta_query" => array(
                    'relation' => 'AND', // Optional, defaults to "AND"
                    array(
                            'key'     => 'destaque',
                            'value'   => 'sim',
                            'compare' => 'LIKE'
                    )
                    )));
                  if(count($banners)):
                  foreach ($banners as $key => $banner):
                  $campos = get_fields($banner->ID);
                ?>
                    <li class="bg-azul mb-20">
                        <a target="<?php echo $campos["trajeto"];?>" href="<?php echo $campos["url"] ? $campos["url"] : "javascript:;";?>" style="background-image: url('<?php echo $this->createIMG($banner->ID, $campos['imagem'], 362,180, true);?>');" class="bg-azul"></a>
                    </li>
                <?php 
                endforeach;
                endif;
                ?>
            </ul>
        </div>
        <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12 noPaddingXs">
            <div class="box-title">
               <figure><img src="<?php echo $this->getURL("icone-servicos.png"); ?>" alt=""></figure>
                <h4 class="azul v-a">Serviços</h4>
            </div>
            <div class="box-padding bg-branco mb-20">
                <?php 
                  $servicos = get_posts(array('post_type' => 'servicos',"meta_query" => array(
                    'relation' => 'AND', // Optional, defaults to "AND"
                    array(
                            'key'     => 'destaque',
                            'value'   => 'sim',
                            'compare' => 'LIKE'
                    )
                    )));
                  if(count($servicos)):
                  foreach ($servicos as $key => $servico):
                  $campos = get_fields($servico->ID);
                ?>
                    <a target="<?php echo $campos["trajeto"];?>" href="<?php echo $campos["url"] ? $campos["url"] : "servicos/".$servico->post_name ?>" class="botao branco bg-cinza-medio cem mb-10">
                        <?php echo StringUtils::limite($servico->post_title,35); ?>
                    </a>
                <?php 
                endforeach;
                endif;
                ?>
            </div>
            <div class="box-title">
               <figure><img src="<?php echo $this->getURL("icone-data.png"); ?>" alt=""></figure>
                <h4 class="azul v-a">Agenda da <?php echo $camposConfigurar["tipo"]; ?></h4>
            </div>
            <div class="box-padding bg-branco agendaScroll">
                <ul class="agenda box">
                    <?php 
                      $habilitaCategoria = false;
                      $agendas = get_posts(array('post_type' => 'agenda','numberposts' => 20,'orderby' => 'data','order' => "ASC"));
                      if(count($agendas)):
                      $mesCorrente = null;
                      foreach ($agendas as $key => $agenda):
                      $campos = get_fields($agenda->ID);
                      $mes = DateUtils::format(array("post_date" => $campos["data"],"format" => "F"));
                      if($mesCorrente == null || $mesCorrente != $mes):
                    ?>
                        <li class="text-center">
                            <h4><?php echo $mes; ?></h4>
                        </li>
                        <?php 
                        endif;
                        $mesCorrente = $mes;
                         ?>
                        <li class="box">
                            <a href="/agenda/<?php echo $agenda->post_name;?>">
                                <span class="date-azul branco bg-azul"><?php echo DateUtils::format(array("post_date" => $campos["data"],"format" => "d"));?></span>
                                <span class="titulo-agenda cinza"><?php echo StringUtils::limite($agenda->post_title,60);?></span>
                            </a>
                        </li>
                    <?php 
                    endforeach;
                    endif;
                    ?>
                </ul>
            </div>
            <div class="box-padding bg-cinza-claro mb-20">
                <a href="/agenda/index" class="botao branco bg-cinza-medio">Ver agenda completa</a>
            </div>
            <div class="box-title">
               <figure><img src="<?php echo $this->getURL("icone-parlamentar.png"); ?>" alt=""></figure>
                <?php if($camposConfigurar["tipo"] == "Câmara"): ?>
                <h4 class="azul v-a">Parlamentares</h4>
                <?php else: ?>
                <h4 class="azul v-a">Equipe</h4>
                <?php endif; ?>
            </div>
            <div class="box-padding bg-branco">
                <div class="parlamentar box-comum">
                    <?php if($camposConfigurar["tipo"] == "Câmara"): ?>
                    <?php 
                      $pessoas = get_posts(array('post_type' => 'colaboradores','numberposts' => 1,"orderby" => "rand","meta_query" => array(
                    'relation' => 'AND', // Optional, defaults to "AND"
                    array(
                            'key'     => 'tipo',
                            'value'   => 'parlamentar',
                            'compare' => 'LIKE'
                    )
                    )));
                      if(count($pessoas)):
                      foreach ($pessoas as $key => $pessoa):
                      $campos = get_fields($pessoa->ID);
                    ?>
                        <figure><img id="imagemPessoa" src="<?php echo $campos["imagem"]["url"]; ?>" alt=""></figure>
                        <div class="info">
                            <p class="nome" id="nomePessoa"><?php echo $pessoa->post_title; ?></p>
                            <p class="h5 cinza" id="partidoPessoa">Partido: <?php echo $campos["partido"]; ?></p>
                            <p class="h5 cinza" id="gabinetePessoa">Gabinete n <?php echo $campos["gabinete"]; ?></p>
                            <p class="h5 cinza" id="telefonePessoa">Telefone: <?php echo $campos["telefone"]; ?></p>
                            <a href="/colaboradores/<?php echo $pessoa->post_name;?>" id="detalhesPessoa" class="mais">Mais informações</a>
                        </div>
                    <?php 
                    endforeach;
                    endif;
                    ?>
                    <?php else: ?>
                        <?php 
                          $pessoas = get_posts(array('post_type' => 'colaboradores','numberposts' => 1,"orderby" => "rand","meta_query" => array(
                    'relation' => 'AND', // Optional, defaults to "AND"
                    array(
                            'key'     => 'tipo',
                            'value'   => 'equipe',
                            'compare' => 'LIKE'
                    )
                    )));
                          if(count($pessoas)):
                          foreach ($pessoas as $key => $pessoa):
                          $campos = get_fields($pessoa->ID);
                        ?>
                            <figure><img id="imagemPessoa" src="<?php echo $campos["imagem"]["url"]; ?>" alt=""></figure>
                            <div class="info">
                                <p class="nome" id="nomePessoa"><?php echo $pessoa->post_title; ?></p>
                                <p class="h5 cinza" id="cargoPessoa">Cargo: <?php echo $campos["cargo"]; ?> </p>
                                <p class="h5 cinza" id="telefonePessoa">Telefone: <?php echo $campos["telefone"]; ?></p>
                                <a href="/colaboradores/<?php echo $pessoa->post_name;?>" id="detalhesPessoa" class="mais">Mais informações</a>
                            </div>
                        <?php 
                        endforeach;
                        endif;
                        ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="box-padding bg-cinza-claro mb-20">
                <select name="updatePessoa" class="branco bg-cinza-medio estilizado" id="custom_tamanho">
                    <option value="">Selecionar outro</option>
                    <?php 
                      $cond = array('post_type' => 'colaboradores','numberposts' => -1,"order" => "ASC","orderby" => "title");
                      if($camposConfigurar["tipo"] == "Câmara"){
                        $cond["meta_query"] = array(
                        'relation' => 'AND', // Optional, defaults to "AND"
                        array(
                                'key'     => 'tipo',
                                'value'   => 'parlamentar',
                                'compare' => 'LIKE'
                        ));
                      }else{
                        $cond["meta_query"] = array(
                        'relation' => 'AND', // Optional, defaults to "AND"
                        array(
                                'key'     => 'tipo',
                                'value'   => 'equipe',
                                'compare' => 'LIKE'
                        ));
                      }

                      $pessoas = get_posts($cond);
                      if(count($pessoas)):
                      foreach ($pessoas as $key => $pessoa):
                      $campos = get_fields($pessoa->ID);
                    ?>
                        <option value="<?php echo $pessoa->ID; ?>"><?php echo $pessoa->post_title; ?></option>
                    <?php 
                    endforeach;
                    endif;
                    ?>
                </select>
            </div>
            <?php 
                $contato = get_page_by_path("contato");
                $camposContato = get_fields($contato->ID);
            ?>
            <input type="hidden" name="latitudeMapa" id="latitudeMapa" value="<?php echo $camposContato["localizacao"]["lat"]?>"/>
            <input type="hidden" name="longitudeMapa" id="longitudeMapa" value="<?php echo $camposContato["localizacao"]["lng"]?>"/>
            <input type="hidden" name="enderecoMapa" id="enderecoMapa" value="<?php echo $camposContato["localizacao"]["address"]?>"/>
            <div class="box-title">
               <figure><img src="<?php echo $this->getURL("icone-contato.png"); ?>" alt=""></figure>
                <h4 class="azul v-a">Localização da <?php echo $camposConfigurar["tipo"]; ?></h4>
            </div>
            <div class="box">
                <div class="mapa" id="map"></div>
                <div class="box-padding bg-branco">
                    <h5 class="cinza-claro noMargin">Endereço da <?php echo $camposConfigurar["tipo"]; ?></h5>
                    <p class="cinza noMargin"><?php echo $camposContato["localizacao"]["address"]?></p>
                </div>
            </div>
            <div class="box-padding bg-cinza-claro">
                <a href="/contato" class="botao branco bg-cinza-medio">Ver todos os contatos</a>
            </div>
        </div>
    </div>
</section>




