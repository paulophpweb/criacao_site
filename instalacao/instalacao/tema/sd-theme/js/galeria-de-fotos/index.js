(function($){
	$(document).ready(function(){
		$(".galeriaFotos").click(function(e){
			e.preventDefault();
			$.get( baseUrl+"/galeria-de-fotos/get/",{id:$(this).attr("data-id")}, function( data ) {
				console.log(data);
			   $.fancybox.open(data,{
				    'openEffect'    :   'elastic',
				    'closeEffect'   :   'elastic',
				    'nextEffect'    :   'fade',
				    'openSpeed'     :   600, 
				    'closeSpeed'    :   200,
				    helpers : {
			            title : {
			              type: 'outside'
			            },
			            thumbs  : {
			              width : 50,
			              height  : 50
			            }
			          }
				});
			},'json');
		});
	});
})(jQuery);