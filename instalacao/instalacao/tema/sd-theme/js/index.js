(function($){
	$(document).ready(function(){
        initMap($);
        // Inicio Carousel Prefa
        var owl = $("#banner").owlCarousel({
            pagination: false,
            navigation: false,
            navigationText: false,
            autoPlay: true,
            transitionStyle : "fadeUp",
            rewindNav: true,
            responsive: true,
            itemsCustom: [
	                       [0, 1],
	                       [450, 1],
	                       [600, 1],
	                       [700, 1],
	                       [1000, 1],
	                       [1200, 1],
	                       [1400, 1],
	                       [1600, 1]
	                     ],
            afterMove: function(elem){
                var current = this.currentItem;
                var titulo = $(".itemCarrousel"+current).attr('data-titulo');
                var descricao = $(".itemCarrousel"+current).attr('data-descricao');
                var url = $(".itemCarrousel"+current).find("a").attr('href');
                var trajeto = $(".itemCarrousel"+current).find("a").attr('target');
                $("#tituloBanner").html(titulo);
                $("#descricaoBanner").html(descricao);
                $("#tituloBanner").attr("href",url);
                $("#tituloBanner").attr("target",trajeto);
            },
            afterInit: function(elem){
                var current = this.currentItem;
                var titulo = $(".itemCarrousel"+current).attr('data-titulo');
                var descricao = $(".itemCarrousel"+current).attr('data-descricao');
                var url = $(".itemCarrousel"+current).find("a").attr('href');
                var trajeto = $(".itemCarrousel"+current).find("a").attr('target');
                $("#tituloBanner").html(titulo);
                $("#descricaoBanner").html(descricao);
                $("#tituloBanner").attr("href",url);
                $("#tituloBanner").attr("target",trajeto);
            }
        });
        $("a#next").click(function (e) {
        	e.preventDefault();
            owl.trigger('owl.prev');
        });

        $("a#prev").click(function (e) {
        	e.preventDefault();
            owl.trigger('owl.next');
        });

        // Fim Carousel Prefa
	});
    $("select[name=updatePessoa]").on("change",function(e){
         var ID = $(this).val();
        if(ID){
            $(".parlamentar").slideUp(300,function(){
                $.post( baseUrl+"/index/get_pessoa",{id:ID}, function( data ) {
                  if(data.status == "sucesso"){
                    if(data.campos.tipo == "equipe"){
                        $("#nomePessoa").html(data.pessoa.post_title);
                        $("#cargoPessoa").html("Cargo: "+data.campos.cargo);
                        $("#telefonePessoa").html(data.campos.telefone);
                        $("#imagemPessoa").attr("src",data.campos.imagem.url);
                        $("#detalhesPessoa").attr("href","/colaboradores/"+data.pessoa.post_name);
                        $(".parlamentar").slideDown();
                    }else{
                        $("#nomePessoa").html(data.pessoa.post_title);
                        $("#partidoPessoa").html("Partido: "+data.campos.partido);
                        $("#gabinetePessoa").html("Gabinete n "+data.campos.gabinete);
                        $("#telefonePessoa").html("Telefone: "+data.campos.telefone);
                        $("#imagemPessoa").attr("src",data.campos.imagem.url);
                        $("#detalhesPessoa").attr("href","/colaboradores/"+data.pessoa.post_name);
                        $(".parlamentar").slideDown();
                    }
                  }else{
                    alert(data.mensagem);
                    $(".parlamentar").slideDown();
                  }
                },"json");
            });
        }
       
        
    });
})(jQuery);

function initMap($) {
      var lat = parseFloat($('#latitudeMapa').val());
      var lng = parseFloat($('#longitudeMapa').val());
      if($('#latitudeMapa').val() && $('#longitudeMapa').val()){
          var myLatLng = {lat: lat, lng: lng};
    
          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 16,
            center: myLatLng,
            scrollwheel: false,
          });
    
    
          var marker = new google.maps.Marker({
            position: myLatLng,
            animation: google.maps.Animation.DROP,
            map: map,
            icon: "/wp-content/themes/sd-theme/images/gps-mapa.png",
            title: $('#enderecoMapa').val()
          });
      }

}

