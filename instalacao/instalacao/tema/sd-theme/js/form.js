/**
 * Validação de formulário
 * Exemplo básico = <input type="text" name="nome" value="" placeholder="Nome:" required="required">
 * Exemplo com email = <input type="email" name="email" value="" placeholder="E-mail:" required="required">
 * Exemplo com Cpf = <input type="text" name="cpf" value="" placeholder="CPF:" required="required" data-is-cpf="true">
 * Exemplo com Cep = <input type="text" name="cep" value="" placeholder="Cep:" required="required" data-is-cep="true">
 * Exemplo com Tel = <input type="tel" name="telefone" value="" placeholder="Telefone:" required="required">
 */
(function( $ ){
	
    $.fn.SitioDigitalForm = function(options) {
    	var erros = [];
    	var args = [];
    	var defaults = {
          'erro_bg_color' : 'rgba(204,051,000,0.9)',
          'sucesso_bg_color' : 'rgba(0,255,127,0.9)',
          'tempo' : '3000',
          'refresh': false,
          'reset':true
        };
    	var settings = $.extend( {}, defaults, options );
    	
        return this.each (function() {
        	// remove a validacao por html5
        	settings.formulario.attr('novalidate','novalidate');
        	settings.formulario.submit(function(ev){
        		
        		  $('.alert-geral').remove();
        		  erros = [];
	      		  ev.preventDefault();
	      		  $campos = settings.formulario.find("input[required], textarea[required], select[required]").each(function(index){
	      		    	var $obj = $(this);
	      		    	
	      		    	if($obj.attr("required") == 'required' && $obj.attr("data-is-cpf") && $obj.attr("type") == 'text' && $obj.css("display") != 'none'){
	      		    		if (!$obj.isValidCpf()){
	      		    			if(!$obj.attr("data-msg")){
		      		    	    	erros.push({
		      		    	            msg: "Campo "+($obj.attr("name"))+" inválido.",
		      		    	            obj:$obj
		      		    	        });
	      		    			}else{
	      		    				erros.push({
		      		    	            msg: $obj.attr("data-msg"),
		      		    	            obj:$obj
		      		    	        });
	      		    			}
	      		        	}
	      		    	}else if($obj.attr("required") == 'required' && $obj.attr("type") == 'tel' && $obj.css("display") != 'none'){
	      		    		if (!$obj.inSizeRangeString(1)){
	      		    			if(!$obj.attr("data-msg")){
		      		    	    	erros.push({
		      		    	            msg: "Campo "+($obj.attr("name"))+" inválido.",
		      		    	            obj:$obj
		      		    	        });
	      		    			}else{
	      		    				erros.push({
		      		    	            msg: $obj.attr("data-msg"),
		      		    	            obj:$obj
		      		    	        });
	      		    			}
	      		    		}
	      		        }else if($obj.attr("required") == 'required' && $obj.attr("data-is-cep") && $obj.attr("type") == 'text' && $obj.css("display") != 'none'){
	      		    		if (!$obj.isValidCep()){
	      		    			if(!$obj.attr("data-msg")){
		      		    	    	erros.push({
		      		    	            msg: "Campo "+($obj.attr("name"))+" inválido.",
		      		    	            obj:$obj
		      		    	        });
	      		    			}else{
	      		    				erros.push({
		      		    	            msg: $obj.attr("data-msg"),
		      		    	            obj:$obj
		      		    	        });
	      		    			}
	      		        	}
	      		    	}else if($obj.attr("type") == 'email' && $obj.attr("required") == 'required' && $obj.css("display") != 'none'){
	      		    		if (!$obj.isValidEMAIL()){
	      		    			if(!$obj.attr("data-msg")){
		      		    	    	erros.push({
		      		    	            msg: "Campo "+($obj.attr("name"))+" inválido.",
		      		    	            obj:$obj
		      		    	        });
	      		    			}else{
	      		    				erros.push({
		      		    	            msg: $obj.attr("data-msg"),
		      		    	            obj:$obj
		      		    	        });
	      		    			}
	      		        	}
	      		    	}else if($obj.attr("required") == 'required' && $obj.attr("type") == 'text' && $obj.css("display") != 'none'){
	      		    		if (!$obj.inSizeRangeString(1)){
	      		    			if(!$obj.attr("data-msg")){
		      		    	    	erros.push({
		      		    	            msg: "Campo "+($obj.attr("name"))+" inválido.",
		      		    	            obj:$obj
		      		    	        });
	      		    			}else{
	      		    				erros.push({
		      		    	            msg: $obj.attr("data-msg"),
		      		    	            obj:$obj
		      		    	        });
	      		    			}
	      		        	}
	      		    	}else if($obj.attr("required") == 'required' && $obj.css("display") != 'none'){
	      		    		if (!$obj.inSizeRangeString(1)){
	      		    			if(!$obj.attr("data-msg")){
		      		    	    	erros.push({
		      		    	            msg: "Campo "+($obj.attr("name"))+" inválido.",
		      		    	            obj:$obj
		      		    	        });
	      		    			}else{
	      		    				erros.push({
		      		    	            msg: $obj.attr("data-msg"),
		      		    	            obj:$obj
		      		    	        });
	      		    			}
	      		        	}
	      		    	}
	      		    	
	      		    });

	      			var $html = '<div>';
	      			var formData = new FormData();
	      			settings.formulario.find("input, select, textarea").each(function(index){
	      				if($(this).attr("type") == "file"){
		        			if($(this).attr("name")){
		        				$tamanho = $(this)[0].files.length;
		        				for(var $i=0;$i<$tamanho;$i++){
		        					$nome = $(this).attr("name");
		        					formData.append($nome+"[]", $(this)[0].files[$i]);
		        				}
		        			}
		        		}else if($(this).attr("type") == 'radio'){
		        			if($(this).is(':checked')){
		        				formData.append($(this).attr("name"), $(this).val());
		        			}
		        		}else{
	      					formData.append($(this).attr("name"), $(this).val());
	      				}
	      				
	      			});
	      		    args = args || {};
	      		    if (erros.length > 0) {
	      		        var $borda = "";
	      		        $(erros).each(function(idx, item) {
		      	            $borda = $(item.obj).css('border-color');
		      	            $html += '					<p style="color:#fff;">'+item.msg+'<br/></p>';
		      		    	$html += '	</div>';
		      		    	$('body').prepend('<div style="position:fixed; top:0; width:100%; z-index:99999; display:none; background:'+(settings.erro_bg_color)+'; color:#fff;" class="alert alert-danger alert-geral"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+$html+'</div>');
		      		    	$(".alert-geral").slideDown(300);
		      		    	setTimeout(function(){
		      		    		$(".alert-geral").slideUp(300);
		      		    		$(item.obj).css('border-color',$borda);
		      		    	},settings.tempo);
		      		    	$(item.obj).focus();
		      		    	$(item.obj).css('border-color','red');
		      		    	return false;
		      	        });
	      		    }else{
	      		    	$valorInput = settings.formulario.find("button[type=submit]").html();
	      		        $.ajax({
	      		            url: settings.formulario.attr('action'),
	      		            data: formData,
	      		            type: settings.formulario.attr('method'),
	      		            processData: false, 
	      		            contentType: false,
	      		            dataType: "json",
	      		            beforeSend: function() {
	      		                $('input:submit, button[type=submit]').attr('disabled', 'disabled');
	      		                settings.formulario.find("button[type=submit]").html("...");
	      		              $('body').prepend('<div style="position:fixed; top:0; left:50%; margin-left:-100px; text-align:center; width:200px; z-index:99999; background:rgba(0,0,0,0.6); color:#fff;" class="alert alert-danger carregando"><img src="data:image/gif;base64,R0lGODlhHAAQAJECAJubm729vf///wAAACH/C05FVFNDQVBFMi4wAwEAAAAh/wtYTVAgRGF0YVhNUDw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpFRjBDM0NEQzFFMjA2ODExODIyQUU5OTdCRDUzRTM0RCIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpCRDY0RUQxRTM2MTQxMUUyOUVEOThCREQxMDVDRjhBMCIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpCRDY0RUQxRDM2MTQxMUUyOUVEOThCREQxMDVDRjhBMCIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M2IChNYWNpbnRvc2gpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RUYwQzNDREMxRTIwNjgxMTgyMkFFOTk3QkQ1M0UzNEQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RkQ3RjExNzQwNzIwNjgxMTgyMkFFOTk3QkQ1M0UzNEQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQJFAACACwAAAAAHAAQAAACNZSPqcvtD6NkoNo6070n+N99nrFZoXiCQokZ4ui+JJvCwhvMZZ3H6ErzqW4y4C6DTCqXTGUBACH5BAkUAAIALAAAAAAcABAAAAI1lI+py+0Po5wG2Gtf2PxgfHCdIG7elxlloJYnGrqkLKDpLLZ5Zcf7+vr4Rg0bgIJMKpfMRwEAIfkECRQAAgAsAAAAABwAEAAAAjWUj6nL7Q+jnA/Ya03Y/PBuYJj2BV55iBf5nW2oAiwolCaszptLx3KN0t0EPgqiaEwql0xjAQAh+QQJFAACACwAAAAAHAAQAAACNZSPqcvtD6OcBthrX9j8YHxwnSBu3pcZZaCWJxq6pCyg6Sy2eWXH+/r6+EYNG4CCTCqXzEcBACH5BAUUAAIALAAAAAAcABAAAAI1lI+py+0Po2Sg2jrTvSf4332esVmheIJCiRni6L4km8LCG8xlncfoSvOpbjLgLoNMKpdMZQEAOw=="/></div>');
	      		    			
	      		            },
	      		            error:function(data){
	      		            	$('.alert-geral').remove();
	      		                if(args.error != null)args.error(data.responseText);
	      		                $('body').prepend('<div style="position:fixed; top:0; width:100%; z-index:99999; display:none; background:'+(settings.erro_bg_color)+'; color:#fff;" class="alert alert-danger alert-geral"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+data.statusText+'</div>');
	      		                $('.carregando').remove();
	      		            },
	      		            complete: function(data) {
	      		                $('input:submit, button[type=submit]').removeAttr('disabled');
	      		                settings.formulario.find("button[type=submit]").html($valorInput);
	      		                if(data.responseJSON.status == "sucesso"){
	      		                	if(settings.onComplete)
	      		                		settings.onComplete(data);
	      		                	
	      		                	if(settings.reset){
	      		                		settings.formulario.trigger('reset');
	      		                	}
	      		                	
	      		                	if(settings.redirect != null && settings.redirect != ''){
	      		                		window.location.href=settings.redirect;
	      		                	}else{
	      		                		alerta(data.responseJSON.mensagem,$);
	      		                	}
	      		                	
	      		                	if(settings.refresh){
	      		                		location.reload();
	      		                	}
	      		                }else{
	      		                	alerta(data.responseJSON.mensagem,$);
	      		                }
	      		                if(settings.complete != null)settings.complete(data.responseJSON.msg);
	      		                $('.carregando').remove();
	      		            }
	      		      });
	      		   }
	      	});
        });
    }; 

    $.fn.inSizeRangeString = function(min, max) {
        var len = this.val().length;
        if (this.val() == this.attr('placeholder')) return false;
        if (min != null && max != null) {
            if (len < min || len > max)return false;
        } else if (min != null && max == null) {
            if (len < min)return false;
        } else if (min == null && max != null) {
            if (len > max)return false;
        }
        return true;
    };
    $.fn.isValidEMAIL = function() {
        var email = this.val();
        var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        return emailPattern.test(email);
    };
    $.fn.isValidCpf = function() {
    	var strCpf = this.val().replace('.', '').replace('-', '').replace('.', '');
    	var soma;
    	var resto;
    	soma = 0;
    	if (strCpf == "00000000000") {
    	    return false;
    	}

    	for (var i = 1; i <= 9; i++) {
    	    soma = soma + parseInt(strCpf.substring(i - 1, i)) * (11 - i);
    	}

    	resto = soma % 11;

    	if (resto == 10 || resto == 11 || resto < 2) {
    	    resto = 0;
    	} else {
    	    resto = 11 - resto;
    	}

    	if (resto != parseInt(strCpf.substring(9, 10))) {
    	    return false;
    	}

    	soma = 0;

    	for (var i = 1; i <= 10; i++) {
    	    soma = soma + parseInt(strCpf.substring(i - 1, i)) * (12 - i);
    	}
    	resto = soma % 11;

    	if (resto == 10 || resto == 11 || resto < 2) {
    	    resto = 0;
    	} else {
    	    resto = 11 - resto;
    	}

    	if (resto != parseInt(strCpf.substring(10, 11))) {
    	    return false;
    	}

    	return true;
    };
    $.fn.isValidCep = function() {
        var cep = this.val();
        if (/\d{2}\.\d{3}\-\d{3}/.test(cep))
            return true;
        else
            return false;
    };
    
    
 // jQuery Mask Plugin v1.14.0
 // github.com/igorescobar/jQuery-Mask-Plugin
 (function(b){"function"===typeof define&&define.amd?define(["jquery"],b):"object"===typeof exports?module.exports=b(require("jquery")):b(jQuery||Zepto)})(function(b){var y=function(a,e,d){var c={invalid:[],getCaret:function(){try{var r,b=0,e=a.get(0),d=document.selection,f=e.selectionStart;if(d&&-1===navigator.appVersion.indexOf("MSIE 10"))r=d.createRange(),r.moveStart("character",-c.val().length),b=r.text.length;else if(f||"0"===f)b=f;return b}catch(g){}},setCaret:function(r){try{if(a.is(":focus")){var c,
 b=a.get(0);b.setSelectionRange?(b.focus(),b.setSelectionRange(r,r)):(c=b.createTextRange(),c.collapse(!0),c.moveEnd("character",r),c.moveStart("character",r),c.select())}}catch(e){}},events:function(){a.on("keydown.mask",function(c){a.data("mask-keycode",c.keyCode||c.which)}).on(b.jMaskGlobals.useInput?"input.mask":"keyup.mask",c.behaviour).on("paste.mask drop.mask",function(){setTimeout(function(){a.keydown().keyup()},100)}).on("change.mask",function(){a.data("changed",!0)}).on("blur.mask",function(){n===
 c.val()||a.data("changed")||a.trigger("change");a.data("changed",!1)}).on("blur.mask",function(){n=c.val()}).on("focus.mask",function(a){!0===d.selectOnFocus&&b(a.target).select()}).on("focusout.mask",function(){d.clearIfNotMatch&&!p.test(c.val())&&c.val("")})},getRegexMask:function(){for(var a=[],c,b,d,f,l=0;l<e.length;l++)(c=g.translation[e.charAt(l)])?(b=c.pattern.toString().replace(/.{1}$|^.{1}/g,""),d=c.optional,(c=c.recursive)?(a.push(e.charAt(l)),f={digit:e.charAt(l),pattern:b}):a.push(d||
 c?b+"?":b)):a.push(e.charAt(l).replace(/[-\/\\^$*+?.()|[\]{}]/g,"\\$&"));a=a.join("");f&&(a=a.replace(new RegExp("("+f.digit+"(.*"+f.digit+")?)"),"($1)?").replace(new RegExp(f.digit,"g"),f.pattern));return new RegExp(a)},destroyEvents:function(){a.off("input keydown keyup paste drop blur focusout ".split(" ").join(".mask "))},val:function(c){var b=a.is("input")?"val":"text";if(0<arguments.length){if(a[b]()!==c)a[b](c);b=a}else b=a[b]();return b},getMCharsBeforeCount:function(a,c){for(var b=0,d=0,
 f=e.length;d<f&&d<a;d++)g.translation[e.charAt(d)]||(a=c?a+1:a,b++);return b},caretPos:function(a,b,d,h){return g.translation[e.charAt(Math.min(a-1,e.length-1))]?Math.min(a+d-b-h,d):c.caretPos(a+1,b,d,h)},behaviour:function(d){d=d||window.event;c.invalid=[];var e=a.data("mask-keycode");if(-1===b.inArray(e,g.byPassKeys)){var m=c.getCaret(),h=c.val().length,f=c.getMasked(),l=f.length,k=c.getMCharsBeforeCount(l-1)-c.getMCharsBeforeCount(h-1),n=m<h;c.val(f);n&&(8!==e&&46!==e&&(m=c.caretPos(m,h,l,k)),
 c.setCaret(m));return c.callbacks(d)}},getMasked:function(a,b){var m=[],h=void 0===b?c.val():b+"",f=0,l=e.length,k=0,n=h.length,q=1,p="push",u=-1,t,w;d.reverse?(p="unshift",q=-1,t=0,f=l-1,k=n-1,w=function(){return-1<f&&-1<k}):(t=l-1,w=function(){return f<l&&k<n});for(;w();){var x=e.charAt(f),v=h.charAt(k),s=g.translation[x];if(s)v.match(s.pattern)?(m[p](v),s.recursive&&(-1===u?u=f:f===t&&(f=u-q),t===u&&(f-=q)),f+=q):s.optional?(f+=q,k-=q):s.fallback?(m[p](s.fallback),f+=q,k-=q):c.invalid.push({p:k,
 v:v,e:s.pattern}),k+=q;else{if(!a)m[p](x);v===x&&(k+=q);f+=q}}h=e.charAt(t);l!==n+1||g.translation[h]||m.push(h);return m.join("")},callbacks:function(b){var g=c.val(),m=g!==n,h=[g,b,a,d],f=function(a,b,c){"function"===typeof d[a]&&b&&d[a].apply(this,c)};f("onChange",!0===m,h);f("onKeyPress",!0===m,h);f("onComplete",g.length===e.length,h);f("onInvalid",0<c.invalid.length,[g,b,a,c.invalid,d])}};a=b(a);var g=this,n=c.val(),p;e="function"===typeof e?e(c.val(),void 0,a,d):e;g.mask=e;g.options=d;g.remove=
 function(){var b=c.getCaret();c.destroyEvents();c.val(g.getCleanVal());c.setCaret(b-c.getMCharsBeforeCount(b));return a};g.getCleanVal=function(){return c.getMasked(!0)};g.getMaskedVal=function(a){return c.getMasked(!1,a)};g.init=function(e){e=e||!1;d=d||{};g.clearIfNotMatch=b.jMaskGlobals.clearIfNotMatch;g.byPassKeys=b.jMaskGlobals.byPassKeys;g.translation=b.extend({},b.jMaskGlobals.translation,d.translation);g=b.extend(!0,{},g,d);p=c.getRegexMask();!1===e?(d.placeholder&&a.attr("placeholder",d.placeholder),
 a.data("mask")&&a.attr("autocomplete","off"),c.destroyEvents(),c.events(),e=c.getCaret(),c.val(c.getMasked()),c.setCaret(e+c.getMCharsBeforeCount(e,!0))):(c.events(),c.val(c.getMasked()))};g.init(!a.is("input"))};b.maskWatchers={};var A=function(){var a=b(this),e={},d=a.attr("data-mask");a.attr("data-mask-reverse")&&(e.reverse=!0);a.attr("data-mask-clearifnotmatch")&&(e.clearIfNotMatch=!0);"true"===a.attr("data-mask-selectonfocus")&&(e.selectOnFocus=!0);if(z(a,d,e))return a.data("mask",new y(this,
 d,e))},z=function(a,e,d){d=d||{};var c=b(a).data("mask"),g=JSON.stringify;a=b(a).val()||b(a).text();try{return"function"===typeof e&&(e=e(a)),"object"!==typeof c||g(c.options)!==g(d)||c.mask!==e}catch(n){}};b.fn.mask=function(a,e){e=e||{};var d=this.selector,c=b.jMaskGlobals,g=c.watchInterval,c=e.watchInputs||c.watchInputs,n=function(){if(z(this,a,e))return b(this).data("mask",new y(this,a,e))};b(this).each(n);d&&""!==d&&c&&(clearInterval(b.maskWatchers[d]),b.maskWatchers[d]=setInterval(function(){b(document).find(d).each(n)},
 g));return this};b.fn.masked=function(a){return this.data("mask").getMaskedVal(a)};b.fn.unmask=function(){clearInterval(b.maskWatchers[this.selector]);delete b.maskWatchers[this.selector];return this.each(function(){var a=b(this).data("mask");a&&a.remove().removeData("mask")})};b.fn.cleanVal=function(){return this.data("mask").getCleanVal()};b.applyDataMask=function(a){a=a||b.jMaskGlobals.maskElements;(a instanceof b?a:b(a)).filter(b.jMaskGlobals.dataMaskAttr).each(A)};var p={maskElements:"input,td,span,div",
 dataMaskAttr:"*[data-mask]",dataMask:!0,watchInterval:300,watchInputs:!0,useInput:function(a){var b=document.createElement("div"),d;a="on"+a;d=a in b;d||(b.setAttribute(a,"return;"),d="function"===typeof b[a]);return d}("input"),watchDataMask:!1,byPassKeys:[9,16,17,18,36,37,38,39,40,91],translation:{0:{pattern:/\d/},9:{pattern:/\d/,optional:!0},"#":{pattern:/\d/,recursive:!0},A:{pattern:/[a-zA-Z0-9]/},S:{pattern:/[a-zA-Z]/}}};b.jMaskGlobals=b.jMaskGlobals||{};p=b.jMaskGlobals=b.extend(!0,{},p,b.jMaskGlobals);
 p.dataMask&&b.applyDataMask();setInterval(function(){b.jMaskGlobals.watchDataMask&&b.applyDataMask()},p.watchInterval)});


})( jQuery );

(function( $ ){
	$('input[data-is-cep=true]').mask('00.000-000');
	$('input[data-is-cpf=true]').mask('000.000.000-00');
	$('input[data-is-cnpj=true]').mask('00.000.000/0000-00');
	$('input[type=tel]').mask('(00) 0000-00009');
	$('input[type=tel]').blur(function(event) {
	   if($(this).val().length == 15) { // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
	      $('input[type=tel]').mask('(00) 00000-0009');
	   } else {
	      $('input[type=tel]').mask('(00) 0000-00009');
	   }
	});
})( jQuery );


function alerta($msg,$){
	$('body').prepend('<div style="position:fixed; top:0; width:100%; z-index:99999; display:none; background:rgba(0,0,0,0.8); color:#fff;" class="alert alert-danger alert-geral"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+$msg+'</div>');
	$(".alert-geral").slideDown(300);
  	setTimeout(function(){
  		$(".alert-geral").slideUp(300);
  	},3000);
}


