(function($){
	$(document).ready(function(){
        var $ano = Number($('#countdown_wrapper').attr("data-y"));
        var $mes = Number($('#countdown_wrapper').attr("data-m"))-1;
        var $dia = Number($('#countdown_wrapper').attr("data-d"));
		var newYear = new Date($ano, $mes, $dia); 
            $('#countdown_wrapper').countdown({
            until: newYear, 
            format: 'DHMS',
            layout: '<div id="t7_timer">'+
                        '<div class="countdown_numbers">'+
                            '<div id="days" class="span-n">{dnn}<\/div>'+
                            '<div id="hours" class="span-n">{hnn}<\/div>'+
                            '<div id="minutes" class="span-n">{mnn}<\/div>'+
                            '<div id="seconds" class="span-n last">{snn}<\/div>'+
                        '<\/div>'+
                        '<div class="countdown_numbers">'+
                        	'<div class="span-t">DIAS<\/div>'+
                            '<div class="span-t">HRS<\/div>'+
                            '<div class="span-t">MIN<\/div>'+
                            '<div class="span-t" last">SEG<\/div>'+
                        '<\/div>'+
                    '<\/div>'
            });
	});
})(jQuery);