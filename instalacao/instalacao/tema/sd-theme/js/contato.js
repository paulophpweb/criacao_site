(function($){
	$(document).ready(function(){
		initMap($);
		$("body").SitioDigitalForm({formulario:$('#formulario'),erro_bg_color:'rgba(0,0,0,0.8)'});
	});
})(jQuery);

function initMap($) {
	  var lat = parseFloat($('#latitudeMapa').val());
	  var lng = parseFloat($('#longitudeMapa').val());
	  if($('#latitudeMapa').val() && $('#longitudeMapa').val()){
		  var myLatLng = {lat: lat, lng: lng};
	
		  var map = new google.maps.Map(document.getElementById('map'), {
		    zoom: 16,
		    center: myLatLng,
		    scrollwheel: false,
		  });
	
		 var contentString = '<div id="content" style="width:320px;">'+
		      '<div id="siteNotice">'+
		      '</div>'+
		      '<h5 id="firstHeading" class="firstHeading">Localização</h5>'+
		      '<div id="bodyContent">'+
		      '<p> '+($('#enderecoMapa').val())+' </p>'+
		      '</div>'+
		      '</div>';
	
		  var infowindow = new google.maps.InfoWindow({
		    content: contentString
		  });
	
	
		  var marker = new google.maps.Marker({
		    position: myLatLng,
		    animation: google.maps.Animation.DROP,
		    map: map,
		    icon: "/wp-content/themes/sd-theme/images/gps-mapa.png",
		    title: $('#enderecoMapa').val()
		  });
	
		  marker.addListener('click', function() {
		    infowindow.open(map, marker);
		  });
	  }

}