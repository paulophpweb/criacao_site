(function($){
	$(document).ready(function(){
		var owl = $("#banner").owlCarousel({
            pagination: false,
            navigation: false,
            navigationText: false,
            autoPlay: false,
            transitionStyle : "fadeUp",
            rewindNav: true,
            responsive: true,
            itemsCustom: [
               [0, 1],
               [450, 1],
               [600, 1],
               [700, 1],
               [1000, 1],
               [1200, 1],
               [1400, 1],
               [1600, 1]
             ]
        });
        $("a#next").click(function (e) {
        	e.preventDefault();
            owl.trigger('owl.prev');
        });

        $("a#prev").click(function (e) {
        	e.preventDefault();
            owl.trigger('owl.next');
        });

        $(".fancybox").fancybox({
          'openEffect'    :   'elastic',
          'closeEffect'   :   'elastic',
          'nextEffect'    :   'fade',
          'openSpeed'     :   600, 
          'closeSpeed'    :   200,
          helpers : {
            title : {
              type: 'outside'
            },
            thumbs  : {
              width : 50,
              height  : 50
            }
          }
        });
	});
})(jQuery);