<?php
require_once(_DIR_PLUGIN."sdlib/theme/SDAbstractController.php");
class SDDefault extends SDAbstractController{
	/**
	 * VARIÁVEL $this->post;
	 *  -> Sempre que é executado o método [action_open] a variável $this->post é setada com os dados do post
	 *  -> neste caso, ela contem todas as colunas da tabela wp_posts selecionada. exemplo: $this->post->ID ou $this->post->post_title
	 * VARIÁVEL $this->template;
	 *  -> Como o proprio nome indica, ela é uma string contendo o nome do template
	 * VARIÁVEL $this->view;
	 *  -> Está variável é do tipo stdClass, e serve para armazenar dados para serem recuperados dentro do view
	 * VARIÁVEL $this->admin_post;
	 *  -> Contêm dados de configuração do [post_type=criar-admin] Ainda não vejo utilidade de utilizar os dados dele dentro do controller/view.
	 *  -> mas pode ser que sejá util um dia.
	 * Método $this->addCSS($part_name, $order_name)
	 * 	-> $part_name = recebe uma string com o caminho a partir da pasta CSS e não é necessário adicionar a extensão do arquivo
	 *  -> $order_name = O nome do arquivo que ele depende.
	 * Método $this->addJS($part_name, $order_name)
	 * 	-> $part_name = recebe uma string com o caminho a partir da pasta CSS e não é necessário adicionar a extensão do arquivo
	 *  -> $order_name = O nome do arquivo que ele depende.
	 * Método $this->is_ajax()
	 * -> returna um valor boneano
	 * Método $this->post_name()
	 * -> returna o nome da página/post
	 * Método $this->renderPartial($part_name, $view_name_or_folder)
	 * -> $part_name = Nome da parte a partir da pasta view
	 * -> $view_name_or_folder = Nome da página que ele está agrupado ou post.
	 * -> OBS: Este método faz alto include de JS e CSS dependente.
	 */
	public function __construct($admin_post){
		parent::__construct($admin_post);
		$this->addJS("sitio_digital_form", "jquery");
		$this->addCSS("font/stylesheet", array("bootstrap"));
		$this->addCSS("font-awesome.min", array("bootstrap"));
		$this->addCSS("share", array("bootstrap"));
		$this->addCSS("plugins/owl-carousel/owl-carousel/owl.carousel", array("bootstrap"));
		$this->addCSS("plugins/owl-carousel/owl-carousel/owl.theme", array("bootstrap"));
		$this->addCSS("plugins/owl-carousel/owl-carousel/owl.transitions", array("bootstrap"));
		$this->addJS("plugins/owl-carousel/owl-carousel/owl.carousel", array("jquery")); 

		$taxonomias = get_field("create_tax", 4);
		if(count($taxonomias)){
		    global $wpdb;
		    $aSlug = array();
		    foreach ($taxonomias as $taxonomia){
		        foreach ($taxonomia['receber_taxonimias'] as $posts){
		            $slug = $wpdb->get_var("
                SELECT
                    wp_posts.post_name
                FROM
                    wp_posts
                    LEFT JOIN wp_postmeta ON wp_posts.ID = wp_postmeta.post_id AND wp_postmeta.meta_key = 'tipo_de_post'
                WHERE wp_posts.ID = ".$posts."");
		            $aSlug[] = $slug;
		        }
		        $nome_tax = StringUtils::clear($taxonomia['nome_tax']);
		        register_taxonomy($nome_tax,
		        $aSlug,
		        array(
		        "label" => $taxonomia['nome_tax'],
		        "singular_label" => $taxonomia['nome_tax'],
		        "rewrite" => true,
		        "hierarchical" => true,
		        "show_admin_column" => $taxonomia['admin_column'],
		        "show_ui" => $taxonomia['type_input'],
		        "show_tagcloud" => $taxonomia['add_tags'],
		        "rewrite" => array("slug" => $nome_tax, "hierarchical" => true)
		        )
		        );
		
		    }
		}
		
	} 

	public function action_custom_css(){
	   header("Content-type: text/css; charset: UTF-8");

	  $configurar = get_page_by_path("configuracoes");
	  $campos = get_fields($configurar->ID);

	   echo "
			.bg-topo{background:".$campos["background_topo"].";}
			.topo .azul-claro, .topo .branco{color:".$campos["cor_do_texto_topo"].";}
			header .logo-header a {background-image:url(".$campos["logo_colorida"]["url"]."); background-repeat: no-repeat; background-position:center center;}
			header ul.redes-sociais li a {color:".$campos["cor_do_texto_topo"]."}
			.bg-azul{background:".$campos["background_topo"]."; color:".$campos["cor_do_texto_topo"].";}
			#buscar button{background:".$campos["cor_do_texto_topo"]."; color:".$campos["background_topo"].";}
			#buscar button{background-image:none;}
			#buscar button:hover{background:".$campos["background_hover"]."; color:".$campos["texto_hover"].";}
			.acessibilidade a i{font-size:22px;}
			.menu .bg-verde{background-color:".$campos["background_topo"]." !important;}
			.menu .multilevel{background-color:".$campos["background_topo"]." !important;}
			.menu .multilevel:hover{background-color:".$campos["background_topo"]." !important;}
			.sub-menu{background:".$campos["background_topo"].";}
			header ul.redes-sociais li a:hover{color:".$campos["texto_hover"].";}
			.acessibilidade a:hover{color:".$campos["texto_hover"].";}
			.menu li a:hover{color:".$campos["texto_hover"]." !important; background-color:".$campos["background_hover"]." !important;}
			footer .rodape .logo-footer figure a{background-size: 188px;background-image:url(".$campos["logo_colorida"]["url"]."); -webkit-filter: grayscale(100%); filter: grayscale(100%); background-repeat: no-repeat; background-position:center center;}
	   ";
	}


	public function action_get_pessoa(){
	   header("Content-type: text/css; charset: UTF-8");
	   $pessoa = get_post($_REQUEST["id"]);
       if($pessoa){
       	$campos = get_fields($pessoa->ID);
	   	  echo json_encode(array("status" => "sucesso", "mensagem" => "Resultado encontrado.","pessoa" => $pessoa,"campos" => $campos));
	   }else{
	   	  echo json_encode(array("status" => "erro", "mensagem" => "Nenhum resultado encontrado."));
	   }
	   exit;

	  
	}

}