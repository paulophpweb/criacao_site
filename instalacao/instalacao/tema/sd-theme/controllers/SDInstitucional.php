<?php
require_once(_DIR_CONTROLLERS.'SDDefault.php');
class SDInstitucional extends SDDefault{
	/**
	 * VARIÁVEL $this->post;
	 *  -> Sempre que é executado o método [action_open] a variável $this->post é setada com os dados do post
	 *  -> neste caso, ela contem todas as colunas da tabela wp_posts selecionada. exemplo: $this->post->ID ou $this->post->post_title
	 * VARIÁVEL $this->template;
	 *  -> Como o proprio nome indica, ela é uma string contendo o nome do template
	 * VARIÁVEL $this->view;
	 *  -> Está variável é do tipo stdClass, e serve para armazenar dados para serem recuperados dentro do view
	 * VARIÁVEL $this->admin_post;
	 *  -> Contêm dados de configuração do [post_type=criar-admin] Ainda não vejo utilidade de utilizar os dados dele dentro do controller/view.
	 *  -> mas pode ser que sejá util um dia.
	 * Método $this->addCSS($part_name, $order_name)
	 * 	-> $part_name = recebe uma string com o caminho a partir da pasta CSS e não é necessário adicionar a extensão do arquivo
	 *  -> $order_name = O nome do arquivo que ele depende.
	 * Método $this->addJS($part_name, $order_name)
	 * 	-> $part_name = recebe uma string com o caminho a partir da pasta CSS e não é necessário adicionar a extensão do arquivo
	 *  -> $order_name = O nome do arquivo que ele depende.
	 * Método $this->is_ajax()
	 * -> returna um valor boneano
	 * Método $this->post_name()
	 * -> returna o nome da página/post
	 * Método $this->renderPartial($part_name, $view_name_or_folder)
	 * -> $part_name = Nome da parte a partir da pasta view
	 * -> $view_name_or_folder = Nome da página que ele está agrupado ou post.
	 * -> OBS: Este método faz alto include de JS e CSS dependente.
	 */
	public function __construct($admin_post){
		parent::__construct($admin_post);
	}
	public function action_index(){
		parent::renderHeader();
		parent::renderView();
		parent::renderFooter();
	}
	public function action_open(){
		parent::renderHeader();
		parent::renderView();
		parent::renderFooter();
	}
}