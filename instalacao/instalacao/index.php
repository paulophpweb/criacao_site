<?php 
  $erro = "";
  $sucesso = "";
  function isEnabled($func) {
      return is_callable($func) && false === stripos(ini_get('disable_functions'), $func);
  }
  if (!isEnabled('shell_exec')) {
      echo "A função shell_exec não esta habilitada, por favor habilite.";
      exit;
  }
	if(isset($_REQUEST["host"]) && $_REQUEST["host"]){
    $conn = new mysqli($_REQUEST["host"], $_REQUEST["user"], $_REQUEST["password"], $_REQUEST["dbname"]);
		if(mysqli_connect_errno()){
		  $erro = 'Erro ao conectar ' . mysqli_connect_error();
		}else{
			$selectDb = true;
			if(!$selectDb){
				$erro = 'Erro ao selecionar o banco de dados ' . $_REQUEST["dbname"]." ".mysql_error();
			}else{
				$zip = new ZipArchive;
				// extrai o arquivo
				if ($zip->open('arquivos/wordpress.zip') === TRUE) {
					$zip->extractTo('../');
    				$zip->close();
            // coloca as imagens como 777
            shell_exec("chmod -R 777 upload_imagens");
    				// copia os dados para a raiz
    				shell_exec("cp -r ../wordpress/. ../");
    				// deleta o lixo
    				delTree("../wordpress");
    				// deleta os themas desnecessarios
    				shell_exec("rm -r ../wp-content/themes/twentyfifteen && rm -r ../wp-content/themes/twentyseventeen && rm -r ../wp-content/themes/twentysixteen");
    				// deleta os plugins desnecessarios
    				shell_exec("rm -r ../wp-content/plugins/akismet && rm -r ../wp-content/plugins/hello.php");
    				// copia o tema
    				shell_exec("cp -r tema/sd-theme ../wp-content/themes");
    				// copia os plugins
    				shell_exec("cp -r plugins/. ../wp-content/plugins");
            // copia a pasta de imagens
            shell_exec("cp -r upload_imagens/. ../wp-content/uploads");
    				// renomeia o config php
    				rename("../wp-config-sample.php", "../wp-config.php");
            // copia o htacess
            shell_exec("cp arquivos/.htaccess ../");
            // coloca as imagens como 777
            shell_exec("chmod -R 777 ../wp-content/uploads");
    				$configFile = '../wp-config.php';
    				$file_contents = file_get_contents($configFile);
    				$fh = fopen($configFile, 'w');
    				$file_contents = str_replace('nome_do_banco_de_dados_aqui',$_REQUEST["dbname"],$file_contents);
    				$file_contents = str_replace('nome_de_usuario_aqui',$_REQUEST["user"],$file_contents);
    				$file_contents = str_replace('senha_aqui',$_REQUEST["password"],$file_contents);
    				$file_contents = str_replace('localhost',$_REQUEST["host"],$file_contents);
    				fwrite($fh, $file_contents);
					  fclose($fh);
  					// Coloca a pasta para escrever
      				//system("chmod -R 777 ../*");
  					// importa o banco de dados
            $stringPassword = '';
            if($_REQUEST["password"])
              $stringPassword = '--password='.$_REQUEST["password"];

            $sqlSource = file_get_contents('sql/site.sql');
            if(mysqli_multi_query($conn,$sqlSource)){
               do {
                  /* store first result set */
                  if ($result = mysqli_store_result($conn)) {
                      while ($row = mysqli_fetch_row($result)) {
                          printf("%s\n", $row[0]);
                      }
                      mysqli_free_result($result);
                  }
                  /* print divider */
                  if (mysqli_more_results($conn)) {}
              } while (mysqli_next_result($conn));

              if($ret = mysqli_query($conn,"SELECT option_value FROM wp_options WHERE option_name = 'siteurl'; ")){
                $row = mysqli_fetch_assoc($ret);
                if(isset($row['option_value'])){
                  $urlReplace = $row['option_value'];
                  $url = "http://$_SERVER[HTTP_HOST]";
                  $ret = "UPDATE wp_options SET option_value = replace(option_value, '".$urlReplace."', '".$url."') WHERE option_name = 'home' OR option_name = 'siteurl';";
                  $ret .= "UPDATE wp_posts SET guid = replace(guid, '".$urlReplace."','".$url."');";
                  $ret .= "UPDATE wp_posts SET post_content = replace(post_content, '".$urlReplace."', '".$url."');";
                  $ret .= "UPDATE wp_postmeta SET meta_value = replace(meta_value, '".$urlReplace."', '".$url."');";
                  if(mysqli_multi_query($conn,$ret)){
                      do {
                        /* store first result set */
                        if ($result = mysqli_store_result($conn)) {
                            while ($row = mysqli_fetch_row($result)) {
                                printf("%s\n", $row[0]);
                            }
                            mysqli_free_result($result);
                        }
                        /* print divider */
                        if (mysqli_more_results($conn)) {}
                    } while (mysqli_next_result($conn));
                    $sucesso = "Importado com sucesso!";
                  }else{
                    $erro = 'Erro ao executar a query '.$conn->error;
                  }
                  
                }else{
                  $erro = 'Um erro aconteceu a atualizar a url, contate o administrador.';
                }
              }else{
                $erro = 'Erro ao executar a query SELECT option_value FROM wp_options WHERE option_name = siteurl; '.$conn->error;
              }
              
            }else{
              $erro = 'Um erro aconteceu, erro ao executar o arquivo sql';
            }
				}else{
					$erro = 'Um erro aconteceu, contate o administrador';
				}
			}
		}
	}
	function delTree($dir) { 
   $files = array_diff(scandir($dir), array('.','..')); 
    foreach ($files as $file) { 
      (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file"); 
    } 
    return rmdir($dir); 
  } 
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<!-- Chrome, Firefox OS and Opera -->
	<meta name="theme-color" content="#a6ce39">
	<!-- Windows Phone -->
	<meta name="msapplication-navbutton-color" content="#a6ce39">
	<!-- iOS Safari -->
	<meta name="apple-mobile-web-app-status-bar-style" content="#a6ce39">

    <link rel='stylesheet'  href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' type='text/css' media='all' />

</head>
<body>
<div class="container">
<div class="alert alert-warning">
  <strong>Alerta!</strong> Verifique se a pasta raiz esta com a permissão 777, obs: permissão 777 somente na pasta raiz, não incluir subpastas, APÓS A INSTALAÇÃO REMOVER A PERMISSÃO 777 DA PASTA RAIZ E TAMBÉM REMOVER A PASTA DE INSTALAÇÃO, tenha certeza que o banco de dados esta vazio.
</div>
<?php if($sucesso): ?>
<div class="alert alert-success">
  <strong>Sucesso!</strong> <?php echo $sucesso; ?> <a href="<?php echo "http://$_SERVER[HTTP_HOST]"; ?>">Ir para o site</a>
</div>
<?php elseif($erro): ?>
<div class="alert alert-warning">
  <strong>Sucesso!</strong> <?php echo $erro; ?>
</div>
<?php endif; ?>
<div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-title">
            <i class="glyphicon glyphicon-wrench pull-right"></i>
            <h4>Instalação</h4>
          </div>
        </div>
        <div class="panel-body">
          
          <form action="index.php" class="form form-vertical" method="post">
            <div class="control-group">
              <label>Host Banco de dados</label>
              <div class="controls">
                <input type="text" class="form-control" name="host" placeholder="Ex: localhost">
              </div>
            </div>      
            <div class="control-group">
              <label>Nome do Banco de dados</label>
              <div class="controls">
                <input type="text" class="form-control" name="dbname" placeholder="Ex: prefeitura_beta">
              </div>
            </div>   
            <div class="control-group">
              <label>Usuário</label>
              <div class="controls">
                <input type="text" class="form-control" name="user" placeholder="Ex: root">
              </div>
            </div> 
            <div class="control-group">
              <label>Senha</label>
              <div class="controls">
                <input type="password" class="form-control" name="password" placeholder="Ex: Senha">
              </div>
            </div>   
            <div class="control-group">
              <label></label>
              <div class="controls">
                <button type="submit" class="btn btn-primary">
                  Instalar
                </button>
              </div>
            </div>   
            
          </form>
        </div><!--/panel content-->
      </div><!--/panel-->
      </div>
</body>
</html>

