-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 12, 2016 at 01:06 PM
-- Server version: 5.6.26
-- PHP Version: 5.5.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `remap`
--

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE IF NOT EXISTS `banner` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `imagem` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `titulo`, `url`, `imagem`, `status`) VALUES
(1, 'Banner 1', '', 'arq_banner/banner_1457783562.jpg', 1),
(2, 'Banner2', '', 'arq_banner/banner_1457783580.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `imagem` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cliente`
--

INSERT INTO `cliente` (`id`, `nome`, `url`, `imagem`, `status`) VALUES
(1, 'Arroz Cristal', '', 'arq_cliente/cliente_1424216639.jpg', 1),
(2, 'Arroz Tio Jorge', '', 'arq_cliente/cliente_1424216661.gif', 1),
(3, 'Cargill', '', 'arq_cliente/cliente_1424216689.gif', 1),
(4, 'Granol', '', 'arq_cliente/cliente_1424216713.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `configuracao`
--

CREATE TABLE IF NOT EXISTS `configuracao` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `tipo` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `configuracao`
--

INSERT INTO `configuracao` (`id`, `nome`, `email`, `tipo`, `status`) VALUES
(3, 'Cristiano', 'cristiano@remapmaquinas.com.br', 'Fale Conosco', 1),
(4, 'leonardo', 'leonardoremap@bol.com.br', 'Fale Conosco', 1),
(5, 'Paulo', 'paulophpweb@gmail.com', 'Orcamento', 1);

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `equipe`
--

CREATE TABLE IF NOT EXISTS `equipe` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `imagem` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `cargo` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `equipe`
--

INSERT INTO `equipe` (`id`, `nome`, `imagem`, `email`, `cargo`, `status`) VALUES
(1, 'Cristiano Pires', 'arq_equipe/equipe_1424806731.jpg', 'cristiano@remapmaquinas.com.br', 'Diretor', 1),
(2, 'Leonardo Ribeiro', 'arq_equipe/equipe_1424184379.jpg', 'leonardoremap@bol.com.br', 'Vendedor', 1),
(5, 'Cleonilson Oliveira', 'arq_equipe/equipe_1424806365.jpg', 'nilson@remapmaquinas.com.br', 'Comprador/Contas a Receber', 1),
(6, 'Washington Luis Pereira Lima   -  LIMA', 'arq_equipe/equipe_1425411818.jpg', 'lima.remapmaquinas@uol.com.br', 'vendedor', 1);

-- --------------------------------------------------------

--
-- Table structure for table `fornecedor`
--

CREATE TABLE IF NOT EXISTS `fornecedor` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `imagem` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fornecedor`
--

INSERT INTO `fornecedor` (`id`, `nome`, `url`, `imagem`, `status`) VALUES
(7, 'KEPLER WEBER - PANAMBI - RS', '', 'arq_fornecedor/fornecedor_1424217757.jpeg', 1),
(9, 'MAX BELT - LONDRINA - PR', '', 'arq_fornecedor/fornecedor_1424217963.jpg', 1),
(11, 'MAQUINAS SUZUKI - SANTA CRUZ DO RIO PARDO - SP', '', 'arq_fornecedor/fornecedor_1424218042.jpg', 1),
(14, 'MAQUINAS NOVO HORIZONTE - LIMEIRA - SP', '', '', 1),
(16, 'COMAG - PANAMBI - RS', '', 'arq_fornecedor/fornecedor_1424218301.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(11) NOT NULL,
  `usuario` varchar(100) DEFAULT NULL,
  `area` varchar(50) DEFAULT NULL,
  `id_area` int(11) DEFAULT NULL,
  `acao` varchar(50) DEFAULT NULL,
  `datahora` datetime DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=443 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `usuario`, `area`, `id_area`, `acao`, `datahora`, `ip`) VALUES
(192, 'Administrador', 'usuários', 1, 'saiu', '2014-08-28 08:42:23', '192.168.7.36'),
(193, 'Paulo', 'usuários', 2, 'entrou', '2014-08-28 08:47:01', '192.168.7.36'),
(194, 'Paulo', 'usuários', 2, 'alterou', '2014-08-28 08:55:56', '192.168.7.36'),
(195, 'Paulo', 'usuários', 2, 'alterou', '2014-08-28 09:20:27', '192.168.7.36'),
(196, 'Administrador', 'usuários', 1, 'entrou', '2015-02-10 16:11:31', '127.0.0.1'),
(197, 'Administrador', 'Módulo', 1, 'Cadastrou', '2015-02-10 16:12:24', '127.0.0.1'),
(198, 'Administrador', 'Módulo', 1, 'Cadastrou', '2015-02-10 16:13:40', '127.0.0.1'),
(199, 'Administrador', 'Módulo', 1, 'Cadastrou', '2015-02-10 16:14:57', '127.0.0.1'),
(200, 'Administrador', 'usuários', 1, 'alterou', '2015-02-10 16:15:12', '127.0.0.1'),
(201, 'Administrador', 'usuários', 1, 'entrou', '2015-02-16 15:03:45', '127.0.0.1'),
(202, 'Administrador', 'Banner', 1, 'Cadastrou', '2015-02-16 15:09:15', '127.0.0.1'),
(203, 'Administrador', 'Banner', 1, 'Publicou', '2015-02-16 15:09:19', '127.0.0.1'),
(204, 'Administrador', 'Banner', 1, 'Alterou', '2015-02-16 15:15:18', '127.0.0.1'),
(205, 'Administrador', 'usuários', 1, 'entrou', '2015-02-17 11:56:18', '127.0.0.1'),
(206, 'Administrador', 'Equipe', 1, 'Cadastrou', '2015-02-17 12:09:33', '127.0.0.1'),
(207, 'Administrador', 'Equipe', 1, 'Publicou', '2015-02-17 12:09:36', '127.0.0.1'),
(208, 'Administrador', 'Equipe', 1, 'Alterou', '2015-02-17 12:10:38', '127.0.0.1'),
(209, 'Administrador', 'Equipe', 1, 'Alterou', '2015-02-17 12:10:43', '127.0.0.1'),
(210, 'Administrador', 'Equipe', 1, 'Alterou', '2015-02-17 12:36:59', '127.0.0.1'),
(211, 'Administrador', 'Equipe', 1, 'Cadastrou', '2015-02-17 12:37:51', '127.0.0.1'),
(212, 'Administrador', 'Equipe', 1, 'Publicou', '2015-02-17 12:37:59', '127.0.0.1'),
(213, 'Administrador', 'Equipe', 1, 'Alterou', '2015-02-17 12:43:52', '127.0.0.1'),
(214, 'Administrador', 'Equipe', 1, 'Alterou', '2015-02-17 12:46:19', '127.0.0.1'),
(215, 'Administrador', 'Equipe', 1, 'Alterou', '2015-02-17 12:46:53', '127.0.0.1'),
(216, 'Administrador', 'Equipe', 1, 'Alterou', '2015-02-17 12:57:09', '127.0.0.1'),
(217, 'Administrador', 'usuários', 1, 'entrou', '2015-02-17 14:36:57', '127.0.0.1'),
(218, 'Administrador', 'Módulo', 1, 'Cadastrou', '2015-02-17 14:38:45', '127.0.0.1'),
(219, 'Administrador', 'usuários', 1, 'alterou', '2015-02-17 14:39:09', '127.0.0.1'),
(220, 'Administrador', 'Módulo', 1, 'Cadastrou', '2015-02-17 14:41:03', '127.0.0.1'),
(221, 'Administrador', 'usuários', 1, 'alterou', '2015-02-17 14:41:30', '127.0.0.1'),
(222, 'Administrador', 'Categoria', 1, 'Cadastrou', '2015-02-17 14:43:10', '127.0.0.1'),
(223, 'Administrador', 'Categoria', 1, 'Alterou', '2015-02-17 14:43:16', '127.0.0.1'),
(224, 'Administrador', 'Categoria', 1, 'Deletou', '2015-02-17 14:43:21', '127.0.0.1'),
(225, 'Administrador', 'Categoria', 1, 'Cadastrou', '2015-02-17 14:43:29', '127.0.0.1'),
(226, 'Administrador', 'Categoria', 1, 'Cadastrou', '2015-02-17 14:43:30', '127.0.0.1'),
(227, 'Administrador', 'Categoria', 1, 'Deletou', '2015-02-17 14:43:34', '127.0.0.1'),
(228, 'Administrador', 'Categoria', 1, 'Publicou', '2015-02-17 14:46:38', '127.0.0.1'),
(229, 'Administrador', 'Categoria', 1, 'Publicou', '2015-02-17 14:46:49', '127.0.0.1'),
(230, 'Administrador', 'Produto', 1, 'Cadastrou', '2015-02-17 14:49:11', '127.0.0.1'),
(231, 'Administrador', 'Produto', 1, 'Cadastrou', '2015-02-17 14:51:52', '127.0.0.1'),
(232, 'Administrador', 'foto do produto', 7, 'deletou foto', '2015-02-17 14:55:58', '127.0.0.1'),
(233, 'Administrador', 'Produto', 1, 'Alterou', '2015-02-17 14:56:02', '127.0.0.1'),
(234, 'Administrador', 'Produto', 1, 'Deletou', '2015-02-17 14:56:05', '127.0.0.1'),
(235, 'Administrador', 'Produto', 1, 'Alterou', '2015-02-17 14:56:21', '127.0.0.1'),
(236, 'Administrador', 'Equipe', 1, 'Cadastrou', '2015-02-17 15:04:23', '127.0.0.1'),
(237, 'Administrador', 'Equipe', 1, 'Publicou', '2015-02-17 15:04:26', '127.0.0.1'),
(238, 'Administrador', 'Equipe', 1, 'Cadastrou', '2015-02-17 15:04:39', '127.0.0.1'),
(239, 'Administrador', 'Equipe', 1, 'Publicou', '2015-02-17 15:04:59', '127.0.0.1'),
(240, 'Administrador', 'Equipe', 1, 'Deletou', '2015-02-17 15:06:45', '127.0.0.1'),
(241, 'Administrador', 'Equipe', 1, 'Deletou', '2015-02-17 15:06:48', '127.0.0.1'),
(242, 'Administrador', 'Categoria', 1, 'Alterou', '2015-02-17 15:31:24', '127.0.0.1'),
(243, 'Administrador', 'Categoria', 1, 'Alterou', '2015-02-17 15:32:28', '127.0.0.1'),
(244, 'Administrador', 'Categoria', 1, 'Cadastrou', '2015-02-17 15:32:52', '127.0.0.1'),
(245, 'Administrador', 'Produto', 1, 'Alterou', '2015-02-17 15:50:23', '127.0.0.1'),
(246, 'Administrador', 'Produto', 1, 'Cadastrou', '2015-02-17 15:52:37', '127.0.0.1'),
(247, 'Administrador', 'Produto', 1, 'Alterou', '2015-02-17 16:00:07', '127.0.0.1'),
(248, 'Administrador', 'Produto', 1, 'Alterou', '2015-02-17 16:04:37', '127.0.0.1'),
(249, 'Administrador', 'Produto', 1, 'Alterou', '2015-02-17 16:05:22', '127.0.0.1'),
(250, 'Administrador', 'usuários', 1, 'entrou', '2015-02-17 18:34:14', '127.0.0.1'),
(251, 'Administrador', 'foto do produto', 9, 'deletou foto', '2015-02-17 18:34:25', '127.0.0.1'),
(252, 'Administrador', 'Produto', 1, 'Alterou', '2015-02-17 18:34:38', '127.0.0.1'),
(253, 'Administrador', 'Produto', 1, 'Alterou', '2015-02-17 18:41:46', '127.0.0.1'),
(254, 'Administrador', 'Produto', 1, 'Alterou', '2015-02-17 18:43:40', '127.0.0.1'),
(255, 'Administrador', 'Produto', 1, 'Alterou', '2015-02-17 18:44:18', '127.0.0.1'),
(256, 'Administrador', 'Produto', 1, 'Alterou', '2015-02-17 18:46:00', '127.0.0.1'),
(257, 'Administrador', 'usuários', 1, 'entrou', '2015-02-17 21:08:51', '127.0.0.1'),
(258, 'Administrador', 'Módulo', 1, 'Cadastrou', '2015-02-17 21:09:39', '127.0.0.1'),
(259, 'Administrador', 'usuários', 1, 'alterou', '2015-02-17 21:09:56', '127.0.0.1'),
(260, 'Administrador', 'Cliente', 1, 'Cadastrou', '2015-02-17 21:10:16', '127.0.0.1'),
(261, 'Administrador', 'Cliente', 1, 'Deletou', '2015-02-17 21:10:25', '127.0.0.1'),
(262, 'Administrador', 'Cliente', 1, 'Publicou', '2015-02-17 21:10:28', '127.0.0.1'),
(263, 'Administrador', 'Cliente', 1, 'Alterou', '2015-02-17 21:10:55', '127.0.0.1'),
(264, 'Administrador', 'Cliente', 1, 'Alterou', '2015-02-17 21:16:24', '127.0.0.1'),
(265, 'Administrador', 'Cliente', 1, 'Alterou', '2015-02-17 21:16:40', '127.0.0.1'),
(266, 'Administrador', 'Cliente', 1, 'Cadastrou', '2015-02-17 21:19:01', '127.0.0.1'),
(267, 'Administrador', 'Cliente', 1, 'Publicou', '2015-02-17 21:19:06', '127.0.0.1'),
(268, 'Administrador', 'Cliente', 1, 'Alterou', '2015-02-17 21:19:31', '127.0.0.1'),
(269, 'Administrador', 'Cliente', 1, 'Cadastrou', '2015-02-17 21:20:34', '127.0.0.1'),
(270, 'Administrador', 'Cliente', 1, 'Publicou', '2015-02-17 21:20:40', '127.0.0.1'),
(271, 'Administrador', 'Cliente', 1, 'Cadastrou', '2015-02-17 21:21:45', '127.0.0.1'),
(272, 'Administrador', 'Cliente', 1, 'Publicou', '2015-02-17 21:21:49', '127.0.0.1'),
(273, 'Administrador', 'Cliente', 1, 'Cadastrou', '2015-02-17 21:23:21', '127.0.0.1'),
(274, 'Administrador', 'Cliente', 1, 'Publicou', '2015-02-17 21:23:26', '127.0.0.1'),
(275, 'Administrador', 'Módulo', 1, 'Cadastrou', '2015-02-17 21:31:46', '127.0.0.1'),
(276, 'Administrador', 'usuários', 1, 'alterou', '2015-02-17 21:32:03', '127.0.0.1'),
(277, 'Administrador', 'Fornecedor', 1, 'Cadastrou', '2015-02-17 21:32:58', '127.0.0.1'),
(278, 'Administrador', 'Fornecedor', 1, 'Publicou', '2015-02-17 21:33:01', '127.0.0.1'),
(279, 'Administrador', 'Fornecedor', 1, 'Alterou', '2015-02-17 21:33:17', '127.0.0.1'),
(280, 'Administrador', 'Fornecedor', 1, 'Cadastrou', '2015-02-17 21:33:50', '127.0.0.1'),
(281, 'Administrador', 'Fornecedor', 1, 'Cadastrou', '2015-02-17 21:34:08', '127.0.0.1'),
(282, 'Administrador', 'Fornecedor', 1, 'Cadastrou', '2015-02-17 21:34:36', '127.0.0.1'),
(283, 'Administrador', 'Fornecedor', 1, 'Cadastrou', '2015-02-17 21:34:59', '127.0.0.1'),
(284, 'Administrador', 'Fornecedor', 1, 'Publicou', '2015-02-17 21:37:29', '127.0.0.1'),
(285, 'Administrador', 'Fornecedor', 1, 'Publicou', '2015-02-17 21:37:31', '127.0.0.1'),
(286, 'Administrador', 'Fornecedor', 1, 'Publicou', '2015-02-17 21:37:33', '127.0.0.1'),
(287, 'Administrador', 'Fornecedor', 1, 'Publicou', '2015-02-17 21:37:34', '127.0.0.1'),
(288, 'Administrador', 'Fornecedor', 1, 'Cadastrou', '2015-02-17 21:38:13', '127.0.0.1'),
(289, 'Administrador', 'Fornecedor', 1, 'Cadastrou', '2015-02-17 21:38:34', '127.0.0.1'),
(290, 'Administrador', 'Fornecedor', 1, 'Cadastrou', '2015-02-17 21:38:53', '127.0.0.1'),
(291, 'Administrador', 'Fornecedor', 1, 'Cadastrou', '2015-02-17 21:39:09', '127.0.0.1'),
(292, 'Administrador', 'Fornecedor', 1, 'Cadastrou', '2015-02-17 21:39:26', '127.0.0.1'),
(293, 'Administrador', 'Fornecedor', 1, 'Cadastrou', '2015-02-17 21:39:49', '127.0.0.1'),
(294, 'Administrador', 'Fornecedor', 1, 'Publicou', '2015-02-17 21:39:54', '127.0.0.1'),
(295, 'Administrador', 'Fornecedor', 1, 'Publicou', '2015-02-17 21:39:56', '127.0.0.1'),
(296, 'Administrador', 'Fornecedor', 1, 'Publicou', '2015-02-17 21:40:00', '127.0.0.1'),
(297, 'Administrador', 'Fornecedor', 1, 'Publicou', '2015-02-17 21:40:03', '127.0.0.1'),
(298, 'Administrador', 'Fornecedor', 1, 'Publicou', '2015-02-17 21:40:06', '127.0.0.1'),
(299, 'Administrador', 'Fornecedor', 1, 'Publicou', '2015-02-17 21:40:08', '127.0.0.1'),
(300, 'Administrador', 'Banner', 1, 'Cadastrou', '2015-02-17 21:43:28', '127.0.0.1'),
(301, 'Administrador', 'Banner', 1, 'Publicou', '2015-02-17 21:43:31', '127.0.0.1'),
(302, 'Administrador', 'Cliente', 1, 'Cadastrou', '2015-02-17 21:43:59', '127.0.0.1'),
(303, 'Administrador', 'Cliente', 1, 'Publicou', '2015-02-17 21:44:01', '127.0.0.1'),
(304, 'Administrador', 'Cliente', 1, 'Cadastrou', '2015-02-17 21:44:21', '127.0.0.1'),
(305, 'Administrador', 'Cliente', 1, 'Cadastrou', '2015-02-17 21:44:49', '127.0.0.1'),
(306, 'Administrador', 'Cliente', 1, 'Cadastrou', '2015-02-17 21:45:13', '127.0.0.1'),
(307, 'Administrador', 'Cliente', 1, 'Publicou', '2015-02-17 21:45:17', '127.0.0.1'),
(308, 'Administrador', 'Cliente', 1, 'Publicou', '2015-02-17 21:45:19', '127.0.0.1'),
(309, 'Administrador', 'Cliente', 1, 'Publicou', '2015-02-17 21:45:21', '127.0.0.1'),
(310, 'Administrador', 'Fornecedor', 1, 'Alterou', '2015-02-17 22:02:37', '127.0.0.1'),
(311, 'Administrador', 'Fornecedor', 1, 'Alterou', '2015-02-17 22:04:57', '127.0.0.1'),
(312, 'Administrador', 'Fornecedor', 1, 'Alterou', '2015-02-17 22:06:03', '127.0.0.1'),
(313, 'Administrador', 'Fornecedor', 1, 'Alterou', '2015-02-17 22:06:42', '127.0.0.1'),
(314, 'Administrador', 'Fornecedor', 1, 'Alterou', '2015-02-17 22:07:22', '127.0.0.1'),
(315, 'Administrador', 'Fornecedor', 1, 'Alterou', '2015-02-17 22:08:22', '127.0.0.1'),
(316, 'Administrador', 'Fornecedor', 1, 'Alterou', '2015-02-17 22:09:18', '127.0.0.1'),
(317, 'Administrador', 'Fornecedor', 1, 'Alterou', '2015-02-17 22:10:35', '127.0.0.1'),
(318, 'Administrador', 'Fornecedor', 1, 'Alterou', '2015-02-17 22:11:41', '127.0.0.1'),
(319, 'Administrador', 'Fornecedor', 1, 'Alterou', '2015-02-17 22:12:29', '127.0.0.1'),
(320, 'Administrador', 'Equipe', 1, 'Cadastrou', '2015-02-17 22:16:42', '127.0.0.1'),
(321, 'Administrador', 'Equipe', 1, 'Publicou', '2015-02-17 22:16:45', '127.0.0.1'),
(322, 'Administrador', 'Equipe', 1, 'Cadastrou', '2015-02-17 22:28:32', '127.0.0.1'),
(323, 'Administrador', 'Equipe', 1, 'Publicou', '2015-02-17 22:28:41', '127.0.0.1'),
(324, 'Administrador', 'usuários', 1, 'entrou', '2015-02-18 12:37:07', '189.123.191.172'),
(325, 'Administrador', 'Produto', 1, 'Deletou', '2015-02-18 12:37:27', '189.123.191.172'),
(326, 'Administrador', 'usuários', 1, 'entrou', '2015-02-18 16:38:22', '189.123.191.172'),
(327, 'Administrador', 'usuários', 2, 'deletou', '2015-02-18 16:39:38', '189.123.191.172'),
(328, 'Administrador', 'usuários', 1, 'saiu', '2015-02-18 16:40:38', '189.123.191.172'),
(329, 'Cristiano', 'usuários', 3, 'entrou', '2015-02-18 16:40:47', '189.123.191.172'),
(330, 'Cristiano', 'usuários', 3, 'saiu', '2015-02-18 16:40:52', '189.123.191.172'),
(331, 'Administrador', 'usuários', 1, 'entrou', '2015-02-18 16:45:15', '189.123.191.172'),
(332, 'Administrador', 'usuários', 1, 'alterou senha', '2015-02-18 16:45:32', '189.123.191.172'),
(333, 'Administrador', 'usuários', 1, 'alterou', '2015-02-18 16:45:32', '189.123.191.172'),
(334, 'Cristiano', 'usuários', 3, 'entrou', '2015-02-18 16:51:35', '177.17.160.121'),
(335, 'Cristiano', 'Equipe', 3, 'Deletou', '2015-02-18 16:52:24', '177.17.160.121'),
(336, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-18 16:57:08', '177.17.160.121'),
(337, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-18 16:59:08', '177.17.160.121'),
(338, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-18 16:59:55', '177.17.160.121'),
(339, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-18 17:00:49', '177.17.160.121'),
(340, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-18 17:07:39', '177.17.160.121'),
(341, 'Cristiano', 'Produto', 3, 'Deletou', '2015-02-18 17:08:33', '177.17.160.121'),
(342, 'Cristiano', 'Produto', 3, 'Alterou', '2015-02-18 17:11:13', '177.17.160.121'),
(343, 'Cristiano', 'Produto', 3, 'Alterou', '2015-02-18 17:14:24', '177.17.160.121'),
(344, 'Cristiano', 'Produto', 3, 'Alterou', '2015-02-18 17:16:54', '177.17.160.121'),
(345, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-18 17:23:17', '177.17.160.121'),
(346, 'Cristiano', 'Produto', 3, 'Alterou', '2015-02-18 17:25:17', '177.17.160.121'),
(347, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-18 17:28:39', '177.17.160.121'),
(348, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-18 17:30:23', '177.17.160.121'),
(349, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-18 17:33:29', '177.17.160.121'),
(350, 'Cristiano', 'Produto', 3, 'Alterou', '2015-02-18 17:33:51', '177.17.160.121'),
(351, 'Cristiano', 'Produto', 3, 'Alterou', '2015-02-18 17:34:10', '177.17.160.121'),
(352, 'Cristiano', 'Produto', 3, 'Alterou', '2015-02-18 17:35:52', '177.17.160.121'),
(353, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-18 17:37:30', '177.17.160.121'),
(354, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-18 17:40:25', '177.17.160.121'),
(355, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-18 17:46:30', '177.17.160.121'),
(356, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-18 17:49:09', '177.17.160.121'),
(357, 'Cristiano', 'usuários', 3, 'saiu', '2015-02-18 17:49:41', '177.17.160.121'),
(358, 'Administrador', 'usuários', 1, 'entrou', '2015-02-19 08:14:40', '189.123.191.172'),
(359, 'Administrador', 'Produto', 1, 'Deletou', '2015-02-19 08:14:56', '189.123.191.172'),
(360, 'Administrador', 'Produto', 1, 'Alterou', '2015-02-19 08:15:04', '189.123.191.172'),
(361, 'Administrador', 'Produto', 1, 'Alterou', '2015-02-19 08:15:14', '189.123.191.172'),
(362, 'Administrador', 'Produto', 1, 'Alterou', '2015-02-19 08:15:28', '189.123.191.172'),
(363, 'Administrador', 'Produto', 1, 'Alterou', '2015-02-19 08:15:39', '189.123.191.172'),
(364, 'Administrador', 'Configuração', 1, 'Alterou', '2015-02-19 08:26:17', '189.123.191.172'),
(365, 'Cristiano', 'usuários', 3, 'entrou', '2015-02-23 10:39:27', '179.182.202.158'),
(366, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 10:41:31', '179.182.202.158'),
(367, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 10:45:41', '179.182.202.158'),
(368, 'Cristiano', 'Categoria', 3, 'Alterou', '2015-02-23 10:50:34', '179.182.202.158'),
(369, 'Cristiano', 'Categoria', 3, 'Alterou', '2015-02-23 10:51:17', '179.182.202.158'),
(370, 'Cristiano', 'Fornecedor', 3, 'Deletou', '2015-02-23 10:52:33', '179.182.202.158'),
(371, 'Cristiano', 'Fornecedor', 3, 'Deletou', '2015-02-23 10:52:46', '179.182.202.158'),
(372, 'Cristiano', 'Fornecedor', 3, 'Deletou', '2015-02-23 10:52:52', '179.182.202.158'),
(373, 'Cristiano', 'Fornecedor', 3, 'Deletou', '2015-02-23 10:53:03', '179.182.202.158'),
(374, 'Cristiano', 'Fornecedor', 3, 'Deletou', '2015-02-23 10:53:09', '179.182.202.158'),
(375, 'Cristiano', 'Fornecedor', 3, 'Deletou', '2015-02-23 10:53:15', '179.182.202.158'),
(376, 'Cristiano', 'Equipe', 3, 'Alterou', '2015-02-23 10:57:01', '179.182.202.158'),
(377, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 10:59:42', '179.182.202.158'),
(378, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 11:01:15', '179.182.202.158'),
(379, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 11:01:51', '179.182.202.158'),
(380, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 11:03:21', '179.182.202.158'),
(381, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 11:05:24', '179.182.202.158'),
(382, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 11:06:20', '179.182.202.158'),
(383, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 11:07:05', '179.182.202.158'),
(384, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 11:08:03', '179.182.202.158'),
(385, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 11:09:39', '179.182.202.158'),
(386, 'Cristiano', 'usuários', 3, 'entrou', '2015-02-23 13:32:21', '179.176.110.85'),
(387, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 13:34:02', '179.176.110.85'),
(388, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 13:34:47', '179.176.110.85'),
(389, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 13:35:57', '179.176.110.85'),
(390, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 13:36:51', '179.176.110.85'),
(391, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 13:38:17', '179.176.110.85'),
(392, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 13:39:00', '179.176.110.85'),
(393, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 13:39:50', '179.176.110.85'),
(394, 'Cristiano', 'Produto', 3, 'Alterou', '2015-02-23 13:41:36', '179.176.110.85'),
(395, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 13:42:30', '179.176.110.85'),
(396, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 13:43:12', '179.176.110.85'),
(397, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 13:43:55', '179.176.110.85'),
(398, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 13:45:51', '179.176.110.85'),
(399, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 13:46:37', '179.176.110.85'),
(400, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 13:47:25', '179.176.110.85'),
(401, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 13:48:14', '179.176.110.85'),
(402, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 13:49:07', '179.176.110.85'),
(403, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 13:50:39', '179.176.110.85'),
(404, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 13:51:21', '179.176.110.85'),
(405, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 13:52:02', '179.176.110.85'),
(406, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 13:52:47', '179.176.110.85'),
(407, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 13:53:26', '179.176.110.85'),
(408, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 13:54:20', '179.176.110.85'),
(409, 'Cristiano', 'usuários', 3, 'entrou', '2015-02-23 15:36:45', '179.176.110.85'),
(410, 'Cristiano', 'Serviço', 3, 'Deletou', '2015-02-23 15:37:53', '179.176.110.85'),
(411, 'Cristiano', 'Equipe', 3, 'Alterou', '2015-02-23 15:38:38', '179.176.110.85'),
(412, 'Cristiano', 'Equipe', 3, 'Cadastrou', '2015-02-23 15:41:53', '179.176.110.85'),
(413, 'Cristiano', 'Equipe', 3, 'Publicou', '2015-02-23 15:42:00', '179.176.110.85'),
(414, 'Cristiano', 'Categoria', 3, 'Cadastrou', '2015-02-23 15:46:07', '179.176.110.85'),
(415, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 15:51:08', '179.176.110.85'),
(416, 'Cristiano', 'usuários', 3, 'entrou', '2015-02-23 15:55:26', '179.176.110.85'),
(417, 'Cristiano', 'Produto', 3, 'Alterou', '2015-02-23 15:57:41', '179.176.110.85'),
(418, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 16:00:56', '179.176.110.85'),
(419, 'Cristiano', 'Produto', 3, 'Cadastrou', '2015-02-23 16:03:56', '179.176.110.85'),
(420, 'Cristiano', 'usuários', 3, 'entrou', '2015-02-24 16:31:54', '189.27.105.91'),
(421, 'Cristiano', 'Equipe', 3, 'Alterou', '2015-02-24 16:32:43', '189.27.105.91'),
(422, '0', 'Equipe', 0, 'Alterou', '2015-02-24 16:32:45', '189.27.105.91'),
(423, 'Cristiano', 'Equipe', 3, 'Alterou', '2015-02-24 16:38:51', '189.27.105.91'),
(424, 'Cristiano', 'Equipe', 3, 'Alterou', '2015-02-24 16:46:06', '189.27.105.91'),
(425, 'Cristiano', 'usuários', 3, 'entrou', '2015-03-03 16:42:45', '179.179.142.133'),
(426, 'Cristiano', 'Equipe', 3, 'Alterou', '2015-03-03 16:43:38', '179.179.142.133'),
(427, 'Administrador', 'usuários', 1, 'entrou', '2015-05-30 11:41:22', '201.11.177.59'),
(428, 'Administrador', 'usuários', 3, 'alterou senha', '2015-05-30 11:42:08', '201.11.177.59'),
(429, 'Administrador', 'usuários', 3, 'alterou', '2015-05-30 11:42:08', '201.11.177.59'),
(430, 'Administrador', 'usuários', 1, 'saiu', '2015-05-30 11:42:13', '201.11.177.59'),
(431, 'Cristiano', 'usuários', 3, 'entrou', '2015-05-30 11:42:25', '201.11.177.59'),
(432, 'Cristiano', 'Fornecedor', 3, 'Cadastrou', '2015-05-30 11:53:25', '201.11.177.59'),
(433, 'Cristiano', 'Fornecedor', 3, 'Deletou', '2015-05-30 11:53:35', '201.11.177.59'),
(434, 'Cristiano', 'usuários', 3, 'saiu', '2015-05-30 11:56:05', '201.11.177.59'),
(435, 'Administrador', 'usuários', 1, 'entrou', '2015-05-30 11:56:14', '201.11.177.59'),
(436, 'Administrador', 'Configuração', 1, 'Cadastrou', '2015-05-30 11:56:51', '201.11.177.59'),
(437, 'Administrador', 'usuários', 1, 'entrou', '2015-06-05 08:47:32', '189.123.188.54'),
(438, 'Administrador', 'Configuração', 1, 'Cadastrou', '2016-03-12 00:52:05', '127.0.0.1'),
(439, 'Administrador', 'usuários', 1, 'entrou', '2016-03-12 12:51:53', '127.0.0.1'),
(440, 'Administrador', 'Banner', 1, 'Alterou', '2016-03-12 12:52:42', '127.0.0.1'),
(441, 'Administrador', 'Banner', 1, 'Cadastrou', '2016-03-12 12:53:00', '127.0.0.1'),
(442, 'Administrador', 'Banner', 1, 'Publicou', '2016-03-12 12:53:03', '127.0.0.1');

-- --------------------------------------------------------

--
-- Table structure for table `permissao_secao_fixa`
--

CREATE TABLE IF NOT EXISTS `permissao_secao_fixa` (
  `id` int(11) NOT NULL,
  `secao_fixa_id` int(11) NOT NULL DEFAULT '0',
  `usuarios_id` int(11) NOT NULL,
  `cadastrar` int(1) NOT NULL DEFAULT '0',
  `alterar` int(1) NOT NULL DEFAULT '0',
  `excluir` int(1) NOT NULL DEFAULT '0',
  `publicar` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissao_secao_fixa`
--

INSERT INTO `permissao_secao_fixa` (`id`, `secao_fixa_id`, `usuarios_id`, `cadastrar`, `alterar`, `excluir`, `publicar`) VALUES
(68, 1, 1, 1, 1, 1, 1),
(69, 8, 1, 1, 1, 1, 1),
(70, 9, 1, 1, 1, 1, 1),
(71, 3, 1, 1, 1, 1, 1),
(72, 4, 1, 1, 1, 1, 1),
(73, 5, 1, 1, 1, 1, 1),
(74, 7, 1, 1, 1, 1, 1),
(75, 6, 1, 1, 1, 1, 1),
(76, 1, 3, 1, 1, 1, 1),
(77, 8, 3, 1, 1, 1, 1),
(78, 9, 3, 1, 1, 1, 1),
(79, 3, 3, 1, 1, 1, 1),
(80, 4, 3, 1, 1, 1, 1),
(81, 5, 3, 1, 1, 1, 1),
(82, 7, 3, 1, 1, 1, 1),
(83, 6, 3, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `produto`
--

CREATE TABLE IF NOT EXISTS `produto` (
  `id` int(11) NOT NULL,
  `categoria_id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `video` varchar(400) NOT NULL,
  `imagem` varchar(255) NOT NULL,
  `texto` text NOT NULL,
  `destaque` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produto`
--

INSERT INTO `produto` (`id`, `categoria_id`, `nome`, `video`, `imagem`, `texto`, `destaque`, `status`) VALUES
(4, 2, 'Levantador de Sacos', '', 'arq_produto/produto_1424285828.jpg', '<strong>Equipamento para elevar sacaria at&eacute; a cabe&ccedil;a do operador, agilizando a movimenta&ccedil;&atilde;o e evitando contus&otilde;es.</strong>', 1, 1),
(5, 2, 'Aspirador Industrial', '', 'arq_produto/produto_1424285948.jpg', '<strong>Equipamento para suc&ccedil;ao e aspira&ccedil;&atilde;o de residuos de gr&atilde;os, excelente para limpeza de fundo de silos, p&eacute; de elevadores, roscas sem fim e outros.</strong>', 1, 1),
(6, 2, 'Calador de Amostras', '', 'arq_produto/produto_1424285995.png', '', 0, 1),
(7, 2, 'Elevador de canecas', '', 'arq_produto/produto_1424286049.jpg', '<strong>Equipamento para transporte de gr&atilde;os a granel, de forma vertical. Servindo para abastecer m&aacute;quinas, silos dep&oacute;sitos, balan&ccedil;as etc...</strong>', 0, 1),
(9, 2, 'Rosca Transportadora helicoidal', '', 'arq_produto/produto_1424287397.jpg', '<strong>Equipamento para transporte de gr&atilde;os de forma horizontal. Servindo para abastecer m&aacute;quinas, silos dep&oacute;sitos, etc...</strong>', 1, 1),
(10, 2, 'Costuradeira Portátil', '', 'arq_produto/produto_1424287719.jpg', '<b>Equipamento para costurar e &nbsp;fechar boca de sacaria em geral. &nbsp;</b>', 1, 1),
(11, 2, 'Fita Transportadora', '', 'arq_produto/produto_1424287823.jpg', '<strong>Equipamento para transporte de gr&atilde;os a granel de forma horizontal, utilizando-se de correia para o transporte do mesmo.</strong>', 0, 1),
(12, 2, 'Chupim Elevador', '', 'arq_produto/produto_1424288009.jpg', '<b>Equipamento m&oacute;vel para transporte de gr&atilde;os. Ultilizado principalmente para cargas e descargas a granel.</b>', 0, 1),
(13, 2, 'Paleteira Manual', '', 'arq_produto/produto_1424288250.jpg', '<strong>Equipamento para transporte de pallets em geral.</strong>', 0, 1),
(14, 2, 'Medidor de Umidade Digital', '', 'arq_produto/produto_1424288425.jpg', '<strong>Equipamento destinado a medir a umidade dos gr&atilde;os, de forma digital.</strong>', 0, 1),
(15, 2, 'Misturador de Ração', '', 'arq_produto/produto_1424288790.jpg', '<b>Equipamento para mistura de rac&otilde;es, sal e premix &nbsp;at&eacute; &nbsp;500, 1000 e 2.000 kilos.</b>', 0, 1),
(16, 2, 'Testadora de Arroz', '', 'arq_produto/produto_1424288949.jpg', '<strong>Equipamento para teste de amostras de arroz.</strong>', 0, 1),
(17, 2, 'Balança Digital', '', 'arq_produto/produto_1424698891.jpg', '<strong>Balan&ccedil;a Digital para pesagens de 0 a 300 kilos.</strong>', 0, 1),
(18, 2, 'Empilhadeira Manual', '', 'arq_produto/produto_1424699141.jpg', '<strong>Equipamento Manual para transporte e empilhamento de mercadorias, capacidade at&eacute; 2.000 kilos, atingindo uma altura m&aacute;xima de 1.60 mts.</strong>', 0, 1),
(19, 4, 'Amortecedor de linha', '', 'arq_produto/produto_1424699982.jpg', '', 0, 1),
(20, 4, 'Amortecedor Final', '', 'arq_produto/produto_1424700074.jpg', '', 0, 1),
(21, 4, 'Barbante', '', 'arq_produto/produto_1424700111.jpg', '', 0, 1),
(22, 4, 'Biela e Molas Pré-limpeza', '', 'arq_produto/produto_1424700201.jpg', '', 0, 1),
(23, 4, 'Bifurcada', '', 'arq_produto/produto_1424700324.jpg', '', 0, 1),
(24, 4, 'Curva, presilha  e anel', '', 'arq_produto/produto_1424700380.jpg', '', 0, 1),
(25, 4, 'Entrada dupla Y', '', 'arq_produto/produto_1424700425.jpg', '', 0, 1),
(26, 2, 'Grelhas pra fornalha do secador', '', 'arq_produto/produto_1424700483.jpg', '', 0, 1),
(27, 2, 'Tubos Metálicos', '', 'arq_produto/produto_1424700579.jpg', '', 0, 1),
(28, 4, 'Tubos Metálicos', '', 'arq_produto/produto_1424709242.jpg', '', 0, 1),
(29, 4, 'Peneiras de Pré-limpeza', '', 'arq_produto/produto_1424709287.jpg', '', 0, 1),
(30, 4, 'Correia transportadora e elevadora', '', 'arq_produto/produto_1424709357.jpg', '', 0, 1),
(31, 4, 'Canecas Metalicas', '', 'arq_produto/produto_1424709411.jpg', '', 0, 1),
(32, 4, 'Caneca vazada polietileno', '', 'arq_produto/produto_1424709497.jpg', '', 0, 1),
(33, 4, 'Caneca convencional polietileno', '', 'arq_produto/produto_1424709540.jpg', '', 0, 1),
(34, 4, 'caneca vazada metalica', '', 'arq_produto/produto_1424709590.jpg', '', 0, 1),
(35, 2, 'balança para amostras', '', 'arq_produto/produto_1424709750.jpg', '', 0, 1),
(36, 4, 'Concha e calador', '', 'arq_produto/produto_1424709792.jpg', '', 0, 1),
(37, 4, 'Correias V', '', 'arq_produto/produto_1424709835.jpg', '', 0, 1),
(38, 4, 'Fio para Sacaria', '', 'arq_produto/produto_1424709951.jpg', '', 0, 1),
(39, 4, 'Mancais, Buchas e Rolamentos', '', 'arq_produto/produto_1424709997.jpg', '', 0, 1),
(40, 4, 'Escova p/ polidor de feijão', '', 'arq_produto/produto_1424710045.jpg', '', 0, 1),
(41, 4, 'kit testadora de arroz', '', 'arq_produto/produto_1424710094.jpg', '', 0, 1),
(42, 4, 'Oleo de mamona', '', 'arq_produto/produto_1424710147.jpg', '', 0, 1),
(43, 4, 'Parafusos para canecas', '', 'arq_produto/produto_1424710239.jpg', '', 0, 1),
(44, 4, 'Rebolos para Brunidores', '', 'arq_produto/produto_1424710280.jpg', '', 0, 1),
(45, 4, 'peneiras de teste', '', 'arq_produto/produto_1424710322.jpg', '', 0, 1),
(46, 4, 'Polias de Canal', '', 'arq_produto/produto_1424710367.jpg', '', 0, 1),
(47, 4, 'Roletes descascadores de arroz', '', 'arq_produto/produto_1424710406.jpg', '', 0, 1),
(48, 4, 'Telas para polidores e brunidores', '', 'arq_produto/produto_1424710459.jpg', '', 0, 1),
(49, 5, 'balanca analitica marte mod. AS5500C', '', 'arq_produto/produto_1424717468.jpg', '<strong>Balan&ccedil;a anal&iacute;tica divisor 0,01 gramas. Revisada, em otimo estado de conserva&ccedil;&atilde;o. &nbsp;R$990,00</strong>', 0, 1),
(50, 5, 'Descascador de Arroz, marca Zaccarias mod. 50', '', 'arq_produto/produto_1424718056.jpg', '<strong>Descascador de arroz, com sistema de separa&ccedil;&atilde;o de casca conjugado, marca Zaccarias, para a produ&ccedil;&atilde;o de at&eacute; 50 sacas/hora. &nbsp;R$17.000,00</strong>', 0, 1),
(51, 5, 'Pré-Limpeza Kepler Weber, mod. LC - 160 SP', '', 'arq_produto/produto_1424718236.jpg', '<strong>Pr&eacute;-limpeza para Cereais marca Kepler Weber, modelo LC-160, revisada &nbsp; &nbsp;R$21.000,00</strong>', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `produto_categoria`
--

CREATE TABLE IF NOT EXISTS `produto_categoria` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produto_categoria`
--

INSERT INTO `produto_categoria` (`id`, `nome`, `status`) VALUES
(2, 'Máquinas e Equipamentos', 1),
(4, 'Peças de Reposição', 1),
(5, 'Equipamentos Usados', 1);

-- --------------------------------------------------------

--
-- Table structure for table `produto_foto`
--

CREATE TABLE IF NOT EXISTS `produto_foto` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `produto_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produto_foto`
--

INSERT INTO `produto_foto` (`id`, `nome`, `img`, `produto_id`) VALUES
(1, '10712764_10201396182427104_8692349326299228799_n.jpg', 'arq_produto/1/31d434c1857228d9881cd5b0b23b1338.jpg', 1),
(2, 'avatar.png', 'arq_produto/1/4a301072dec6b6a49050e5b294cd7983.png', 1),
(3, 'semFoto.jpg', 'arq_produto/1/0a47e8561bf765153845251407e9c62d.jpg', 1),
(4, '1962621_10200503334026452_86019119_n.jpg', 'arq_produto/2/349070e123f1dd1290ef05be56580e03.jpg', 2),
(5, '10712764_10201396182427104_8692349326299228799_n.jpg', 'arq_produto/2/31d434c1857228d9881cd5b0b23b1338.jpg', 2),
(6, 'avatar.png', 'arq_produto/2/4a301072dec6b6a49050e5b294cd7983.png', 2),
(8, 'semFoto.jpg', 'arq_produto/2/0a47e8561bf765153845251407e9c62d.jpg', 2),
(10, '3726_4.jpg', 'arq_produto/3/68f2aa9e27d888953f326c09aa68fa78.jpg', 3),
(11, 'ft22-04-2013_152156.jpg', 'arq_produto/3/7c6b80d13d60adf1d8d6878ff8bad689.jpg', 3),
(12, 'proup20130215092946.jpg', 'arq_produto/3/d0e8d0ad49c3cd873b630000516d1d47.jpg', 3);

-- --------------------------------------------------------

--
-- Table structure for table `secao_fixa`
--

CREATE TABLE IF NOT EXISTS `secao_fixa` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `menu` varchar(255) NOT NULL,
  `ctrl` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `ordem` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `secao_fixa`
--

INSERT INTO `secao_fixa` (`id`, `titulo`, `menu`, `ctrl`, `img`, `ordem`) VALUES
(1, 'Usuários', 'Usuários', '../usuarios/ctrlUsuarios.php', 'images/icons/todos/adminUser.png', 0),
(3, 'banner', 'Banners', '../ctrlBanner.php', 'images/icons/todos/application.png', 1),
(4, 'equipe', 'Equipe', '../ctrlEquipe.php', 'images/icons/todos/user2.png', 2),
(5, 'servico', 'Serviços', '../ctrlServico.php', 'images/icons/todos/companies.png', 3),
(6, 'produto', 'Produtos', '../ctrlProduto.php', 'images/icons/todos/bag.png', 5),
(7, 'categoria', 'Produto Categoria', '../ctrlCategoria.php', 'images/icons/todos/files.png', 4),
(8, 'cliente', 'Clientes', '../ctrlCliente.php', 'images/icons/todos/users2.png', 0),
(9, 'fornecedor', 'Fornecedores', '../ctrlFornecedor.php', 'images/icons/todos/users.png', 0);

-- --------------------------------------------------------

--
-- Table structure for table `secao_fixa_menu`
--

CREATE TABLE IF NOT EXISTS `secao_fixa_menu` (
  `id` int(11) NOT NULL,
  `secao_fixa_id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `url` varchar(300) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `secao_fixa_menu`
--

INSERT INTO `secao_fixa_menu` (`id`, `secao_fixa_id`, `titulo`, `url`) VALUES
(8, 1, 'Cadastrar', 'index.php?acao=frmCadUsuario&ctrl=usuarios'),
(9, 1, 'Listar', 'index.php?acao=listaUsuarios&ctrl=usuarios'),
(12, 3, 'Cadastrar', 'index.php?acao=frmCad&ctrl=banner'),
(13, 3, 'Listar', 'index.php?acao=listar&ctrl=banner'),
(14, 4, 'Cadastrar', 'index.php?acao=frmCad&ctrl=equipe'),
(15, 4, 'Listar', 'index.php?acao=listar&ctrl=equipe'),
(16, 5, 'Cadastrar', 'index.php?acao=frmCad&ctrl=servico'),
(17, 5, 'Listar', 'index.php?acao=listar&ctrl=servico'),
(18, 6, 'Cadastrar', 'index.php?acao=frmCad&ctrl=produto'),
(19, 6, 'Listar', 'index.php?acao=listar&ctrl=produto'),
(20, 7, 'Cadastrar', 'index.php?acao=frmCad&ctrl=categoria'),
(21, 7, 'Listar', 'index.php?acao=listar&ctrl=categoria'),
(22, 8, 'Cadastrar', 'index.php?acao=frmCad&ctrl=cliente'),
(23, 8, 'Listar', 'index.php?acao=listar&ctrl=cliente'),
(24, 9, 'Cadastrar', 'index.php?acao=frmCad&ctrl=fornecedor'),
(25, 9, 'Listar', 'index.php?acao=listar&ctrl=fornecedor');

-- --------------------------------------------------------

--
-- Table structure for table `servico`
--

CREATE TABLE IF NOT EXISTS `servico` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `servico`
--

INSERT INTO `servico` (`id`, `nome`, `status`) VALUES
(2, 'Costuradeira Portátil', 1),
(3, 'Testadora de Arroz', 1),
(4, 'Determinador de Umidade', 1);

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `senha` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `img` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `nivel` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`id`, `nome`, `senha`, `email`, `img`, `status`, `nivel`) VALUES
(1, 'Administrador', 'd2f1fd64e6cd3b064878156b0987f872', 'paulo@pixelgo.com.br', '', 1, 1),
(3, 'Cristiano', '634b098226e5557d47bf09edf9c318ea', 'cristiano@remapmaquinas.com.br', '', 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `configuracao`
--
ALTER TABLE `configuracao`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `equipe`
--
ALTER TABLE `equipe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fornecedor`
--
ALTER TABLE `fornecedor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissao_secao_fixa`
--
ALTER TABLE `permissao_secao_fixa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissao_secao_fixa_FKIndex1` (`usuarios_id`),
  ADD KEY `permissao_secao_fixa_FKIndex2` (`secao_fixa_id`);

--
-- Indexes for table `produto`
--
ALTER TABLE `produto`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produto_categoria`
--
ALTER TABLE `produto_categoria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produto_foto`
--
ALTER TABLE `produto_foto`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `secao_fixa`
--
ALTER TABLE `secao_fixa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `secao_fixa_menu`
--
ALTER TABLE `secao_fixa_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_secao_fixa` (`secao_fixa_id`);

--
-- Indexes for table `servico`
--
ALTER TABLE `servico`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `configuracao`
--
ALTER TABLE `configuracao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `equipe`
--
ALTER TABLE `equipe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `fornecedor`
--
ALTER TABLE `fornecedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=443;
--
-- AUTO_INCREMENT for table `permissao_secao_fixa`
--
ALTER TABLE `permissao_secao_fixa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=84;
--
-- AUTO_INCREMENT for table `produto`
--
ALTER TABLE `produto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `produto_categoria`
--
ALTER TABLE `produto_categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `produto_foto`
--
ALTER TABLE `produto_foto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `secao_fixa`
--
ALTER TABLE `secao_fixa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `secao_fixa_menu`
--
ALTER TABLE `secao_fixa_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `servico`
--
ALTER TABLE `servico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `permissao_secao_fixa`
--
ALTER TABLE `permissao_secao_fixa`
  ADD CONSTRAINT `permissao_secao_fixa_ibfk_1` FOREIGN KEY (`secao_fixa_id`) REFERENCES `secao_fixa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permissao_secao_fixa_ibfk_2` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `secao_fixa_menu`
--
ALTER TABLE `secao_fixa_menu`
  ADD CONSTRAINT `secao_fixa_menu_ibfk_1` FOREIGN KEY (`secao_fixa_id`) REFERENCES `secao_fixa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
