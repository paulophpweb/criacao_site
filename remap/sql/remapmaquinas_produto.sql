-- MySQL dump 10.13  Distrib 5.7.9, for osx10.9 (x86_64)
--
-- Host: localhost    Database: remapmaquinas
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `produto`
--

DROP TABLE IF EXISTS `produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria_id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `video` varchar(400) NOT NULL,
  `imagem` varchar(255) NOT NULL,
  `texto` text NOT NULL,
  `destaque` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produto`
--

LOCK TABLES `produto` WRITE;
/*!40000 ALTER TABLE `produto` DISABLE KEYS */;
INSERT INTO `produto` VALUES (4,2,'Levantador de Sacos','','arq_produto/produto_1424285828.jpg','<strong>Equipamento para elevar sacaria at&eacute; a cabe&ccedil;a do operador, agilizando a movimenta&ccedil;&atilde;o e evitando contus&otilde;es.</strong>',1,1),(5,2,'Aspirador Industrial','','arq_produto/produto_1424285948.jpg','<strong>Equipamento para suc&ccedil;ao e aspira&ccedil;&atilde;o de residuos de gr&atilde;os, excelente para limpeza de fundo de silos, p&eacute; de elevadores, roscas sem fim e outros.</strong>',1,1),(6,2,'Calador de Amostras','','arq_produto/produto_1424285995.png','',0,1),(7,2,'Elevador de canecas','','arq_produto/produto_1424286049.jpg','<strong>Equipamento para transporte de gr&atilde;os a granel, de forma vertical. Servindo para abastecer m&aacute;quinas, silos dep&oacute;sitos, balan&ccedil;as etc...</strong>',0,1),(9,2,'Rosca Transportadora helicoidal','','arq_produto/produto_1424287397.jpg','<strong>Equipamento para transporte de gr&atilde;os de forma horizontal. Servindo para abastecer m&aacute;quinas, silos dep&oacute;sitos, etc...</strong>',1,1),(10,2,'Costuradeira Portátil','','arq_produto/produto_1424287719.jpg','<b>Equipamento para costurar e &nbsp;fechar boca de sacaria em geral. &nbsp;</b>',1,1),(11,2,'Fita Transportadora','','arq_produto/produto_1424287823.jpg','<strong>Equipamento para transporte de gr&atilde;os a granel de forma horizontal, utilizando-se de correia para o transporte do mesmo.</strong>',0,1),(12,2,'Chupim Elevador','','arq_produto/produto_1424288009.jpg','<b>Equipamento m&oacute;vel para transporte de gr&atilde;os. Ultilizado principalmente para cargas e descargas a granel.</b>',0,1),(13,2,'Paleteira Manual','','arq_produto/produto_1424288250.jpg','<strong>Equipamento para transporte de pallets em geral.</strong>',0,1),(14,2,'Medidor de Umidade Digital','','arq_produto/produto_1424288425.jpg','<strong>Equipamento destinado a medir a umidade dos gr&atilde;os, de forma digital.</strong>',0,1),(15,2,'Misturador de Ração','','arq_produto/produto_1424288790.jpg','<b>Equipamento para mistura de rac&otilde;es, sal e premix &nbsp;at&eacute; &nbsp;500, 1000 e 2.000 kilos.</b>',0,1),(16,2,'Testadora de Arroz','','arq_produto/produto_1424288949.jpg','<strong>Equipamento para teste de amostras de arroz.</strong>',0,1),(17,2,'Balança Digital','','arq_produto/produto_1424698891.jpg','<strong>Balan&ccedil;a Digital para pesagens de 0 a 300 kilos.</strong>',0,1),(18,2,'Empilhadeira Manual','','arq_produto/produto_1424699141.jpg','<strong>Equipamento Manual para transporte e empilhamento de mercadorias, capacidade at&eacute; 2.000 kilos, atingindo uma altura m&aacute;xima de 1.60 mts.</strong>',0,1),(19,4,'Amortecedor de linha','','arq_produto/produto_1424699982.jpg','',0,1),(20,4,'Amortecedor Final','','arq_produto/produto_1424700074.jpg','',0,1),(21,4,'Barbante','','arq_produto/produto_1424700111.jpg','',0,1),(22,4,'Biela e Molas Pré-limpeza','','arq_produto/produto_1424700201.jpg','',0,1),(23,4,'Bifurcada','','arq_produto/produto_1424700324.jpg','',0,1),(24,4,'Curva, presilha  e anel','','arq_produto/produto_1424700380.jpg','',0,1),(25,4,'Entrada dupla Y','','arq_produto/produto_1424700425.jpg','',0,1),(26,2,'Grelhas pra fornalha do secador','','arq_produto/produto_1424700483.jpg','',0,1),(27,2,'Tubos Metálicos','','arq_produto/produto_1424700579.jpg','',0,1),(28,4,'Tubos Metálicos','','arq_produto/produto_1424709242.jpg','',0,1),(29,4,'Peneiras de Pré-limpeza','','arq_produto/produto_1424709287.jpg','',0,1),(30,4,'Correia transportadora e elevadora','','arq_produto/produto_1424709357.jpg','',0,1),(31,4,'Canecas Metalicas','','arq_produto/produto_1424709411.jpg','',0,1),(32,4,'Caneca vazada polietileno','','arq_produto/produto_1424709497.jpg','',0,1),(33,4,'Caneca convencional polietileno','','arq_produto/produto_1424709540.jpg','',0,1),(34,4,'caneca vazada metalica','','arq_produto/produto_1424709590.jpg','',0,1),(35,2,'balança para amostras','','arq_produto/produto_1424709750.jpg','',0,1),(36,4,'Concha e calador','','arq_produto/produto_1424709792.jpg','',0,1),(37,4,'Correias V','','arq_produto/produto_1424709835.jpg','',0,1),(38,4,'Fio para Sacaria','','arq_produto/produto_1424709951.jpg','',0,1),(39,4,'Mancais, Buchas e Rolamentos','','arq_produto/produto_1424709997.jpg','',0,1),(40,4,'Escova p/ polidor de feijão','','arq_produto/produto_1424710045.jpg','',0,1),(41,4,'kit testadora de arroz','','arq_produto/produto_1424710094.jpg','',0,1),(42,4,'Oleo de mamona','','arq_produto/produto_1424710147.jpg','',0,1),(43,4,'Parafusos para canecas','','arq_produto/produto_1424710239.jpg','',0,1),(44,4,'Rebolos para Brunidores','','arq_produto/produto_1424710280.jpg','',0,1),(45,4,'peneiras de teste','','arq_produto/produto_1424710322.jpg','',0,1),(46,4,'Polias de Canal','','arq_produto/produto_1424710367.jpg','',0,1),(47,4,'Roletes descascadores de arroz','','arq_produto/produto_1424710406.jpg','',0,1),(48,4,'Telas para polidores e brunidores','','arq_produto/produto_1424710459.jpg','',0,1),(49,5,'balanca analitica marte mod. AS5500C','','arq_produto/produto_1424717468.jpg','<strong>Balan&ccedil;a anal&iacute;tica divisor 0,01 gramas. Revisada, em otimo estado de conserva&ccedil;&atilde;o. &nbsp;R$990,00</strong>',0,1),(50,5,'Descascador de Arroz, marca Zaccarias mod. 50','','arq_produto/produto_1424718056.jpg','<strong>Descascador de arroz, com sistema de separa&ccedil;&atilde;o de casca conjugado, marca Zaccarias, para a produ&ccedil;&atilde;o de at&eacute; 50 sacas/hora. &nbsp;R$17.000,00</strong>',0,1),(51,5,'Pré-Limpeza Kepler Weber, mod. LC - 160 SP','','arq_produto/produto_1424718236.jpg','<strong>Pr&eacute;-limpeza para Cereais marca Kepler Weber, modelo LC-160, revisada &nbsp; &nbsp;R$21.000,00</strong>',0,1);
/*!40000 ALTER TABLE `produto` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-27 14:42:14
