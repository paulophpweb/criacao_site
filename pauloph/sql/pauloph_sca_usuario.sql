-- MySQL dump 10.13  Distrib 5.7.9, for osx10.9 (x86_64)
--
-- Host: localhost    Database: pauloph
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sca_usuario`
--

DROP TABLE IF EXISTS `sca_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sca_usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nm_usuario` varchar(255) NOT NULL DEFAULT '',
  `password_usuario` varchar(255) NOT NULL DEFAULT '',
  `login_usuario` varchar(255) NOT NULL DEFAULT '',
  `id_avatar` int(11) DEFAULT NULL,
  `st_usuario` tinyint(1) NOT NULL DEFAULT '0',
  `id_grupo` int(11) DEFAULT NULL,
  `ultimo_acesso_usuario` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_usuario`),
  KEY `id_avatar` (`id_avatar`),
  KEY `id_grupo` (`id_grupo`),
  CONSTRAINT `sca_usuario_ibfk_1` FOREIGN KEY (`id_grupo`) REFERENCES `sca_grupo` (`id_grupo`),
  CONSTRAINT `sca_usuario_ibfk_2` FOREIGN KEY (`id_avatar`) REFERENCES `sgg_avatar` (`id_avatar`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sca_usuario`
--

LOCK TABLES `sca_usuario` WRITE;
/*!40000 ALTER TABLE `sca_usuario` DISABLE KEYS */;
INSERT INTO `sca_usuario` VALUES (42,'admin','202cb962ac59075b964b07152d234b70','admin',114,1,3,'2016-03-16 23:35:10'),(43,'Paulo 2','d41d8cd98f00b204e9800998ecf8427e','admin2',116,1,3,'2016-03-16 13:15:26'),(46,'Paulo','d41d8cd98f00b204e9800998ecf8427e','adminnnn',NULL,1,10,'2016-03-16 22:02:31');
/*!40000 ALTER TABLE `sca_usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-27 14:38:26
