-- MySQL dump 10.13  Distrib 5.7.9, for osx10.9 (x86_64)
--
-- Host: localhost    Database: localizapecas
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `informacao`
--

DROP TABLE IF EXISTS `informacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `informacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `texto` text,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `informacao`
--

LOCK TABLES `informacao` WRITE;
/*!40000 ALTER TABLE `informacao` DISABLE KEYS */;
INSERT INTO `informacao` VALUES (3,'Dica para Concessionárias e Distribuidores','O site possui um sistema de atualização automática de estoque.\r\nO processo de atualização será automático não importando o horário que seja enviado.\r\nAs atualizações do site funcionam 24 horas 7 dias por semana.  \r\nEnvie o cadastro de sua empresa e receba as Instruções & Autorização de Uso com todas as informações para envio de seu estoque de peças.\r\nCaso seu grupo possua mais de uma concessionária ou distribuidora cadastre cada uma separadamente para obter uma autorização exclusiva para cada empresa.',1),(4,'Dica para o Usuário Final do site','O uso do site para consulta e envio de cotações de peças é gratuito.\r\n\r\nHá duas maneiras de realizar a pesquisa de peças:\r\n\r\n1) Através do CONSULTAR CÓDIGOSda(s) peça(s) desejada(s) em *Você obtém resposta imediata*\r\n\r\n2)Através de cotação - através do preenchimento de formulário para cotação de peça *Você deve aguardar respostas*\r\n\r\nNas duas maneiras acima, seu contato é diretamente com os fornecedores,sem intermediários.',1),(5,'Formato do Arquivo de Importação','Para a importação de seu estoque de peças o site utiliza um arquivo independente por cada MARCA no formato ARQUIVO.TXT\r\nQualquer dúvida entre em contato em nossa área de contato do site.',1);
/*!40000 ALTER TABLE `informacao` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-27 14:46:05
