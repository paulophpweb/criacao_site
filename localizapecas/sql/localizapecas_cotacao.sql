-- MySQL dump 10.13  Distrib 5.7.9, for osx10.9 (x86_64)
--
-- Host: localhost    Database: localizapecas
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cotacao`
--

DROP TABLE IF EXISTS `cotacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cotacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `membro_id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `telefone` varchar(255) NOT NULL,
  `data` datetime NOT NULL,
  `email` varchar(255) NOT NULL,
  `estado` varchar(2) NOT NULL,
  `cidade` varchar(255) NOT NULL,
  `marca_id` int(11) NOT NULL,
  `veiculo_modelo` varchar(255) NOT NULL,
  `combustivel` varchar(255) NOT NULL,
  `ano_modelo_fabricacao` varchar(255) NOT NULL,
  `cor` varchar(255) NOT NULL,
  `chassi` varchar(255) NOT NULL,
  `descricao` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cotacao`
--

LOCK TABLES `cotacao` WRITE;
/*!40000 ALTER TABLE `cotacao` DISABLE KEYS */;
INSERT INTO `cotacao` VALUES (23,2,'Marcelo','(45) 3534-5345','2015-09-23 10:46:36','paulo@netsuprema.com.br','AM','Goiania',3,'fdsfdsa','Diesel','fdsafsad','Azul','32423423423423','gfds gdf gfd h',0),(24,3,'aadfadsf','(32) 4234-3242','2015-09-24 20:20:27','contato@logicsite.com.br','AL','sdfasdfasd',1,'sdfasdf','Diesel','asdfadsf','sadfa','sdfasd','sdfasdfasdfasdfadsfad',0),(25,2,'aadfadsf','(42) 3423-4234','2015-09-24 20:35:53','contato@logicsite.com.br','AL','sdfasdfasd',3,'23423432','Diesel','asdfadsf','sadfa','sdfasd','borracha',0),(26,3,'aadfadsf','(42) 3423-4234','2015-09-24 20:35:53','contato@logicsite.com.br','AL','sdfasdfasd',3,'23423432','Diesel','asdfadsf','sadfa','sdfasd','borracha',0),(28,2,'aadfadsf','(42) 3423-4234','2015-09-24 20:45:32','contato@logicsite.com.br','AL','sdfasdfasd',3,'23423432','Diesel','asdfadsf','sadfa','sdfasd','borracha',0),(29,3,'aadfadsf','(42) 3423-4234','2015-09-24 20:45:32','contato@logicsite.com.br','AL','sdfasdfasd',3,'23423432','Diesel','asdfadsf','sadfa','sdfasd','borracha',0),(30,5,'aadfadsf','(42) 3423-4234','2015-09-24 20:45:32','contato@logicsite.com.br','AL','sdfasdfasd',3,'23423432','Diesel','asdfadsf','sadfa','sdfasd','borracha',0),(31,7,'Marcelo','(45) 3243-2432','2015-09-30 09:58:42','pudicaph17@hotmail.com','GO','Goiania',51,'fdsfdsa','Alcool','fdsafsad','Azul','32423423423423','teste',1);
/*!40000 ALTER TABLE `cotacao` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-27 14:46:05
