-- MySQL dump 10.13  Distrib 5.7.9, for osx10.9 (x86_64)
--
-- Host: localhost    Database: localizapecas
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `texto` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa`
--

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
INSERT INTO `empresa` VALUES (1,'Texto Quem Somos','','<div style=\"text-align: justify;\">O Localiza Pe&ccedil;as Para Tratores disponibiliza uma ferramenta para quem quer encontrar as pe&ccedil;as que procuram de duas formas, fazendo cota&ccedil;&atilde;o de pe&ccedil;as e tamb&eacute;m consultando pelo c&oacute;digo.<br />\r\n<br />\r\nAliamos a praticidade da busca on-line com um universo vasto de fornecedores e milhares de pe&ccedil;as em nosso banco-de-dados que &eacute; atualizado constantemente para que voc&ecirc; encontre o que precisa com os melhores pre&ccedil;os e com os melhores fornecedores do mercado.<br />\r\n<br />\r\nSeja bem-vindo ao nosso site e consulte nossa &aacute;rea de Informa&ccedil;&otilde;es caso seja sua primeira experi&ecirc;ncia utilizando nosso sistema de busca.</div>\r\n<br />\r\nEntre em contato!<br />\r\n<br />\r\n&nbsp;',1),(2,'Texto da Página Principal.','','<div style=\"text-align: justify;\">O Localiza Pe&ccedil;as Para Tratores disponibiliza uma ferramenta para quem quer encontrar as pe&ccedil;as que procuram de duas formas, fazendo cota&ccedil;&atilde;o de pe&ccedil;as e tamb&eacute;m consultando pelo c&oacute;digo. Aliamos a praticidade da busca on-line com um universo vasto de fornecedores e milhares de pe&ccedil;as em nosso banco-de-dados que &eacute; atualizado constantemente para que voc&ecirc; encontre o que precisa com os melhores pre&ccedil;os e com os melhores fornecedores do mercado. Seja bem-vindo ao nosso site e consulte nossa &aacute;rea de Informa&ccedil;&otilde;es caso seja sua primeira experi&ecirc;ncia utilizando nosso sistema de busca.<br />\r\n&nbsp;</div>\r\n',1),(3,'Texto Rodapé','','<div style=\"text-align: justify;\"><span style=\"color:#FFFFFF\">Nosso trabalho visa a utiliza&ccedil;&atilde;o de tecnologia de vanguarda para levar agilidade para nossos usu&aacute;rios, formando assim um elo entre o com&eacute;rcio de pe&ccedil;as para tratores e seus consumidores. Anuncie em nosso site seu estoque de pe&ccedil;as ou atrav&eacute;s de nossos banners rotativos!</span></div>\r\n',1),(4,'Mercado','','<div style=\"text-align: justify;\">Saiba que milhares de empresas recebem a sua cota&ccedil;&atilde;o e v&atilde;o responder rapidamente as suas consultas, ganhe tempo, enviando j&aacute; sua cota&ccedil;&atilde;o e consiga as pe&ccedil;as que esta procurando nos melhores fornecedores, fa&ccedil;a sua cota&ccedil;&atilde;o.<br />\r\n<br />\r\nAliamos a praticidade da busca on-line com um universo vasto de fornecedores e milhares de pe&ccedil;as em nosso banco-de-dados que &eacute; atualizado constantemente para que voc&ecirc; encontre o que precisa com os melhores pre&ccedil;os e com os melhores fornecedores do mercado.<br />\r\n<br />\r\nSeja bem-vindo ao nosso site e consulte nossa &aacute;rea de Informa&ccedil;&otilde;es caso seja sua primeira experi&ecirc;ncia utilizando nosso sistema de busca.</div>\r\n<br />\r\n<br />\r\n&nbsp;',1),(5,'Consultar Peças','','<br />\r\n<strong>ENCONTRE QUEM TEM AS PE&Ccedil;AS QUE VOC&Ecirc; PROCURA<br />\r\n<br />\r\n1- Primeiramente preencha o formul&aacute;rio de <a href=\"http://www.localizapecas.com/site/cotar-pecas\">COTA&Ccedil;&Atilde;O</a> e envie.<br />\r\n2- Depois consiga o <a href=\"http://www.localizapecas.com/site/consultar-codigo\">C&Oacute;DIGO</a> das pe&ccedil;as se quiser consultar voc&ecirc; mesmo.</strong><br />\r\n<br />\r\n<br />\r\nEntre com at&eacute; 5 c&oacute;digos observando que deve colocar virgula entre eles.<br />\r\n<br />\r\nVeja o exemplo de consulta de uma marca:<br />\r\nYamaha<br />\r\n<br />\r\nEntre com ate 5 c&oacute;digos observando que deve colocar virgula entre eles.<br />\r\n<br />\r\nEXEMPLO:<br />\r\nKAR002 &nbsp; , &nbsp;ACNF178A00&nbsp; &nbsp;,&nbsp; 9851M06012 &nbsp; &nbsp;,&nbsp; 9350316802<br />\r\n&nbsp;',1),(6,'Divulgue sua empresa','','<div style=\"text-align: justify;\"><br />\r\nO p&uacute;blico que sua empresa procura est&aacute; aqui: Concession&aacute;rias de ve&iacute;culos, seguradoras, reparadores, lojas e distribuidores de autope&ccedil;as, fabricantes do setor, montadoras e propriet&aacute;rios.<br />\r\n<br />\r\nO Localiza Pe&ccedil;as Para Tratores &eacute; uma ferramenta essencial de trabalho para empresas que compram e vendem pe&ccedil;as diariamente, e para os propriet&aacute;rios de ve&iacute;culos que se utilzam dos servi&ccedil;os do site para achar e comprar as pe&ccedil;as que necessitam. Temos um grande n&uacute;mero de acessos di&aacute;rios o que nos torna a m&iacute;dia ideal para que sua empresa veicule sua marca e produtos.<br />\r\n<br />\r\nO destaque que sua empresa merece: Os an&uacute;ncios tem veicula&ccedil;&atilde;o rotativa no lugar de maior destaque do site, o topo da p&aacute;gina e suas laterais. N&atilde;o importa qual p&aacute;gina que o internauta esteja visitando, seja uma p&aacute;gina institucional, uma p&aacute;gina din&acirc;mica ou um formul&aacute;rio, o seu anuncio sempre estar&aacute; em evid&ecirc;ncia.<br />\r\n&nbsp;</div>\r\n',1),(7,'Informações','','<br />\r\nAqui voc&ecirc; pode tirar suas d&uacute;vidas em rela&ccedil;&atilde;o ao funcionamento de nosso sistema de busca conforme os itens abaixo:<br />\r\n&nbsp;',1);
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-27 14:46:03
