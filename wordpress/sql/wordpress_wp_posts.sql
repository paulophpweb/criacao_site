-- MySQL dump 10.13  Distrib 5.7.9, for osx10.9 (x86_64)
--
-- Host: localhost    Database: wordpress
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wp_posts`
--

DROP TABLE IF EXISTS `wp_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_posts`
--

LOCK TABLES `wp_posts` WRITE;
/*!40000 ALTER TABLE `wp_posts` DISABLE KEYS */;
INSERT INTO `wp_posts` VALUES (1,1,'2016-01-27 11:34:13','2016-01-27 11:34:13','Bem-vindo ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!','Olá, mundo!','','publish','open','open','','ola-mundo','','','2016-01-27 11:34:13','2016-01-27 11:34:13','',0,'http://wordpress.local/?p=1',0,'post','',1),(2,1,'2016-01-27 11:34:13','2016-01-27 11:34:13','Esta é uma página de exemplo. É diferente de um post porque ela ficará em um local e será exibida na navegação do seu site (na maioria dos temas). A maioria das pessoas começa com uma página de introdução aos potenciais visitantes do site. Ela pode ser assim:\n\n<blockquote>Olá! Eu sou um bike courrier de dia, ator amador à noite e este é meu blog. Eu moro em São Paulo, tenho um cachorro chamado Tonico e eu gosto de caipirinhas. (E de ser pego pela chuva.)</blockquote>\n\nou assim:\n\n<blockquote>A XYZ foi fundada em 1971 e desde então vem proporcionando produtos de qualidade a seus clientes. Localizada em Valinhos, XYZ emprega mais de 2.000 pessoas e faz várias contribuições para a comunidade local.</blockquote>\nComo um novo usuário do WordPress, você deve ir até o <a href=\"http://wordpress.local/wp-admin/\">seu painel</a> para excluir essa página e criar novas páginas com seu próprio conteúdo. Divirta-se!','Página de Exemplo','','trash','closed','open','','pagina-exemplo','','','2016-01-27 11:51:05','2016-01-27 11:51:05','',0,'http://wordpress.local/?page_id=2',0,'page','',0),(4,1,'2016-01-27 11:46:47','2016-01-27 11:46:47','a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:1:\"7\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:15:{i:0;s:9:\"permalink\";i:1;s:11:\"the_content\";i:2;s:7:\"excerpt\";i:3;s:13:\"custom_fields\";i:4;s:10:\"discussion\";i:5;s:8:\"comments\";i:6;s:9:\"revisions\";i:7;s:4:\"slug\";i:8;s:6:\"author\";i:9;s:6:\"format\";i:10;s:15:\"page_attributes\";i:11;s:14:\"featured_image\";i:12;s:10:\"categories\";i:13;s:4:\"tags\";i:14;s:15:\"send-trackbacks\";}s:11:\"description\";s:0:\"\";}','Sobre','sobre','publish','closed','closed','','group_56a8adff75e30','','','2016-01-27 11:52:40','2016-01-27 11:52:40','',0,'http://wordpress.local/?post_type=acf-field-group&#038;p=4',0,'acf-field-group','',0),(5,1,'2016-01-27 11:46:47','2016-01-27 11:46:47','a:9:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:5:\"basic\";s:12:\"media_upload\";i:1;}','Texto','texto','publish','closed','closed','','field_56a8ae06f2e98','','','2016-01-27 11:46:47','2016-01-27 11:46:47','',4,'http://wordpress.local/?post_type=acf-field&p=5',0,'acf-field','',0),(6,1,'2016-01-27 11:51:05','2016-01-27 11:51:05','Esta é uma página de exemplo. É diferente de um post porque ela ficará em um local e será exibida na navegação do seu site (na maioria dos temas). A maioria das pessoas começa com uma página de introdução aos potenciais visitantes do site. Ela pode ser assim:\n\n<blockquote>Olá! Eu sou um bike courrier de dia, ator amador à noite e este é meu blog. Eu moro em São Paulo, tenho um cachorro chamado Tonico e eu gosto de caipirinhas. (E de ser pego pela chuva.)</blockquote>\n\nou assim:\n\n<blockquote>A XYZ foi fundada em 1971 e desde então vem proporcionando produtos de qualidade a seus clientes. Localizada em Valinhos, XYZ emprega mais de 2.000 pessoas e faz várias contribuições para a comunidade local.</blockquote>\nComo um novo usuário do WordPress, você deve ir até o <a href=\"http://wordpress.local/wp-admin/\">seu painel</a> para excluir essa página e criar novas páginas com seu próprio conteúdo. Divirta-se!','Página de Exemplo','','inherit','closed','closed','','2-revision-v1','','','2016-01-27 11:51:05','2016-01-27 11:51:05','',2,'http://wordpress.local/index.php/2016/01/27/2-revision-v1/',0,'revision','',0),(7,1,'2016-01-27 11:51:16','2016-01-27 11:51:16','','Sobre','','publish','closed','closed','','sobre','','','2016-01-27 11:51:16','2016-01-27 11:51:16','',0,'http://wordpress.local/?page_id=7',0,'page','',0),(8,1,'2016-01-27 11:51:16','2016-01-27 11:51:16','','Sobre','','inherit','closed','closed','','7-revision-v1','','','2016-01-27 11:51:16','2016-01-27 11:51:16','',7,'http://wordpress.local/index.php/2016/01/27/7-revision-v1/',0,'revision','',0),(10,1,'2016-01-27 12:10:50','2016-01-27 12:10:50','a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:17:\"email_recebimento\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:15:{i:0;s:9:\"permalink\";i:1;s:11:\"the_content\";i:2;s:7:\"excerpt\";i:3;s:13:\"custom_fields\";i:4;s:10:\"discussion\";i:5;s:8:\"comments\";i:6;s:9:\"revisions\";i:7;s:4:\"slug\";i:8;s:6:\"author\";i:9;s:6:\"format\";i:10;s:15:\"page_attributes\";i:11;s:14:\"featured_image\";i:12;s:10:\"categories\";i:13;s:4:\"tags\";i:14;s:15:\"send-trackbacks\";}s:11:\"description\";s:0:\"\";}','Email Recebimento','email-recebimento','publish','closed','closed','','group_56a8b393d1b5e','','','2016-01-27 12:10:50','2016-01-27 12:10:50','',0,'http://wordpress.local/?post_type=acf-field-group&#038;p=10',0,'acf-field-group','',0),(11,1,'2016-01-27 12:10:50','2016-01-27 12:10:50','a:9:{s:4:\"type\";s:5:\"email\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";}','E-mail','email','publish','closed','closed','','field_56a8b3a9b55a7','','','2016-01-27 12:10:50','2016-01-27 12:10:50','',10,'http://wordpress.local/?post_type=acf-field&p=11',0,'acf-field','',0),(12,1,'2016-01-27 12:11:15','2016-01-27 12:11:15','','Paulo','','publish','closed','closed','','paulo','','','2016-02-19 16:57:27','2016-02-19 16:57:27','',0,'http://wordpress.local/?post_type=email_recebimento&#038;p=12',0,'email_recebimento','',0),(15,1,'2016-01-27 12:27:38','2016-01-27 12:27:38','a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:2:\"17\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:15:{i:0;s:9:\"permalink\";i:1;s:11:\"the_content\";i:2;s:7:\"excerpt\";i:3;s:13:\"custom_fields\";i:4;s:10:\"discussion\";i:5;s:8:\"comments\";i:6;s:9:\"revisions\";i:7;s:4:\"slug\";i:8;s:6:\"author\";i:9;s:6:\"format\";i:10;s:15:\"page_attributes\";i:11;s:14:\"featured_image\";i:12;s:10:\"categories\";i:13;s:4:\"tags\";i:14;s:15:\"send-trackbacks\";}s:11:\"description\";s:0:\"\";}','Configurar','configurar','publish','closed','closed','','group_56a8b792ebbfe','','','2016-01-27 12:30:23','2016-01-27 12:30:23','',0,'http://wordpress.local/?post_type=acf-field-group&#038;p=15',0,'acf-field-group','',0),(16,1,'2016-01-27 12:27:38','2016-01-27 12:27:38','a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}','Facebook','facebook','publish','closed','closed','','field_56a8b79f7a3ff','','','2016-01-27 12:27:38','2016-01-27 12:27:38','',15,'http://wordpress.local/?post_type=acf-field&p=16',0,'acf-field','',0),(17,1,'2016-01-27 12:27:56','2016-01-27 12:27:56','','Configurar','','publish','closed','closed','','configurar','','','2016-01-27 12:27:56','2016-01-27 12:27:56','',0,'http://wordpress.local/?page_id=17',0,'page','',0),(18,1,'2016-01-27 12:27:56','2016-01-27 12:27:56','','Configurar','','inherit','closed','closed','','17-revision-v1','','','2016-01-27 12:27:56','2016-01-27 12:27:56','',17,'http://wordpress.local/index.php/2016/01/27/17-revision-v1/',0,'revision','',0);
/*!40000 ALTER TABLE `wp_posts` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-27 14:53:55
