-- MySQL dump 10.13  Distrib 5.7.9, for osx10.9 (x86_64)
--
-- Host: localhost    Database: portalgynleste
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `coluna_fotos`
--

DROP TABLE IF EXISTS `coluna_fotos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coluna_fotos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coluna_id` int(11) NOT NULL,
  `img` varchar(255) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `produto_id` (`coluna_id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coluna_fotos`
--

LOCK TABLES `coluna_fotos` WRITE;
/*!40000 ALTER TABLE `coluna_fotos` DISABLE KEYS */;
INSERT INTO `coluna_fotos` VALUES (2,6,'arq_coluna/6/73698e4fcdf7127a0a08eaecd9a28317.jpg'),(3,6,'arq_coluna/6/fa11ef2b296d47bb5942870f1ff4ff86.jpg'),(4,6,'arq_coluna/6/b0ebc924d47c1d2c7e23d17a838a860d.jpg'),(5,6,'arq_coluna/6/574fe40887abd66ce2b1561244cedae8.jpg'),(6,6,'arq_coluna/6/34b2805eb672d2b2abbb6751f1777af2.jpg'),(7,6,'arq_coluna/6/d0ae9de5237f4c0defa7fd15d0cd9fcb.jpg'),(8,6,'arq_coluna/6/23fb10575d0693aef9d01293e6546059.jpg'),(9,6,'arq_coluna/6/f6be662727705d063d28a6d40edda2e0.jpg'),(10,6,'arq_coluna/6/f307815dc237ded62e02e5ec727f58c9.jpg'),(11,0,'arq_coluna//73698e4fcdf7127a0a08eaecd9a28317.jpg'),(12,0,'arq_coluna//fa11ef2b296d47bb5942870f1ff4ff86.jpg'),(13,0,'arq_coluna//b0ebc924d47c1d2c7e23d17a838a860d.jpg'),(14,0,'arq_coluna//574fe40887abd66ce2b1561244cedae8.jpg'),(15,0,'arq_coluna//34b2805eb672d2b2abbb6751f1777af2.jpg'),(16,0,'arq_coluna//d0ae9de5237f4c0defa7fd15d0cd9fcb.jpg'),(17,0,'arq_coluna//23fb10575d0693aef9d01293e6546059.jpg'),(18,0,'arq_coluna//f6be662727705d063d28a6d40edda2e0.jpg'),(19,0,'arq_coluna//f307815dc237ded62e02e5ec727f58c9.jpg'),(20,2,'arq_coluna/2/b1d057137d38f7a139ce4cb61766e72c.jpg'),(21,2,'arq_coluna/2/879f333c249ca21a413ada264089c86a.jpg'),(22,0,'arq_coluna//b1d057137d38f7a139ce4cb61766e72c.jpg'),(23,0,'arq_coluna//879f333c249ca21a413ada264089c86a.jpg'),(24,8,'arq_coluna/8/05b9e13622fddb8ab361750eb50bb60c.jpg'),(25,8,'arq_coluna/8/5fedcbed9356d46f5475788c95b2863d.jpg'),(26,8,'arq_coluna/8/05ca70854a8ffdadfbab04fff4df9f0f.jpg'),(27,8,'arq_coluna/8/178834e7e390f09a188b13052b25603c.jpg'),(28,8,'arq_coluna/8/e28770b3cbe3f9fff3f9546a1824007b.jpg'),(29,8,'arq_coluna/8/aa99db2b7b1d26cb8d6d18b7ff2026e8.jpg'),(30,8,'arq_coluna/8/93bc964b7ffced6ee5ac4e53d4eb670c.jpg'),(35,0,'arq_coluna//4495eda8377423bd6be398bc2ec4ae3c.jpg'),(36,0,'arq_coluna//dd56aeed63b2596b16a724b2f85cdcf2.jpg'),(37,0,'arq_coluna//e6fdee75111b2a837464d10ab5e720cd.jpg'),(38,0,'arq_coluna//7732f85f300df44aa84a2edda1bc28c8.jpg'),(39,0,'arq_coluna//d94e5b0e11d7574d83f205c2b8c238b0.jpg'),(40,11,'arq_coluna/11/4495eda8377423bd6be398bc2ec4ae3c.jpg'),(41,11,'arq_coluna/11/36eb3e6558d741deb0c3e57cc43b31c9.jpg'),(42,11,'arq_coluna/11/4a18318d71a26a0d148ea1d7bec89011.jpg'),(43,0,'arq_coluna//616460f70d5d781500d36ba19390a6b9.jpg'),(44,0,'arq_coluna//4495eda8377423bd6be398bc2ec4ae3c.jpg'),(45,0,'arq_coluna//36eb3e6558d741deb0c3e57cc43b31c9.jpg'),(46,0,'arq_coluna//4a18318d71a26a0d148ea1d7bec89011.jpg'),(47,12,'arq_coluna/12/6fc9572d6e1628572628e547f807b27d.jpg'),(48,12,'arq_coluna/12/c81eb20fc61dae6e26a64b1b49aabb01.jpg'),(49,12,'arq_coluna/12/b0ebc924d47c1d2c7e23d17a838a860d.jpg'),(50,12,'arq_coluna/12/574fe40887abd66ce2b1561244cedae8.jpg'),(51,12,'arq_coluna/12/879f333c249ca21a413ada264089c86a.jpg'),(52,12,'arq_coluna/12/ac96ab75b4e334d55b1f4c21e5c787d3.jpg'),(53,12,'arq_coluna/12/6950e720e3058feb66d106387a73cbfc.jpg'),(54,12,'arq_coluna/12/55ee97d7017f1bdae9afbc80baf9024e.jpg'),(55,12,'arq_coluna/12/e25125359b3054114ae2b29ba2192e57.jpg'),(56,12,'arq_coluna/12/0bc1103eb9233768a76eae6a3a10818a.jpg'),(57,13,'arq_coluna/13/b9ca41c529c82a6116ecf029f4d688d7.jpg'),(58,13,'arq_coluna/13/233c7f53967aac7e86307d360b5a27ea.jpg'),(59,13,'arq_coluna/13/12c08d1c68eb6b8eae280bd8d8080472.jpg'),(60,13,'arq_coluna/13/c6e08fc06ce6090560de430e01c9882c.jpg'),(61,13,'arq_coluna/13/8b614ab5a37d288f806e206ad6f2b49c.jpg'),(62,13,'arq_coluna/13/9e2b690581b36fca153d360bf3940781.jpg');
/*!40000 ALTER TABLE `coluna_fotos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-27 14:43:48
