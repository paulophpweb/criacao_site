-- MySQL dump 10.13  Distrib 5.7.9, for osx10.9 (x86_64)
--
-- Host: localhost    Database: portalgynleste
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categoria`
--

DROP TABLE IF EXISTS `categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria`
--

LOCK TABLES `categoria` WRITE;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` VALUES (1,'Supermecado',1),(2,'Auto Mecânia',1),(3,'Academia',1),(4,'Açougues',1),(5,'Advogados/Contabilidade',1),(6,'Auto Escola',1),(7,'Bancos',1),(8,'Bazar',1),(9,'Bar/Choperia',1),(10,'Boates',1),(11,'Calcário/Marmoraria',1),(12,'Clínica Médica/Laboratório',1),(13,'Corretora de Seguros',1),(14,'Cosméticos/Perfumaria',1),(15,'Odontologia',1),(16,'Depósito de Gás',1),(17,'Distribuidora de Bebidas',1),(18,'Eletricista/Encanador',1),(19,'Eletrônica',1),(20,'Eventos',1),(21,'Farmácia/Drogaria',1),(22,'Gráfica/Serigrafia',1),(23,'Igreja',1),(24,'Imobiliária',1),(25,'Informática e Ass. Técnica',1),(26,'Instituição de Ensino',1),(27,'Locadoras',1),(28,'Loja de Pneus/Borracharia',1),(29,'Madereira',1),(30,'Marcenarias',1),(31,'Marmoraria',1),(32,'Materiais Agropecuários',1),(33,'Materiais de Construção',1),(34,'Moda/Roupas',1),(35,'Motel',1),(36,'Móveis e Eletrodomésticos',1),(37,'Ótica e Relojoaria',1),(38,'Panificadora/Padaria',1),(39,'Papelaria',1),(40,'Pastelarias',1),(41,'Pesque Pague/ Camping',1),(42,'Pet Shop/Veterinário',1),(43,'Pit Dog / Sanduicheria / Lanchonete',1),(44,'Pizzarias',1),(45,'Posto de Gasolina',1),(46,'Pregão',1),(47,'Restaurantes',1),(48,'Salão de Beleza',1),(49,'Som e Acessórios',1),(50,'Moto Táxi',1),(51,'Vidraçaria ',1),(52,'Estética ',1),(53,'Podólogo',1),(54,'Serralheria',1),(55,'Aviamentos',1),(56,'Enxovais',1),(57,'Banca de Revista',1),(58,'Loja de Calçados',1),(59,'Ferragista',1),(60,'Lan House',1),(61,'Auto Peças e Serviços',1),(62,'Vendas de Carros',1),(63,'Loja de Bolos',1),(64,'Prestador de Serviços',1),(65,'Berçário',1),(66,'Casa de Embalagens',1),(67,'Suplementos Alimentares',1),(68,'Pamonharia',1),(69,'Chaveiro',1),(70,'Lava jato',1),(71,'Torneadora',1),(72,'Frios e Espetinhos',1),(73,'Colégios/Escolas',1),(74,'Telefones Úteis',1),(75,'Escritório de Contabilidade ',1);
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-27 14:43:44
