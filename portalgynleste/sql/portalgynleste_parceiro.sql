-- MySQL dump 10.13  Distrib 5.7.9, for osx10.9 (x86_64)
--
-- Host: localhost    Database: portalgynleste
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `parceiro`
--

DROP TABLE IF EXISTS `parceiro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parceiro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destaque` int(11) NOT NULL,
  `local` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `nome` varchar(255) CHARACTER SET latin1 NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) CHARACTER SET latin1 NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parceiro`
--

LOCK TABLES `parceiro` WRITE;
/*!40000 ALTER TABLE `parceiro` DISABLE KEYS */;
INSERT INTO `parceiro` VALUES (10,1,'lateral','califórnia','https://www.facebook.com/californiasteakhouse?fref=ts','arq_parceiro/parceiro_12042014105636.png',0),(12,1,'central','Bicho Solto','','arq_parceiro/14042014081654.png',0),(13,1,'central','Donna Xic','https://www.facebook.com/DonnaXc?fref=ts','arq_parceiro/parceiro_13042014101605.png',0),(15,1,'topo','Anuncio Topo','','arq_parceiro/parceiro_14042014024818.png',0),(22,1,'lateral','Anuncio Lateral 2','','arq_parceiro/parceiro_14042014082100.png',0),(24,1,'central','Beauty e Beauty','https://www.facebook.com/pages/Beauty-Beauty/268332819874766','arq_parceiro/06102014101105.jpg',0),(25,1,'lateral','Supermercado Xavier','','arq_parceiro/parceiro_15042014025406.png',0),(26,1,'lateral','Supermercado Xavier','','arq_parceiro/parceiro_15042014033339.png',1),(27,1,'central','Leão Assesoria','http://www.leaoassessoria.com.br/','arq_parceiro/parceiro_26042014022907.png',0),(28,1,'central','Cruvinel Vidros Box','','arq_parceiro/parceiro_26042014023038.png',0),(29,1,'central','Alfhaville Centro Automotivo','','arq_parceiro/parceiro_12052014044823.png',0),(30,1,'topo','Promoção ','https://www.facebook.com/pages/Portal-Gyn-Leste/254373418043682?fref=ts','arq_parceiro/parceiro_13052014091803.png',0),(32,1,'central','Creative Publicidade Gráfica ','https://www.facebook.com/creativegyn?fref=ts','arq_parceiro/parceiro_24062014075946.png',0),(34,1,'central','Banner 2','','arq_parceiro/parceiro_05052015023623.jpg',1),(35,1,'central','Banner 3','','arq_parceiro/parceiro_05052015023701.jpg',1),(36,1,'central','Banner 4','','arq_parceiro/parceiro_05052015023853.jpg',1),(37,1,'lateral','Banner Lateral 1','','arq_parceiro/parceiro_05052015023925.jpg',0),(38,1,'lateral','Banner Lateral 2','','arq_parceiro/05052015024134.jpg',0),(39,1,'lateral','Creative Publicidade Gráfica ','https://www.facebook.com/creativegyn?fref=ts','arq_parceiro/parceiro_30072015050413.png',1),(40,1,'topo','Festival Louvai','https://www.facebook.com/juliolemes?fref=ts','arq_parceiro/parceiro_16022016120611.jpg',1),(41,1,'central','Banner divulgação','','arq_parceiro/16032016020402.jpg',1),(45,1,'popup','Banner Promocional','http://pauloph.com.br','arq_parceiro/15032016014250.jpeg',1);
/*!40000 ALTER TABLE `parceiro` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-27 14:43:45
