-- MySQL dump 10.13  Distrib 5.7.9, for osx10.9 (x86_64)
--
-- Host: localhost    Database: portalgynleste
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `secao_fixa_menu`
--

DROP TABLE IF EXISTS `secao_fixa_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `secao_fixa_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_secao_fixa` int(11) NOT NULL,
  `titulo` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `url` varchar(300) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_secao_fixa` (`id_secao_fixa`),
  CONSTRAINT `secao_fixa_menu_ibfk_1` FOREIGN KEY (`id_secao_fixa`) REFERENCES `secao_fixa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `secao_fixa_menu`
--

LOCK TABLES `secao_fixa_menu` WRITE;
/*!40000 ALTER TABLE `secao_fixa_menu` DISABLE KEYS */;
INSERT INTO `secao_fixa_menu` VALUES (2,1,'Cadastrar','index.php?acao=frmCadUsuario&ctrl=usuarios'),(3,1,'Listar','index.php?acao=listaUsuarios&ctrl=usuarios'),(4,2,'Cadastrar','index.php?acao=frmCad&ctrl=noticia'),(5,2,'Listar','index.php?acao=listar&ctrl=noticia'),(6,3,'Cadastrar','index.php?acao=frmCad&ctrl=parceiro'),(7,3,'Listar','index.php?acao=listar&ctrl=parceiro'),(8,4,'Cadastrar','index.php?acao=frmCad&ctrl=enquete'),(9,4,'Listar','index.php?acao=listar&ctrl=enquete'),(10,5,'Cadastrar','index.php?acao=frmCad&ctrl=coluna'),(11,5,'Listar','index.php?acao=listar&ctrl=coluna'),(12,6,'Cadastrar','index.php?acao=frmCad&ctrl=artigo'),(13,6,'Listar','index.php?acao=listar&ctrl=artigo'),(14,7,'Cadastrar','index.php?acao=frmCad&ctrl=categoria'),(15,7,'Listar','index.php?acao=listar&ctrl=categoria'),(16,8,'Cadastrar','index.php?acao=frmCad&ctrl=guia'),(17,8,'Listar','index.php?acao=listar&ctrl=guia'),(18,9,'Cadastrar','index.php?acao=frmCad&ctrl=tv'),(19,9,'Listar','index.php?acao=listar&ctrl=tv');
/*!40000 ALTER TABLE `secao_fixa_menu` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-27 14:43:47
