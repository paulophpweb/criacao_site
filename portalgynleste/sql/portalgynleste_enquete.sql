-- MySQL dump 10.13  Distrib 5.7.9, for osx10.9 (x86_64)
--
-- Host: localhost    Database: portalgynleste
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `enquete`
--

DROP TABLE IF EXISTS `enquete`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enquete` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pergunta` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enquete`
--

LOCK TABLES `enquete` WRITE;
/*!40000 ALTER TABLE `enquete` DISABLE KEYS */;
INSERT INTO `enquete` VALUES (7,'O que você achou do site?',0),(8,'Você é a favor ou contra a volta do transporte alternativo?',0),(9,'Você acha que a Seleção Brasileira vence o Chile?',0),(10,'Você acha que a Seleção Brasileira vence a Colômbia? ',0),(11,'Você acha que a Seleção Brasileira vence a Alemanha?',0),(12,'Você acha que a Seleção Brasileira vence a Holanda?',0),(13,'Você gostou da volta de Dunga para a Seleção Brasileira',0),(14,'Você já escolheu seus candidatos para as próximas eleições ?',0),(15,'Se a eleição fosse hoje em quem você votaria ?',0),(16,'Em quem você votará no 2º turno para GOVERNADOR de Goiás ?',0),(17,'Você é a favor ou contra o aumento de 57,8% do IPTU?',0),(18,'O que você deseja em 2015',0),(19,'Você é a favor ou contra o reajuste de R$ 0,50 na passagem de ônibus',0),(20,'Fim do horário de verão você gostou ou não?',0),(21,'Qual clube será campeão Goiano 2015?',0),(22,'O que a Região Leste necessita?',0),(23,'Você é a favor ou contra o projeto que cria \'bolsa arma\' para a população?',0),(24,'Você é a favor ou contra a redução da maioridade penal?',0),(25,'Você concorda com a reserva de 20% das vagas nos concurso públicos federais para candidatos negros? Como no caso do Concurso do Hospital das Clínicas.',0),(26,'Você é a favor da volta da Contribuição Provisória sobre Movimentação Financeira (CPMF)?',0),(27,'Você é a favor ou contra o Horário de Verão?',0),(28,'O que você deseja em 2016',0),(29,'Você está feliz com o fim do horário de verão?',1);
/*!40000 ALTER TABLE `enquete` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-27 14:43:46
