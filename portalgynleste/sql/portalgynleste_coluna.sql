-- MySQL dump 10.13  Distrib 5.7.9, for osx10.9 (x86_64)
--
-- Host: localhost    Database: portalgynleste
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `coluna`
--

DROP TABLE IF EXISTS `coluna`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coluna` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) CHARACTER SET latin1 NOT NULL,
  `data` date NOT NULL,
  `texto` text CHARACTER SET latin1 NOT NULL,
  `foto_principal` varchar(255) CHARACTER SET latin1 NOT NULL,
  `destaque` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coluna`
--

LOCK TABLES `coluna` WRITE;
/*!40000 ALTER TABLE `coluna` DISABLE KEYS */;
INSERT INTO `coluna` VALUES (2,'FESTA AO IMACULADO CORAÇÃO DE MARIA - PARQUE SANTA MARIA ','2014-06-22','&nbsp;&nbsp;&nbsp; &nbsp;','arq_coluna/coluna_23062014094806.jpg',1,1),(6,'COPA DO MUNDO 2014: BRASIL X CAMARÕES','2014-06-23','','arq_coluna/coluna_23062014055140.jpg',1,1),(8,'COPA DO MUNDO 2014: BRASIL X ALEMANHA','2014-07-08','','arq_coluna/coluna_08072014070701.jpg',1,1),(11,'FOTOS: Veja imagens da Conferência Radicais Livres 2014 ','2014-09-07','Evento gospel reuniu mais de 50 mil fi&eacute;is de todo o pa&iacute;s no Est&aacute;dio Serra Dourada, em Goi&acirc;nia','arq_coluna/coluna_07092014105749.jpg',1,0),(12,'JOGO DA SAIA 2014','2014-12-21','','arq_coluna/coluna_21122014031547.jpg',1,1),(13,'JOGO DA SAIA 2014','2014-12-21','','arq_coluna/coluna_21122014031834.jpg',1,1);
/*!40000 ALTER TABLE `coluna` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-27 14:43:52
