-- MySQL dump 10.13  Distrib 5.7.9, for osx10.9 (x86_64)
--
-- Host: localhost    Database: portalgynleste
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `enquete_resposta`
--

DROP TABLE IF EXISTS `enquete_resposta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enquete_resposta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enquete_id` int(11) NOT NULL,
  `resposta` varchar(255) NOT NULL,
  `votos` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `enquete_id` (`enquete_id`),
  CONSTRAINT `enquete_resposta_ibfk_1` FOREIGN KEY (`enquete_id`) REFERENCES `enquete` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enquete_resposta`
--

LOCK TABLES `enquete_resposta` WRITE;
/*!40000 ALTER TABLE `enquete_resposta` DISABLE KEYS */;
INSERT INTO `enquete_resposta` VALUES (14,7,'Ótimo',75),(15,7,'Bom',9),(16,7,'Regular',5),(17,7,'Ruim',4),(18,8,'A favor',90),(19,8,'Contra',16),(20,8,'Não tem opinião',2),(21,9,'Sim',15),(22,9,'Não',5),(23,10,'Sim',23),(24,10,'Não',4),(25,11,'Sim',9),(26,11,'Não',1),(27,12,'Sim',3),(28,12,'Não',4),(29,13,'Sim',8),(30,13,'Não',38),(31,14,'Sim, para todos os cargos',9),(32,14,'Não, nenhum ainda',6),(33,14,'Escolhi parcialmente',13),(34,15,'(13) Antônio Gomide',5),(35,15,'(15) Íris Rezende',28),(36,15,'(21) Marta Jane',3),(37,15,'(27) Alexandre Magalhães',0),(38,15,'(40) Vanderlan Cardoso',27),(39,15,'(45) Marconi Perillo',26),(40,15,'(50) Prof Weslei Garcia',1),(41,15,'(00) Branco ou Nulo',4),(42,16,'(15) Íris Rezende',69),(43,16,'(45) Marconi Perillo',42),(44,16,'(00) Nulo/Branco',12),(45,17,'Contra',55),(46,17,'A favor',3),(47,18,'Encontrar um novo amor',4),(48,18,'Dar uma guinada na vida profissional',16),(49,18,'Viajar mais',13),(50,18,'Perder os quilos extras',9),(51,18,'Ter mais saúde',4),(52,18,'Não sabe',2),(53,19,'Favor',0),(54,19,'Contra',5),(55,20,'Gostei',36),(56,20,'Não Gostei',11),(57,20,'Não tem opinião',0),(58,21,'Associação Atlética Aparecidense',4),(59,21,'Goiás Esporte Clube',7),(60,22,'Banco',80),(61,22,'Vapt vupt',21),(62,22,'Batalhão/Companhia Militar',80),(63,22,'Cepal',9),(64,22,'Outras opções',3),(65,23,'A Favor',15),(66,23,'Contra',18),(67,23,'Não tem opinião formada',1),(68,24,'A favor',78),(69,24,'Contra',13),(70,24,'Não tem opinião formada',0),(71,25,'Sim',35),(72,25,'Não',64),(73,25,'Não tenho opinião formada',7),(74,26,'Sim',12),(75,26,'Não',62),(76,27,'Sim! sou a favor',22),(77,27,'Não! sou contra',90),(78,27,'Pra mim tanto faz',21),(79,28,'Encontrar um novo amor',8),(80,28,'Dar uma guinada na vida profissional',14),(81,28,'Viajar mais',10),(82,28,'Perder os quilos extras',10),(83,28,'Ter mais saúde',9),(84,28,'Não sabe',0),(85,29,'Sim, pois detesto este horário de verão',41),(86,29,'Não, pois adoro o horário de verão',4),(87,29,'Não tenho opnião',1);
/*!40000 ALTER TABLE `enquete_resposta` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-27 14:43:46
