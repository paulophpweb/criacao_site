-- MySQL dump 10.13  Distrib 5.7.9, for osx10.9 (x86_64)
--
-- Host: localhost    Database: isnara
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `permissao_secao_fixa`
--

DROP TABLE IF EXISTS `permissao_secao_fixa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissao_secao_fixa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `secao_fixa_id` int(11) NOT NULL DEFAULT '0',
  `usuarios_id` int(11) NOT NULL,
  `cadastrar` int(1) NOT NULL DEFAULT '0',
  `alterar` int(1) NOT NULL DEFAULT '0',
  `excluir` int(1) NOT NULL DEFAULT '0',
  `publicar` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `permissao_secao_fixa_FKIndex1` (`usuarios_id`),
  KEY `permissao_secao_fixa_FKIndex2` (`secao_fixa_id`),
  CONSTRAINT `permissao_secao_fixa_ibfk_1` FOREIGN KEY (`secao_fixa_id`) REFERENCES `secao_fixa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissao_secao_fixa`
--

LOCK TABLES `permissao_secao_fixa` WRITE;
/*!40000 ALTER TABLE `permissao_secao_fixa` DISABLE KEYS */;
INSERT INTO `permissao_secao_fixa` VALUES (35,1,1,1,1,1,1),(36,3,1,1,1,1,1),(38,5,1,1,1,1,1),(39,6,1,1,1,1,1),(53,1,3,1,1,1,1),(54,3,3,1,1,1,1),(56,5,3,1,1,1,1),(57,6,3,1,1,1,1),(71,1,2,1,1,1,1),(72,3,2,1,1,1,1),(74,5,2,1,1,1,1),(75,6,2,1,1,1,1),(77,1,4,1,1,1,1),(78,3,4,1,1,1,1),(80,5,4,1,1,1,1),(81,6,4,1,1,1,1);
/*!40000 ALTER TABLE `permissao_secao_fixa` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-27 14:20:20
