-- MySQL dump 10.13  Distrib 5.7.9, for osx10.9 (x86_64)
--
-- Host: localhost    Database: isnara
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `secao_fixa_menu`
--

DROP TABLE IF EXISTS `secao_fixa_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `secao_fixa_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `secao_fixa_id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `url` varchar(300) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_secao_fixa` (`secao_fixa_id`),
  CONSTRAINT `secao_fixa_menu_ibfk_1` FOREIGN KEY (`secao_fixa_id`) REFERENCES `secao_fixa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `secao_fixa_menu`
--

LOCK TABLES `secao_fixa_menu` WRITE;
/*!40000 ALTER TABLE `secao_fixa_menu` DISABLE KEYS */;
INSERT INTO `secao_fixa_menu` VALUES (8,1,'Cadastrar','index.php?acao=frmCadUsuario&ctrl=usuarios'),(9,1,'Listar','index.php?acao=listaUsuarios&ctrl=usuarios'),(14,3,'Cadastrar','index.php?acao=frmCad&ctrl=banner'),(15,3,'Listar','index.php?acao=listar&ctrl=banner'),(20,5,'Cadastrar','index.php?acao=frmCad&ctrl=categoria'),(21,5,'Listar','index.php?acao=listar&ctrl=categoria'),(22,6,'Cadastrar','index.php?acao=frmCad&ctrl=produto'),(23,6,'Listar','index.php?acao=listar&ctrl=produto');
/*!40000 ALTER TABLE `secao_fixa_menu` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-27 14:20:20
