-- MySQL dump 10.13  Distrib 5.7.9, for osx10.9 (x86_64)
--
-- Host: localhost    Database: isnara
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `produto`
--

DROP TABLE IF EXISTS `produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `imagem` varchar(255) NOT NULL,
  `video` varchar(400) NOT NULL,
  `texto` text NOT NULL,
  `destaque` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produto`
--

LOCK TABLES `produto` WRITE;
/*!40000 ALTER TABLE `produto` DISABLE KEYS */;
INSERT INTO `produto` VALUES (7,'Poltronas Azul','arq_produto/produto_1446984796.jpg','','Com um estilo retr&ocirc; e, ao mesmo tempo, super moderno, esta poltrona decorativa &eacute; perfeita para voc&ecirc; que busca conforto e beleza em um m&oacute;vel.<br />\r\n<br />\r\nSeu estofado &eacute; azul turquesa, &nbsp;cor que garante sofistica&ccedil;&atilde;o ao ambiente e ao m&oacute;vel. Al&eacute;m de contar com p&eacute;s palitos produzidos em eucalipto, detalhe que ser&aacute; perfeito para deixar a sua casa ainda mais bonita e com um toque vintage.<br />\r\n<br />\r\nPoltrona Azul Turquesa Sereia &eacute; ideal para voc&ecirc;, e para a sua montagem, basta fixar os seus p&eacute;s com uma chave Philips. F&aacute;cil, n&atilde;o &eacute;?! Combine esta linda pe&ccedil;a com algumas de nossas camas de casal, camas de solteiro ou c&ocirc;modas e deixe o seu quarto de casal ou quarto de solteiro, mais aconchegante e charmoso.',1,1),(8,'Poltrona Rosa','arq_produto/produto_1446986699.jpg','','Com um estilo retr&ocirc; e, ao mesmo tempo, super moderno, esta poltrona decorativa &eacute; perfeita para voc&ecirc; que busca conforto e beleza em um m&oacute;vel.<br />\r\n<br />\r\nSeu estofado &eacute; azul turquesa, &nbsp;cor que garante sofistica&ccedil;&atilde;o ao ambiente e ao m&oacute;vel. Al&eacute;m de contar com p&eacute;s palitos produzidos em eucalipto, detalhe que ser&aacute; perfeito para deixar a sua casa ainda mais bonita e com um toque vintage.<br />\r\n<br />\r\nPoltrona Azul Turquesa Sereia &eacute; ideal para voc&ecirc;, e para a sua montagem, basta fixar os seus p&eacute;s com uma chave Philips. F&aacute;cil, n&atilde;o &eacute;?! Combine esta linda pe&ccedil;a com algumas de nossas camas de casal, camas de solteiro ou c&ocirc;modas e deixe o seu quarto de casal ou quarto de solteiro, mais aconchegante e charmoso.',1,1),(9,'Poltrona Colorida','arq_produto/produto_1446986771.jpg','','Com um estilo retr&ocirc; e, ao mesmo tempo, super moderno, esta poltrona decorativa &eacute; perfeita para voc&ecirc; que busca conforto e beleza em um m&oacute;vel.<br />\r\n<br />\r\nSeu estofado &eacute; azul turquesa, &nbsp;cor que garante sofistica&ccedil;&atilde;o ao ambiente e ao m&oacute;vel. Al&eacute;m de contar com p&eacute;s palitos produzidos em eucalipto, detalhe que ser&aacute; perfeito para deixar a sua casa ainda mais bonita e com um toque vintage.<br />\r\n<br />\r\nPoltrona Azul Turquesa Sereia &eacute; ideal para voc&ecirc;, e para a sua montagem, basta fixar os seus p&eacute;s com uma chave Philips. F&aacute;cil, n&atilde;o &eacute;?! Combine esta linda pe&ccedil;a com algumas de nossas camas de casal, camas de solteiro ou c&ocirc;modas e deixe o seu quarto de casal ou quarto de solteiro, mais aconchegante e charmoso.',1,1),(10,'Poltrona Branca e amarela','arq_produto/produto_1446986840.jpg','','Com um estilo retr&ocirc; e, ao mesmo tempo, super moderno, esta poltrona decorativa &eacute; perfeita para voc&ecirc; que busca conforto e beleza em um m&oacute;vel.<br />\r\n<br />\r\nSeu estofado &eacute; azul turquesa, &nbsp;cor que garante sofistica&ccedil;&atilde;o ao ambiente e ao m&oacute;vel. Al&eacute;m de contar com p&eacute;s palitos produzidos em eucalipto, detalhe que ser&aacute; perfeito para deixar a sua casa ainda mais bonita e com um toque vintage.<br />\r\n<br />\r\nPoltrona Azul Turquesa Sereia &eacute; ideal para voc&ecirc;, e para a sua montagem, basta fixar os seus p&eacute;s com uma chave Philips. F&aacute;cil, n&atilde;o &eacute;?! Combine esta linda pe&ccedil;a com algumas de nossas camas de casal, camas de solteiro ou c&ocirc;modas e deixe o seu quarto de casal ou quarto de solteiro, mais aconchegante e charmoso.',1,1),(11,'Puff Jeans','arq_produto/produto_1446986937.jpg','','Com um estilo retr&ocirc; e, ao mesmo tempo, super moderno, esta poltrona decorativa &eacute; perfeita para voc&ecirc; que busca conforto e beleza em um m&oacute;vel.<br />\r\n<br />\r\nSeu estofado &eacute; azul turquesa, &nbsp;cor que garante sofistica&ccedil;&atilde;o ao ambiente e ao m&oacute;vel. Al&eacute;m de contar com p&eacute;s palitos produzidos em eucalipto, detalhe que ser&aacute; perfeito para deixar a sua casa ainda mais bonita e com um toque vintage.<br />\r\n<br />\r\nPoltrona Azul Turquesa Sereia &eacute; ideal para voc&ecirc;, e para a sua montagem, basta fixar os seus p&eacute;s com uma chave Philips. F&aacute;cil, n&atilde;o &eacute;?! Combine esta linda pe&ccedil;a com algumas de nossas camas de casal, camas de solteiro ou c&ocirc;modas e deixe o seu quarto de casal ou quarto de solteiro, mais aconchegante e charmoso.',1,1),(12,'Puff Jornal','arq_produto/produto_1446986994.jpg','','Com um estilo retr&ocirc; e, ao mesmo tempo, super moderno, esta poltrona decorativa &eacute; perfeita para voc&ecirc; que busca conforto e beleza em um m&oacute;vel.<br />\r\n<br />\r\nSeu estofado &eacute; azul turquesa, &nbsp;cor que garante sofistica&ccedil;&atilde;o ao ambiente e ao m&oacute;vel. Al&eacute;m de contar com p&eacute;s palitos produzidos em eucalipto, detalhe que ser&aacute; perfeito para deixar a sua casa ainda mais bonita e com um toque vintage.<br />\r\n<br />\r\nPoltrona Azul Turquesa Sereia &eacute; ideal para voc&ecirc;, e para a sua montagem, basta fixar os seus p&eacute;s com uma chave Philips. F&aacute;cil, n&atilde;o &eacute;?! Combine esta linda pe&ccedil;a com algumas de nossas camas de casal, camas de solteiro ou c&ocirc;modas e deixe o seu quarto de casal ou quarto de solteiro, mais aconchegante e charmoso.',1,1);
/*!40000 ALTER TABLE `produto` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-27 14:20:17
