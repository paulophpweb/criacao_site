-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Host: 186.202.152.198
-- Generation Time: Aug 20, 2016 at 02:48 PM
-- Server version: 5.6.30-76.3-log
-- PHP Version: 5.6.23-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `padraoindsite`
--

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `url` varchar(400) NOT NULL,
  `texto` text NOT NULL,
  `img` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `titulo`, `url`, `texto`, `img`, `status`) VALUES
(2, '', '', '', 'arq_banner/banner_20082016022032.jpg', 1),
(3, '', '', '', 'arq_banner/banner_20082016022110.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `data` date NOT NULL,
  `texto` text NOT NULL,
  `img` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `catalogo`
--

CREATE TABLE `catalogo` (
  `id` int(11) NOT NULL,
  `img` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `catalogo`
--

INSERT INTO `catalogo` (`id`, `img`, `status`) VALUES
(1, 'arq_banner/28102014033648.pdf', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cliente`
--

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `url` varchar(400) NOT NULL,
  `texto` text NOT NULL,
  `img` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `configuracao`
--

CREATE TABLE `configuracao` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `tipo` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `configuracao`
--

INSERT INTO `configuracao` (`id`, `nome`, `email`, `tipo`, `status`) VALUES
(3, 'Paulo Henrique', 'pudicaph17@hotmail.com', 'Fale Conosco', 0);

-- --------------------------------------------------------

--
-- Table structure for table `curriculum`
--

CREATE TABLE `curriculum` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `data_de_nascimento` date NOT NULL,
  `estado_civil` varchar(255) NOT NULL,
  `escolaridade` varchar(255) NOT NULL,
  `sexo` varchar(255) NOT NULL,
  `telefone_fixo` varchar(255) NOT NULL,
  `celular` varchar(255) NOT NULL,
  `estado` varchar(255) NOT NULL,
  `cidade` varchar(255) NOT NULL,
  `bairro` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `cep` varchar(255) NOT NULL,
  `experiencia` text NOT NULL,
  `obs` text NOT NULL,
  `img` varchar(255) NOT NULL,
  `arquivo` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `linha`
--

CREATE TABLE `linha` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `texto` text NOT NULL,
  `tipo_linha` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `linha_fotos`
--

CREATE TABLE `linha_fotos` (
  `id` int(11) NOT NULL,
  `id_linha` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `usuario` varchar(100) DEFAULT NULL,
  `area` varchar(50) DEFAULT NULL,
  `id_area` int(11) DEFAULT NULL,
  `acao` varchar(50) DEFAULT NULL,
  `datahora` datetime DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `usuario`, `area`, `id_area`, `acao`, `datahora`, `ip`) VALUES
(1, 'Administrador fe', 'Configuração', 1, 'Deletou', '2012-10-04 08:19:51', '127.0.0.1'),
(2, 'Administrador fe', 'Configuração', 1, 'Alterou', '2012-10-04 08:20:04', '127.0.0.1'),
(3, 'Administrador fe', 'Configuracao', 3, 'desativou', '2012-10-04 08:31:31', '127.0.0.1'),
(4, 'Administrador fe', 'Configuração', 1, 'Publicou', '2012-10-04 08:31:31', '127.0.0.1'),
(5, 'Administrador fe', 'Configuracao', 3, 'ativou', '2012-10-04 08:34:15', '127.0.0.1'),
(6, 'Administrador fe', 'Configuração', 1, 'Publicou', '2012-10-04 08:34:15', '127.0.0.1'),
(7, 'Administrador fe', 'Configuracao', 3, 'desativou', '2012-10-04 08:34:44', '127.0.0.1'),
(8, 'Administrador fe', 'Configuração', 1, 'Publicou', '2012-10-04 08:34:44', '127.0.0.1'),
(9, 'Administrador fe', 'Configuração', 1, 'Cadastrou', '2012-10-04 08:37:44', '127.0.0.1'),
(10, 'Administrador fe', 'Configuração', 1, 'Cadastrou', '2012-10-04 08:38:30', '127.0.0.1'),
(11, 'Administrador fe', 'Configuração', 1, 'Cadastrou', '2012-10-04 08:42:51', '127.0.0.1'),
(12, 'Administrador fe', 'Configuração', 1, 'Cadastrou', '2012-10-04 08:43:52', '127.0.0.1'),
(13, 'Administrador fe', 'Configuração', 1, 'Cadastrou', '2012-10-04 08:44:14', '127.0.0.1'),
(14, 'Administrador fe', 'Configuração', 1, 'Cadastrou', '2012-10-04 08:45:43', '127.0.0.1'),
(15, 'Administrador fe', 'Configuração', 1, 'Cadastrou', '2012-10-04 08:46:26', '127.0.0.1'),
(16, 'Administrador fe', 'Configuração', 1, 'Cadastrou', '2012-10-04 08:47:09', '127.0.0.1'),
(17, 'Administrador fe', 'Configuração', 1, 'Cadastrou', '2012-10-04 08:48:36', '127.0.0.1'),
(18, 'Administrador fe', 'Configuração', 1, 'Cadastrou', '2012-10-04 08:49:07', '127.0.0.1'),
(19, 'Administrador fe', 'Configuração', 1, 'Cadastrou', '2012-10-04 08:50:57', '127.0.0.1'),
(20, 'Administrador fe', 'Configuração', 1, 'Cadastrou', '2012-10-04 08:52:25', '127.0.0.1'),
(21, 'Administrador fe', 'Configuração', 1, 'Cadastrou', '2012-10-04 08:53:18', '127.0.0.1'),
(22, 'Administrador fe', 'Configuração', 1, 'Deletou', '2012-10-04 09:14:38', '127.0.0.1'),
(23, 'Administrador fe', 'Configuração', 1, 'Deletou', '2012-10-04 09:14:48', '127.0.0.1'),
(24, 'Administrador fe', 'Configuração', 1, 'Deletou', '2012-10-04 09:14:57', '127.0.0.1'),
(25, 'Administrador fe', 'Configuração', 1, 'Deletou', '2012-10-04 09:15:30', '127.0.0.1'),
(26, 'Administrador fe', 'Configuração', 1, 'Deletou', '2012-10-04 09:15:39', '127.0.0.1'),
(27, 'Administrador fe', 'Configuração', 1, 'Deletou', '2012-10-04 09:15:48', '127.0.0.1'),
(28, 'Administrador fe', 'Configuração', 1, 'Deletou', '2012-10-04 09:16:15', '127.0.0.1'),
(29, 'Administrador fe', 'Configuração', 1, 'Deletou', '2012-10-04 09:16:24', '127.0.0.1'),
(30, 'Administrador fe', 'Configuração', 1, 'Deletou', '2012-10-04 09:16:32', '127.0.0.1'),
(31, 'Administrador fe', 'Configuração', 1, 'Deletou', '2012-10-04 09:16:40', '127.0.0.1'),
(32, 'Administrador fe', 'Configuração', 1, 'Deletou', '2012-10-04 09:16:47', '127.0.0.1'),
(33, 'Administrador fe', 'Configuração', 1, 'Deletou', '2012-10-04 09:16:54', '127.0.0.1'),
(34, 'Administrador fe', 'Configuração', 1, 'Deletou', '2012-10-04 09:17:03', '127.0.0.1'),
(35, 'Administrador fe', 'Configuração', 1, 'Publicou', '2012-10-04 09:20:50', '127.0.0.1'),
(36, 'Administrador fe', 'Configuração', 1, 'Publicou', '2012-10-04 09:20:59', '127.0.0.1'),
(37, 'Administrador fe', 'Configuração', 1, 'Publicou', '2012-10-04 09:21:14', '127.0.0.1'),
(38, 'Administrador fe', 'Configuração', 1, 'Publicou', '2012-10-04 09:22:14', '127.0.0.1'),
(39, 'Administrador fe', 'Configuração', 1, 'Publicou', '2012-10-04 09:22:21', '127.0.0.1'),
(40, 'Administrador fe', 'Configuração', 1, 'Publicou', '2012-10-04 09:22:38', '127.0.0.1'),
(41, 'Administrador fe', 'Configuração', 1, 'Publicou', '2012-10-04 09:22:45', '127.0.0.1'),
(42, 'Administrador fe', 'Configuração', 1, 'Publicou', '2012-10-04 09:22:54', '127.0.0.1'),
(43, 'Administrador fe', 'Configuração', 1, 'Publicou', '2012-10-04 09:23:23', '127.0.0.1'),
(44, 'Administrador fe', 'Configuração', 1, 'Publicou', '2012-10-04 09:23:30', '127.0.0.1'),
(45, 'Administrador fe', 'Configuração', 1, 'Alterou', '2012-10-04 09:24:15', '127.0.0.1'),
(46, 'Administrador fe', 'Configuração', 1, 'Alterou', '2012-10-04 09:24:26', '127.0.0.1'),
(47, 'Administrador fe', 'Configuração', 1, 'Alterou', '2012-10-04 09:24:48', '127.0.0.1'),
(48, 'Administrador fe', 'Configuração', 1, 'Alterou', '2012-10-04 09:25:14', '127.0.0.1'),
(49, 'Administrador fe', 'Configuração', 1, 'Alterou', '2012-10-04 09:26:06', '127.0.0.1'),
(50, 'Administrador fe', 'Configuração', 1, 'Alterou', '2012-10-04 09:27:57', '127.0.0.1'),
(51, 'Administrador fe', 'Configuração', 1, 'Alterou', '2012-10-04 09:28:00', '127.0.0.1'),
(52, 'Administrador fe', 'Configuração', 1, 'Alterou', '2012-10-04 09:28:31', '127.0.0.1'),
(53, 'Administrador fe', 'Configuração', 1, 'Alterou', '2012-10-04 09:29:10', '127.0.0.1'),
(54, 'Administrador fe', 'Configuração', 1, 'Alterou', '2012-10-04 09:29:17', '127.0.0.1'),
(55, 'Administrador fe', 'Configuração', 1, 'Alterou', '2012-10-04 09:29:39', '127.0.0.1'),
(56, 'Administrador fe', 'Configuração', 1, 'Alterou', '2012-10-04 09:30:26', '127.0.0.1'),
(57, 'Administrador fe', 'Configuração', 1, 'Alterou', '2012-10-04 09:30:37', '127.0.0.1'),
(58, 'Administrador fe', 'Configuração', 1, 'Publicou', '2012-10-04 09:33:07', '127.0.0.1'),
(59, 'Administrador fe', 'usuários', 1, 'alterou senha', '2012-10-04 09:47:32', '127.0.0.1'),
(60, 'Administrador fe', 'usuários', 1, 'alterou', '2012-10-04 09:47:32', '127.0.0.1'),
(61, 'Administrador', 'usuários', 1, 'alterou senha', '2012-10-04 09:48:13', '127.0.0.1'),
(62, 'Administrador', 'usuários', 1, 'alterou', '2012-10-04 09:48:13', '127.0.0.1'),
(63, 'Administrador', 'usuários', 18, 'deletou', '2012-10-04 09:48:31', '127.0.0.1'),
(64, 'Administrador', 'usuários', 16, 'deletou', '2012-10-04 09:48:41', '127.0.0.1'),
(65, 'Administrador', 'Configuração', 1, 'Publicou', '2012-10-04 09:50:55', '127.0.0.1'),
(66, 'Administrador', 'Configuração', 1, 'Publicou', '2012-10-04 09:54:52', '127.0.0.1'),
(67, 'Administrador', 'Configuração', 1, 'Publicou', '2012-10-04 09:55:47', '127.0.0.1'),
(68, 'Administrador', 'usuários', 1, 'entrou', '2012-10-04 10:03:33', '127.0.0.1'),
(69, 'Administrador', 'Configuração', 1, 'Publicou', '2012-10-04 10:03:56', '127.0.0.1'),
(70, 'Administrador', 'usuários', 1, 'entrou', '2012-10-04 12:53:34', '127.0.0.1'),
(71, 'Administrador', 'Configuração', 1, 'Publicou', '2012-10-04 12:53:53', '127.0.0.1'),
(72, 'Administrador', 'Configuração', 1, 'Publicou', '2012-10-04 12:59:54', '127.0.0.1'),
(73, 'Administrador', 'usuários', 1, 'entrou', '2016-08-20 12:50:26', '127.0.0.1'),
(74, 'Administrador', 'usuários', 1, 'entrou', '2016-08-20 12:50:39', '127.0.0.1'),
(75, 'Administrador', 'usuários', 1, 'entrou', '2016-08-20 12:52:10', '127.0.0.1'),
(76, 'Administrador', 'usuários', 15, 'deletou', '2016-08-20 13:57:49', '127.0.0.1'),
(77, 'Administrador', 'usuários', 1, 'alterou senha', '2016-08-20 13:58:34', '127.0.0.1'),
(78, 'Administrador', 'usuários', 1, 'alterou', '2016-08-20 13:58:34', '127.0.0.1'),
(79, 'Administrador', 'Banner', 1, 'Cadastrou', '2016-08-20 14:06:44', '127.0.0.1'),
(80, 'Administrador', 'Banner', 1, 'Alterou', '2016-08-20 14:07:51', '127.0.0.1'),
(81, 'Administrador', 'Banner', 1, 'Deletou', '2016-08-20 14:08:18', '127.0.0.1'),
(82, 'Administrador', 'usuários', 1, 'entrou', '2016-08-20 14:19:25', '189.5.75.30'),
(83, 'Administrador', 'Banner', 1, 'Cadastrou', '2016-08-20 14:20:32', '189.5.75.30'),
(84, 'Administrador', 'Banner', 1, 'Cadastrou', '2016-08-20 14:21:10', '189.5.75.30'),
(85, 'Administrador', 'usuários', 1, 'entrou', '2016-08-20 14:28:28', '189.5.75.30'),
(86, 'Administrador', 'Linha', 1, 'Cadastrou', '2016-08-20 14:32:07', '189.5.75.30'),
(87, 'Administrador', 'Linha', 1, 'Deletou', '2016-08-20 14:32:25', '189.5.75.30'),
(88, 'Administrador', 'Cliente', 1, 'Cadastrou', '2016-08-20 14:34:17', '189.5.75.30'),
(89, 'Administrador', 'Cliente', 1, 'Deletou', '2016-08-20 14:34:41', '189.5.75.30'),
(90, 'Administrador', 'Notícia', 1, 'Cadastrou', '2016-08-20 14:36:43', '189.5.75.30'),
(91, 'Administrador', 'Notícia', 1, 'Deletou', '2016-08-20 14:37:23', '189.5.75.30'),
(92, 'Administrador', 'Curriculum', 1, 'Cadastrou', '2016-08-20 14:41:30', '189.5.75.30'),
(93, 'Administrador', 'Curriculum', 1, 'Deletou', '2016-08-20 14:41:43', '189.5.75.30'),
(94, 'Administrador', 'Blog', 1, 'Cadastrou', '2016-08-20 14:42:54', '189.5.75.30'),
(95, 'Administrador', 'Blog', 1, 'Deletou', '2016-08-20 14:43:10', '189.5.75.30'),
(96, 'Administrador', 'usuários', 19, 'cadastrou', '2016-08-20 14:44:33', '189.5.75.30'),
(97, 'Administrador', 'usuários', 1, 'saiu', '2016-08-20 14:44:45', '189.5.75.30'),
(98, 'Padrao', 'usuários', 19, 'entrou', '2016-08-20 14:44:56', '189.5.75.30');

-- --------------------------------------------------------

--
-- Table structure for table `novidade`
--

CREATE TABLE `novidade` (
  `id` int(11) NOT NULL,
  `titulo` varchar(244) NOT NULL,
  `breve_descricao` varchar(400) NOT NULL,
  `texto` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `permissao_secao`
--

CREATE TABLE `permissao_secao` (
  `id` int(11) NOT NULL,
  `id_secao` int(11) NOT NULL DEFAULT '0',
  `id_usuarios` int(11) NOT NULL DEFAULT '0',
  `cadastrar` int(1) NOT NULL DEFAULT '0',
  `alterar` int(1) NOT NULL DEFAULT '0',
  `excluir` int(1) NOT NULL DEFAULT '0',
  `publicar` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `permissao_secao_fixa`
--

CREATE TABLE `permissao_secao_fixa` (
  `id` int(11) NOT NULL,
  `id_secao_fixa` int(11) NOT NULL DEFAULT '0',
  `id_usuarios` int(11) NOT NULL DEFAULT '0',
  `cadastrar` int(1) NOT NULL DEFAULT '0',
  `alterar` int(1) NOT NULL DEFAULT '0',
  `excluir` int(1) NOT NULL DEFAULT '0',
  `publicar` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissao_secao_fixa`
--

INSERT INTO `permissao_secao_fixa` (`id`, `id_secao_fixa`, `id_usuarios`, `cadastrar`, `alterar`, `excluir`, `publicar`) VALUES
(2, 1, 4, 1, 1, 1, 1),
(3, 1, 5, 1, 1, 1, 1),
(4, 2, 5, 1, 1, 1, 1),
(5, 1, 6, 1, 1, 1, 1),
(6, 2, 6, 1, 1, 1, 1),
(7, 1, 7, 1, 1, 1, 1),
(8, 2, 7, 1, 1, 1, 1),
(9, 1, 8, 1, 1, 1, 1),
(10, 2, 8, 1, 1, 1, 1),
(11, 1, 9, 1, 1, 1, 1),
(12, 2, 9, 1, 1, 1, 1),
(13, 1, 10, 1, 1, 1, 1),
(14, 2, 10, 1, 1, 1, 1),
(15, 1, 11, 1, 1, 1, 1),
(16, 2, 11, 1, 1, 1, 1),
(17, 1, 12, 1, 1, 1, 1),
(18, 2, 12, 1, 1, 1, 1),
(19, 1, 13, 1, 1, 1, 1),
(20, 2, 13, 1, 1, 1, 1),
(21, 1, 14, 1, 1, 1, 1),
(22, 2, 14, 1, 1, 1, 1),
(23, 1, 15, 1, 1, 1, 1),
(24, 2, 15, 1, 1, 1, 1),
(62, 2, 16, 1, 1, 1, 1),
(27, 1, 17, 1, 1, 1, 1),
(28, 2, 17, 1, 1, 1, 1),
(71, 1, 18, 1, 1, 1, 1),
(72, 2, 18, 1, 1, 1, 1),
(177, 1, 1, 1, 1, 1, 1),
(178, 2, 1, 1, 1, 1, 1),
(179, 3, 1, 1, 1, 1, 1),
(180, 4, 1, 1, 1, 1, 1),
(181, 5, 1, 1, 1, 1, 1),
(182, 8, 1, 1, 1, 1, 1),
(183, 9, 1, 1, 1, 1, 1),
(184, 10, 1, 1, 1, 1, 1),
(185, 100, 1, 1, 1, 1, 1),
(186, 1, 19, 1, 1, 1, 1),
(187, 2, 19, 1, 1, 1, 1),
(188, 3, 19, 1, 1, 1, 1),
(189, 4, 19, 1, 1, 1, 1),
(190, 5, 19, 1, 1, 1, 1),
(191, 8, 19, 1, 1, 1, 1),
(192, 9, 19, 1, 1, 1, 1),
(193, 10, 19, 1, 1, 1, 1),
(194, 100, 19, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `secao`
--

CREATE TABLE `secao` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL DEFAULT '',
  `id_secao` int(11) NOT NULL DEFAULT '0',
  `fotos` int(1) NOT NULL DEFAULT '0',
  `videos` int(1) NOT NULL DEFAULT '0',
  `data` int(1) NOT NULL DEFAULT '0',
  `banner` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `ordem` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `secao_fixa`
--

CREATE TABLE `secao_fixa` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `class` varchar(255) NOT NULL,
  `ctrl` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `secao_fixa`
--

INSERT INTO `secao_fixa` (`id`, `titulo`, `class`, `ctrl`, `img`) VALUES
(1, 'Usuários', 'users2', '../usuarios/ctrlUsuarios.php', 'images/icons/light/users2.png'),
(2, 'Banner', 'users2', '../ctrlBanner.php', 'images/icons/light/users2.png'),
(9, 'Catalogo', 'users2', '../ctrlCatalogo.php', 'images/icons/light/users2.png'),
(4, 'Cliente', 'users2', '../ctrlCliente.php', 'images/icons/light/users2.png'),
(8, 'Curriculum', 'users2', '../ctrlCurriculum.php', 'images/icons/light/users2.png'),
(10, 'Email', 'users2', '../ctrlEmail.php', 'images/icons/light/users2.png'),
(3, 'Linha', 'users2', '../ctrlLinha.php', 'images/icons/light/users2.png'),
(5, 'Noticia', 'users2', '../ctrlNovidade.php', 'images/icons/light/users2.png'),
(100, 'Blog', 'users2', '../ctrlBlog.php', 'images/icons/light/users2.png');

-- --------------------------------------------------------

--
-- Table structure for table `secao_fixa_menu`
--

CREATE TABLE `secao_fixa_menu` (
  `id` int(11) NOT NULL,
  `id_secao_fixa` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `url` varchar(300) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `secao_fixa_menu`
--

INSERT INTO `secao_fixa_menu` (`id`, `id_secao_fixa`, `titulo`, `url`) VALUES
(2, 1, 'Cadastrar', 'index.php?acao=frmCadUsuario&ctrl=usuarios'),
(3, 1, 'Listar', 'index.php?acao=listaUsuarios&ctrl=usuarios'),
(43, 2, 'Cadastrar', 'index.php?acao=frmCadBanner&ctrl=banner'),
(44, 2, 'Listar', 'index.php?acao=listaBanner&ctrl=banner'),
(45, 9, 'Cadastrar / Alterar', 'index.php?acao=frmAltCatalogo&ctrl=catalogo&id=1'),
(47, 4, 'Cadastrar', 'index.php?acao=frmCadCliente&ctrl=cliente'),
(48, 4, 'Listar', 'index.php?acao=listaCliente&ctrl=cliente'),
(49, 8, 'Cadastrar', 'index.php?acao=frmCadCurriculum&ctrl=curriculum'),
(50, 8, 'Listar', 'index.php?acao=listaCurriculum&ctrl=curriculum'),
(51, 10, 'Listar', 'index.php?acao=listaEmail&ctrl=email'),
(52, 3, 'Cadastrar', 'index.php?acao=frmCadLinha&ctrl=linha'),
(53, 3, 'Listar', 'index.php?acao=listaLinha&ctrl=linha'),
(54, 5, 'Cadastrar', 'index.php?acao=frmCadNovidade&ctrl=noticia'),
(55, 5, 'Listar', 'index.php?acao=listaNovidade&ctrl=noticia'),
(56, 100, 'Cadastrar', 'index.php?acao=frmCadBlog&ctrl=blog'),
(57, 100, 'Listar', 'index.php?acao=listaBlog&ctrl=blog');

-- --------------------------------------------------------

--
-- Table structure for table `sessao`
--

CREATE TABLE `sessao` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `login` varchar(20) NOT NULL DEFAULT '',
  `senha` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `img` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `nivel` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`id`, `nome`, `login`, `senha`, `email`, `img`, `status`, `nivel`) VALUES
(1, 'Administrador', 'admin', 'd2f1fd64e6cd3b064878156b0987f872', 'paulo@pixelgo.com.br', 'arq_usuario/26082012091412.jpg', 1, 1),
(19, 'Padrao', 'padrao', '86724cc48f471e4c10ec90ca93c999b2', 'contato@padraoind.com.br', 'arq_usuario/usuario_20082016024433.jpeg', 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `catalogo`
--
ALTER TABLE `catalogo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `configuracao`
--
ALTER TABLE `configuracao`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `curriculum`
--
ALTER TABLE `curriculum`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `linha`
--
ALTER TABLE `linha`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `linha_fotos`
--
ALTER TABLE `linha_fotos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `novidade`
--
ALTER TABLE `novidade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissao_secao`
--
ALTER TABLE `permissao_secao`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissao_secao_FKIndex1` (`id_usuarios`),
  ADD KEY `permissao_secao_FKIndex2` (`id_secao`);

--
-- Indexes for table `permissao_secao_fixa`
--
ALTER TABLE `permissao_secao_fixa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissao_secao_fixa_FKIndex1` (`id_usuarios`),
  ADD KEY `permissao_secao_fixa_FKIndex2` (`id_secao_fixa`);

--
-- Indexes for table `secao`
--
ALTER TABLE `secao`
  ADD PRIMARY KEY (`id`),
  ADD KEY `secao_FKIndex1` (`id_secao`),
  ADD KEY `id_banner` (`banner`);

--
-- Indexes for table `secao_fixa`
--
ALTER TABLE `secao_fixa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `secao_fixa_menu`
--
ALTER TABLE `secao_fixa_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessao`
--
ALTER TABLE `sessao`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `catalogo`
--
ALTER TABLE `catalogo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `configuracao`
--
ALTER TABLE `configuracao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `curriculum`
--
ALTER TABLE `curriculum`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `linha`
--
ALTER TABLE `linha`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `linha_fotos`
--
ALTER TABLE `linha_fotos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;
--
-- AUTO_INCREMENT for table `novidade`
--
ALTER TABLE `novidade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `permissao_secao`
--
ALTER TABLE `permissao_secao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `permissao_secao_fixa`
--
ALTER TABLE `permissao_secao_fixa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=195;
--
-- AUTO_INCREMENT for table `secao`
--
ALTER TABLE `secao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `secao_fixa`
--
ALTER TABLE `secao_fixa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `secao_fixa_menu`
--
ALTER TABLE `secao_fixa_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `sessao`
--
ALTER TABLE `sessao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
